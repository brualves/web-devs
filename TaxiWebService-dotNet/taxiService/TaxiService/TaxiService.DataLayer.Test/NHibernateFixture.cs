using System;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace TaxiService.DataLayer.Test
{
	public class NHibernateFixture : BaseFixture
	{
		protected ISession _session; 

		protected ISessionFactory SessionFactory
		{ 
			get { return NHConfigurator.SessionFactory; }
		}
		protected ISession Session
		{
			get { return _session;//SessionFactory.GetCurrentSession(); 
			}
		}

		protected override void OnSetup()
		{
			SetupNHibernateSession();
			base.OnSetup();
		}

		protected override void OnTeardown()
		{
			TearDownNHibernateSession();
			base.OnTeardown();
		}

		protected void SetupNHibernateSession()
		{
			SetupContextualSession();
			BuildSchema();
		}

		protected void TearDownNHibernateSession()
		{
			TearDownContextualSession();
		}

		private void SetupContextualSession()
		{
			_session =  SessionFactory.OpenSession();
		}

		private void TearDownContextualSession()
		{

			//if( Session != null ) Session.Flush() ;
		}
		private void BuildSchema()
		{
			var cfg = NHConfigurator.Configuration;
			var schemaExport = new SchemaExport(cfg);
			schemaExport.Create(false, true);
		}

		protected override void OnFixtureSetup ()
		{
			base.OnFixtureSetup ();
			log4net.Config.XmlConfigurator.Configure ();
		}

	}
}

