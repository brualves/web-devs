using System;
using TaxiService.DataLayer.Repository;
using NUnit.Framework;
using TaxiService.Domain.RepoInterface;
using TaxiService.Domain.Entities;

namespace TaxiService.DataLayer.Test
{
	[TestFixture]
	public class NHZonaRepositoryTest : NHibernateFixture
	{

		IRepository _repo; 
		string _codigo = "LSB";
		String _designacao = "Lisboa";
		int _contigente = 100;
	
		protected override void OnSetup ()
		{
			base.OnSetup ();
			_repo = new NHRepository ( Session);
		}


		[Test]
		public void inserir_nova_zona(){

			int icount = _repo.Count<Zona> ();
			var zona = persistirZona ( newZona());

			int ecount = _repo.Count<Zona> ();

			var zona_db = _repo.SelectById<Zona> ( zona.Id);

			Assert.That ( zona.Id != default(Guid));
			Assert.IsFalse ( Object.ReferenceEquals( zona, zona_db));
			Assert.That ( zona_db.Designacao.Equals(_designacao )) ;
			Assert.That ( zona_db.Contigente.Equals(_contigente )) ;
			Assert.That ( (icount + 1 ) == ecount);
		
		}

		[Test]
		public	void actualizar_propriedades_zona(){

			var zona = persistirZona ( newZona());
			int icount = _repo.Count<Zona> ();

			zona= _repo.SelectById<Zona> ( zona.Id);
			zona.Contigente = 500;
			_repo.Update (zona);
			Session.Flush ();
			Session.Evict (zona);
			var ecount = _repo.Count<Zona> ();

			var zona_db = _repo.SelectById<Zona> (zona.Id);

			Assert.That (zona.Id != default(Guid));
			Assert.IsFalse ( Object.ReferenceEquals(zona, zona_db ));
			Assert.IsTrue ( zona_db.Equals(zona));
			Assert.IsFalse (zona_db.Contigente.Equals(_contigente));
			Assert.IsTrue (ecount == icount);
		
		}
		[Test]
		public void remover_zona_persistida()
		{
			var zona = newZona ();
			_repo.Insert (zona);
			Session.Flush ();
			int icount = _repo.Count<Zona> ();
			
			zona = _repo.SelectById<Zona> (zona.Id);

			_repo.Delete(zona);
			Session.Flush ();
			Session.Evict(zona);

			int ecount = _repo.Count<Zona> ();
			
			Assert.That (zona.Id != default( Guid));
			Assert.IsFalse (_repo.Contains (zona));
			Assert.That ((icount - 1) == ecount);

		}

		Zona newZona(){
			return new Zona(){ Designacao = _designacao, Contigente = _contigente , Codigo = _codigo};

		}

		Zona persistirZona(Zona zona)
		{
			_repo.Insert (zona);
			Session.Flush ();
			Session.Evict (zona);
			return zona;
		}
	}
}

