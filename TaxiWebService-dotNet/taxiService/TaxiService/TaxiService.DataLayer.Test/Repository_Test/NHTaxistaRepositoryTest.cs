using System;
using TaxiService.DataLayer.Repository;
using NUnit.Core;
using NUnit.Framework;
using System.Collections.Generic;
using TaxiService.Domain.Entities;
using TaxiService.Domain.RepoInterface;
using TaxiService.Domain.Exception;

namespace TaxiService.DataLayer.Test
{
	[TestFixture]
	public class NHTaxistaRepositoryTest : NHibernateFixture
	{

		private string _mome = "TaxistaTeste", 
			_cap = "1234567", 
			_telefone = "999999999",
			_email = "teste@isel.pt",
			_userName = "testeUser", 
			_passWord = "12345";
		private Empresa _empresa;
		int _idade = 10;
		IRepository _repo;

		private Taxista newTaxista ()
		{
			return new Taxista () { PrimeiroNome = _mome, NomeUtilizador = _userName, Idade = _idade, Telefone = _telefone, 
				Email = _email, PassWord = _passWord, CAP = _cap, Empresa = _empresa};
		}

		protected override void OnSetup ()
		{
			base.OnSetup ();
			_repo = new NHRepository (Session);
			_empresa = new Empresa () { Designacao ="EmpresaTeste", Alvara="1111111", Telefone = "222223"};

			_repo.Insert (_empresa);
			_session.Flush ();
			_session.Evict (_empresa);

			Assert.IsTrue (_empresa.Id != default(Guid));
		}

		[Test]
		public void inserir_novo_taxista ()
		{
		
			var taxista = newTaxista ();
			_repo.Insert (taxista);

			Session.Flush ();
			Session.Evict (taxista);

			Assert.IsTrue (taxista.Id != default(Guid));


		}

		[Test]
		public void ler_taxista_persistido ()
		{
			var taxista = newTaxista ();
			_repo.Insert (taxista);
			
			Session.Flush ();
			Session.Evict (taxista);

			var taxista_db = _repo.SelectById<Taxista> (taxista.Id);

			Assert.That (taxista.Id != default(Guid));
			Assert.That (taxista.Equals (taxista_db));
			Assert.IsFalse (Object.ReferenceEquals (taxista, taxista_db));

			Assert.IsTrue (_mome.Equals (taxista_db.PrimeiroNome));
			Assert.IsTrue (_userName.Equals (taxista_db.NomeUtilizador));
			Assert.IsTrue (_passWord.Equals (taxista_db.PassWord));
//TO-DO
//			Assert.IsTrue (_telefone.Equals (taxista_db.Telefone));
			Assert.IsTrue (_empresa.Equals (taxista_db.Empresa));
//			Assert.IsTrue (_email.Equals (taxista_db.EMail));
//			Assert.IsTrue (_idade.Equals (taxista_db.Idade));
		}

		[Test]
		public void actualizar_propriedades_taxista ()
		{
			var taxista = newTaxista ();

			_repo.Insert (taxista);
			Session.Flush ();
			Session.Evict (taxista);

			taxista.PrimeiroNome = "userTest2";
			taxista.CAP = "12345";
//			taxista.EMail = "bas@isel.pt";
//			taxista.UserName = "testeU";
			taxista.PassWord = "aaaa";

			_repo.Update (taxista);
			Session.Flush ();
			Session.Evict (taxista);


			var taxista_db = _repo.SelectById<Taxista> (taxista.Id);

			Assert.That (taxista.Id != default(Guid));
			Assert.That (taxista.Equals (taxista_db));
			Assert.IsFalse (Object.ReferenceEquals (taxista, taxista_db));

			Assert.IsFalse (_mome.Equals (taxista_db.PrimeiroNome));
			Assert.IsFalse (_passWord.Equals (taxista_db.PassWord));
			Assert.IsFalse (_cap.Equals (taxista_db.CAP));
			
		}

		[Test]
		public void persistir_entidade_taxista_com_dados_incompletos ()
		{

			Assert.Throws<DBException> (() => { 
				_repo.Insert (new Taxista ());
				Session.Flush ();
			});

		}

		[Test]
		public void persistir_entidades_duplicadas ()
		{
			Assert.Throws<DBException> (() => { 

				var taxista = newTaxista ();
				_repo.Insert (taxista);
				
				Session.Flush ();
				Session.Evict (taxista);

				_repo.Insert (taxista);
				Session.Flush ();
				Session.Evict (taxista);

			});
		}

		[Test]
		public void eliminar_taxista_persistido ()
		{
			var taxista = newTaxista ();
			_repo.Insert (taxista);
			Session.Flush ();
			//Session.Evict (taxista);

			int init_count = _repo.Count<Taxista> ();
		
			_repo.Delete (taxista);
			Session.Flush ();
			//Session.Evict(taxista);

			int count = _repo.Count<Taxista> ();
			var taxista_db = _repo.SelectById <Taxista> (taxista.Id);

			Assert.That (taxista.Id != default(Guid));
			Assert.That ((count + 1) == init_count);
			Assert.Null (taxista_db);
		
		}

		[Test]

		public void listar_taxistas_persistidos ()
		{

			var taxista = newTaxista ();
			_repo.Insert (taxista);
			Session.Flush ();
			Session.Evict (taxista);
			
			int count_db = _repo.Count<Taxista> ();
			var list = _repo.List<Taxista> ();
			
			Assert.That (count_db == list.Count);
			IEnumerator<Taxista> liste = list.GetEnumerator ();
			liste.MoveNext ();
			Assert.IsTrue (taxista.Equals (liste.Current));

		}

		[Test]
		public void verificar_existencia_duma_entidade_persistida()
		{
			var taxista = newTaxista ();
			_repo.Insert (taxista);
			Session.Flush ();
			Session.Evict (taxista);

			Assert.IsTrue (_repo.Contains(taxista ));
		}



	}
}

