using System;
using TaxiService.DataLayer.Repository;
using NUnit.Framework;
using System.Collections.Generic;
using TaxiService.Domain.RepoInterface;
using TaxiService.Domain.Entities;

namespace TaxiService.DataLayer.Test
{
	[TestFixture]
	public class NHServicoTransporteRepositoryTest : NHibernateFixture
	{


		IRepository _repo;
		int? _avaliacao = 5;
		int _distanciaTempo = 5;

		RespostaServico _resposta;		
		SolicitacaoTranporte _solicitacaoTransporte;

		ServicoTaxi _servicoTaxi;
		Taxi _taxi;
		Taxista _taxista;
		DateTime _iservico, _fservico;
		Empresa _empresa;
		Zona _zona;
		Utente _utente;

		protected override void OnSetup ()
		{
			base.OnSetup ();

			_repo = new NHRepository (Session);
			_iservico = DateTime.Now;
			_fservico = default(DateTime);

			_empresa = new Empresa () { Designacao ="EmpresaTeste", Alvara="1111111", Telefone = "222223"};
			_zona = new Zona (){ Designacao ="Lisboa", Contigente = 100, Codigo = "LSB" };
			_taxi = new Taxi (){ Matricula = "222-Teste", Lugares = 4, Licenca = "licteste",
								Zona = _zona, Empresa = _empresa, Numero = 5 };
			_taxista = new Taxista (){ PrimeiroNome = "TaxistaTeste", NomeUtilizador = "ttaTeste", Idade = 10, Telefone = "999999", 
									 PassWord = "12345", CAP = "1224556", Empresa = _empresa, Email = "taxistaTeste@isel.pt" };

			_servicoTaxi = new ServicoTaxi (){
				Taxi = _taxi, Taxista = _taxista, InicioServico = _iservico , FimServico = _fservico};


			_utente = new Utente (){ PrimeiroNome = "UtenteTeste", NomeUtilizador = "tutente", Email = "utente@teste.isel.pt", PassWord = "123457" };

			_solicitacaoTransporte = new SolicitacaoTranporte
			{
				Utente = _utente,
				Latitude = 79.0,
				Longitude = -80,
				Estado = 1, 
				PontoReferencia = "Ao pe daki"
			};

			_resposta = new RespostaServico {
				ServicoTaxi = _servicoTaxi,
				SolicitacaoTranporte = _solicitacaoTransporte
			};


			_repo.Insert (_empresa);
			_repo.Insert (_zona);
			_repo.Insert (_taxi);
			_repo.Insert (_taxista);
			_repo.Insert (_servicoTaxi);
			_repo.Insert (_utente);
			_repo.Insert (_solicitacaoTransporte);
			_repo.Insert (_resposta);

			Session.Flush ();
			Session.Evict (_empresa);
			Session.Evict (_zona);
			Session.Evict (_taxi);
			Session.Evict (_taxista);
			Session.Evict (_servicoTaxi);
			Session.Evict (_solicitacaoTransporte);
			Session.Evict (_resposta);
			Session.Evict (_utente);

		}

		[Test]
		public void InserirNovoServicoTransporte ()
		{

			int icount = _repo.Count<ServicoTransporte> ();

			var transporte = persistirTransporteTaxi (newServicoTransporteaxi ());

			int ecount = _repo.Count<ServicoTransporte> ();

			var transporte_db = _repo.SelectById<ServicoTransporte> (transporte.Id);

			Assert.That (transporte.Id != default(Guid));
			Assert.IsFalse (Object.ReferenceEquals (transporte, transporte_db));
			Assert.That ((icount + 1) == ecount);

			Assert.That (transporte_db.ServicoTaxi.Equals (_resposta));
			Assert.That (transporte_db.SolicitacaoTranporte.Utente.Equals (_utente));


		}

		[Test]
		public void RemoverServicoTransportePersistido ()
		{
			var transporte = newServicoTransporteaxi ();
			_repo.Insert (transporte);
			Session.Flush ();
			int icount = _repo.Count<ServicoTransporte> ();
			
			transporte = _repo.SelectById<ServicoTransporte> (transporte.Id);

			_repo.Delete (transporte);
			Session.Flush ();
			Session.Evict (transporte);

			int ecount = _repo.Count<ServicoTransporte> ();
			
			Assert.That (transporte.Id != default( Guid));
			Assert.IsFalse (_repo.Contains (transporte));
			Assert.That ((icount - 1) == ecount);

		}

		[Test]
		public void ListarServicoTransportePersistidos ()
		{
			var transporte = persistirTransporteTaxi (newServicoTransporteaxi ());

			int db_count = _repo.Count<ServicoTransporte> ();

			IList<ServicoTransporte> transportes = _repo.List<ServicoTransporte> ();
			int list_count = transportes.Count;
			IEnumerator<ServicoTransporte> itTransportes = transportes.GetEnumerator ();
			itTransportes.MoveNext ();
			var transportes_db = itTransportes.Current;

			Assert.That (transporte.Id != default(Guid));
			Assert.That (list_count == db_count);
			Assert.IsFalse (object.ReferenceEquals (transporte, transportes_db));
			Assert.That (transporte.Equals (transporte));


		}

		[Test]
		public void ObterUtenteAssociadoAoServicoDeTransporte ()
		{

			var transporte = persistirTransporteTaxi (newServicoTransporteaxi ());
			
			var transporte_bd = _repo.SelectById<ServicoTransporte> (transporte.Id);

			var utente = transporte.SolicitacaoTranporte.Utente;

			Assert.That (transporte_bd, Is.Not.Null);
			Assert.That (transporte_bd.Id, Is.Not.Null);
			Assert.That (utente, Is.Not.Null);
			Assert.That (utente.Id, Is.Not.Null);
			Assert.That (utente.NomeUtilizador, Is.Not.Null);
			Assert.That (utente.Email, Is.Not.Null);

		
		}

		[Test]
		public void ObterServicoTaxiAssociadoAoServicoDeTransporte ()
		{
			
			var transporte = persistirTransporteTaxi (newServicoTransporteaxi ());
			
			var transporte_bd = _repo.SelectById<ServicoTransporte> (transporte.Id);
			
			var servico = transporte.ServicoTaxi;
			
			Assert.That (transporte_bd, Is.Not.Null);
			Assert.That (transporte_bd.Id, Is.Not.Null);
			Assert.That (servico, Is.Not.Null);
			Assert.That (servico.Id, Is.Not.Null);
//			Assert.That (servico.Taxi, Is.Not.Null);
//			Assert.That (servico.Taxista, Is.Not.Null);
//			Assert.That (servico.Taxi.Id, Is.Not.Null);
//			Assert.That (servico.Taxista.Id, Is.Not.Null);
//			Assert.That (servico.Taxi.Licenca, Is.Not.Null);
//			Assert.That (servico.Taxista.CAP, Is.Not.Null);
				
		}

		[Test]
		public void ActualizarPropriedadesServicoTransporte ()
		{

			var transporte = persistirTransporteTaxi (newServicoTransporteaxi ());

			int icount = _repo.Count<ServicoTransporte> ();

			transporte = _repo.SelectById<ServicoTransporte> (transporte.Id);

			transporte.Avaliacao = 2;
//			transporte.Estado = 2;

			_repo.Update (transporte);
			Session.Flush ();
			Session.Evict (transporte);

			var transporte_db = _repo.SelectById<ServicoTransporte> (transporte.Id);
			int ecount = _repo.Count<ServicoTransporte> ();

			Assert.That (transporte.Id != default(Guid));
			Assert.IsFalse (object.ReferenceEquals (transporte, transporte_db));
			Assert.That (icount == ecount);
			Assert.That (transporte_db.Equals (transporte));

		
		}


		ServicoTransporte newServicoTransporteaxi ()
		{
			return new ServicoTransporte{ 
				SolicitacaoTranporte  = _solicitacaoTransporte,
				ServicoTaxi = _servicoTaxi,
				DistanciaTempo = _distanciaTempo,
				Avaliacao = _avaliacao
			};

		}

		ServicoTransporte persistirTransporteTaxi (ServicoTransporte ttaxi)
		{
			_repo.Insert (ttaxi);
			Session.Flush ();
			Session.Evict (ttaxi);
			return ttaxi;
		}
	}
}

