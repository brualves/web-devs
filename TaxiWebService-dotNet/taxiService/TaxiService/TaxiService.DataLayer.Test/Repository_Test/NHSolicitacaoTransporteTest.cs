using System;
using NUnit.Framework;
using TaxiService.Domain.RepoInterface;
using TaxiService.Domain.Entities;
using TaxiService.DataLayer.Repository;

namespace TaxiService.DataLayer.Test
{
	public class NHSolicitacaoTransporteTest : NHibernateFixture
	{
		static IRepository _repo;

		static double  _longitude =-17.8,  _latitude = 98.145;
		static string  _pontoReferencia = "Ao do ISEL";


		//ServicoTaxi _servicoTaxi;
		//Taxi _taxi;
		//Taxista _taxista;
		//DateTime _iservico, _fservico;
		//Empresa _empresa;
		//Zona _zona;
		static int _estado = 1;
	
		static Utente _utente;

		protected override void OnSetup ()
		{
			base.OnSetup ();

			_repo = new NHRepository (Session);

			//_iservico = DateTime.Now;
			//_fservico = default(DateTime);
			//_empresa = new Empresa () { Designacao ="EmpresaTeste", Alvara="1111111", Telefone = "222223"};
			//_zona = new Zona (){ Designacao ="Lisboa", Contigente = 100, Codigo = "LSB" };
			//_taxi = new Taxi (){ Matricula = "222-Teste", Lugares = 4, Licenca = "licteste",
			//	Zona = _zona, Empresa = _empresa, Numero = 5 };
			//_taxista = new Taxista (){ PrimeiroNome = "TaxistaTeste", NomeUtilizador = "ttaTeste", Idade = 10, Telefone = "999999", 
			//	PassWord = "12345", CAP = "1224556", Empresa = _empresa};
			
//			_servicoTaxi = new ServicoTaxi (){
//				Taxi = _taxi, Taxista = _taxista, InicioServico = _iservico , FimServico = _fservico};
			
			_utente =  new Utente (){ PrimeiroNome = "UtenteTeste", NomeUtilizador = "uteste", Email = "utente@teste.isel.pt", PassWord = "123457" };
//			_repo.Insert (_empresa);
//			_repo.Insert (_zona);
//			_repo.Insert (_taxi);
//			_repo.Insert (_taxista);
//			_repo.Insert (_servicoTaxi);
			_repo.Insert (_utente);
			
			Session.Flush ();
			_repo.Insert (_utente);
//			Session.Evict (_empresa);
//			Session.Evict (_zona);
//			Session.Evict (_taxi);
//			Session.Evict (_taxista);
//			Session.Evict (_servicoTaxi);
		}


		[Test ]
		public void AdicionarNovaSolicitacaoTransporte(){

			int iniCount = _repo.Count<SolicitacaoTranporte> ();

			var novaEntidade = PersistirTransporteTaxi (NewSolicitacaoTransporte ());

			int endCount = _repo.Count<SolicitacaoTranporte> ();

			Assert.That ( novaEntidade, Is.Not.Null); 
			Assert.That ( iniCount + 1 , Is.EqualTo( endCount ));


		}




		SolicitacaoTranporte PersistirTransporteTaxi (SolicitacaoTranporte transporte)
		{
			_repo.Insert (transporte);
			Session.Flush ();
			Session.Evict (transporte);
			return transporte;
		}

		public static SolicitacaoTranporte NewSolicitacaoTransporte()
		{
			return new SolicitacaoTranporte{
				Utente 			= 	_utente,
				Latitude 		= _latitude,
				Longitude 		= _longitude, 
				PontoReferencia = _pontoReferencia,
				Estado 			= _estado
			};
		}
	}
}

