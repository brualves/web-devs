using System;
using NUnit.Framework;
using TaxiService.DataLayer.Repository;
using TaxiService.Domain.RepoInterface;
using TaxiService.Domain.Entities;
using TaxiService.Domain.Exception;

namespace TaxiService.DataLayer.Test
{
	[TestFixture]
	public class NHUtenteRepositoryTest : NHibernateFixture
	{
	

		IRepository _repo;
		String 
			_email = "userTeste@teste.com",
			_nomeUser = "userTeste", 
			_password = "12345";

		protected override void OnSetup ()
		{
			base.OnSetup ();
			_repo = new NHRepository (Session);

		}

		[Test]
		public void PersistirNovaEntidadeUtente ()
		{
			int icount = _repo.Count<Utente> ();

			var utente = persiste_utente (newUtente ());

			var ecount = _repo.Count<Utente> ();

			var utente_db = _repo.SelectById <Utente> (utente.Id);
			Assert.That (utente.Id != default(Guid));
			Assert.IsFalse (Object.ReferenceEquals (utente, utente_db));
			Assert.That (utente.Equals (utente_db));
			Assert.That ((icount + 1) == (ecount));

		}

		[Test]
		public void ErroAoPersistirUtenteDuplicados ()
		{

			Assert.Throws<DBException> (() => {
				persiste_utente (newUtente ());
				persiste_utente (newUtente ());
			});
		}

		[Test]
		public void ErroAoPersistirUtenteSemDados ()
		{
			Assert.Throws <DBException> (() => {

				_repo.Insert (new Utente ());
				Session.Flush ();
			});
		}

		[Test]
		public void ActualizarPropriedadeUtente ()
		{
			var utente = persiste_utente (newUtente ());
			int icount = _repo.Count<Utente> ();

			utente = _repo.SelectById<Utente> (utente.Id);
			utente.PassWord = "3333";
			utente.PrimeiroNome = "User";

			_repo.Update (utente);

			Session.Flush ();
			Session.Evict (utente);
			//Session.Close ();
			var utente_db = _repo.SelectById<Utente> (utente.Id);
			int ecount = _repo.Count<Utente> ();

			Assert.That (utente.Id != default(Guid));
			Assert.IsFalse (Object.ReferenceEquals (utente, utente_db));
			Assert.IsTrue (utente.Equals (utente_db));
//			Assert.IsFalse (utente_db.PrimeiroNome.Equals ("User"));
			Assert.IsFalse (utente_db.PassWord.Equals (_password));
			Assert.That (icount == ecount);
		}

		[Test]
		public void EliminarUtenteDaBd ()
		{
			var utente = newUtente ();
			_repo.Insert (utente);
			Session.Flush ();
			int icount = _repo.Count<Utente> ();

			_repo.Delete (utente);

			int ecount = _repo.Count<Utente> ();

			Assert.That (utente.Id != default( Guid));
			Assert.IsFalse (_repo.Contains (utente));
			Assert.That ((icount - 1) == ecount);
			
		}

		Utente persiste_utente (Utente utente)
		{
			_repo.Insert (utente);
			Session.Flush ();
			Session.Evict (utente);
			return utente;
		}

		Utente newUtente ()
		{
			return new Utente (){ NomeUtilizador = _nomeUser, PassWord = _password , Email = _email };
		}
	}
}

