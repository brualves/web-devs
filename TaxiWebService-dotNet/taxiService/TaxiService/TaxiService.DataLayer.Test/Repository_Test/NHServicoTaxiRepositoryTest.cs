using System;
using TaxiService.DataLayer.Repository;
using NUnit.Framework;
using System.Collections.Generic;
using TaxiService.Domain.RepoInterface;
using TaxiService.Domain.Entities;

namespace TaxiService.DataLayer.Test
{
	[TestFixture]
	public class NHServicoTaxiRepositoryTest : NHibernateFixture
	{

		IRepository _repo;
		Taxi _taxi;
		Taxista _taxista;
		DateTime _iservico, _fservico;
		Empresa _empresa;
		Zona _zona;

		protected override void OnSetup ()
		{
			base.OnSetup ();
			_repo = new NHRepository (Session);
			_iservico = DateTime.Now;
			_fservico = default(DateTime);
			_empresa = new Empresa () { Designacao ="EmpresaTeste", Alvara="1111111", Telefone = "222223"};
			_zona = new Zona (){ Designacao ="Lisboa", Contigente = 100, Codigo = "LSB"};
			_taxi = new Taxi (){ Matricula = "222-Teste", Lugares = 4, Licenca = "licteste",
								Zona = _zona, Empresa = _empresa, Numero = 5 };

			_taxista = new Taxista (){ PrimeiroNome = "TaxistaTeste", NomeUtilizador = "ttaTeste", Idade = 10, Telefone = "999999", 
									 PassWord = "12345", CAP = "1224556", Empresa = _empresa, Email = "taxista@Isel.pt"};
			_repo.Insert (_empresa);
			_repo.Insert (_zona);
			_repo.Insert (_taxi);
			_repo.Insert (_taxista);

			Session.Flush ();
			Session.Evict (_empresa);
			Session.Evict (_zona);
			Session.Evict (_taxi);
			Session.Evict (_taxista);


		}

		[Test]
		public void InserirNovoServicoTaxi ()
		{

			int icount = _repo.Count<ServicoTaxi> ();
			var servico = persistirServicoTaxi (newServicoTaxi ());

			int ecount = _repo.Count<ServicoTaxi> ();

			var servico_db = _repo.SelectById<ServicoTaxi> (servico.Id);

			Assert.That (servico.Id != default(Guid));
			Assert.IsFalse (Object.ReferenceEquals (servico, servico_db));
			Assert.That ((icount + 1) == ecount);

			Assert.That (servico_db.Taxi.Equals (_taxi));
			Assert.That (servico_db.Taxista.Equals (_taxista));
//	Assert.That (servico_db.InicioServico.CompareTo (_iservico) == 0);
			Assert.That (servico_db.FimServico.Equals (default(DateTime)));

		
		}

		[Test]
		public void RemoverServicoTaxiPersistido ()
		{
			var servico = newServicoTaxi ();
			_repo.Insert (servico);
			Session.Flush ();
			int icount = _repo.Count<ServicoTaxi> ();
			
			servico = _repo.SelectById<ServicoTaxi> (servico.Id);

			_repo.Delete (servico);
			Session.Flush ();
			Session.Evict (servico);

			int ecount = _repo.Count<ServicoTaxi> ();
			
			Assert.That (servico.Id != default( Guid));
			Assert.IsFalse (_repo.Contains (servico));
			Assert.That ((icount - 1) == ecount);

		}


		[Test]
		public void ListarServicoTaxisPersistidos()
		{
			var staxi = persistirServicoTaxi (newServicoTaxi());

			int db_count = _repo.Count<ServicoTaxi> ();

			IList<ServicoTaxi> staxis = _repo.List<ServicoTaxi> ();
			int list_count = staxis.Count;
			IEnumerator<ServicoTaxi> itStaxi = staxis.GetEnumerator ();
			itStaxi.MoveNext();
			var staxi_db = itStaxi.Current;

			Assert.That (staxi.Id != default(Guid));
			Assert.That ( list_count == db_count);
			Assert.IsFalse ( object.ReferenceEquals( staxi, staxi_db));
			Assert.That( staxi.Equals(staxi));

		}


		ServicoTaxi newServicoTaxi ()
		{
			return new ServicoTaxi{ Taxi = _taxi, Taxista = _taxista, InicioServico = _iservico, FimServico = _fservico };

		}

		ServicoTaxi persistirServicoTaxi (ServicoTaxi staxi)
		{
			_repo.Insert (staxi);
			Session.Flush ();
			Session.Evict (staxi);
			return staxi;
		}
	}
}

