using System;
using TaxiService.DataLayer.Repository;
using NUnit.Framework;
using System.Collections.Generic;
using TaxiService.Domain.Entities;
using TaxiService.Domain.RepoInterface;
using TaxiService.Domain.Exception;


namespace TaxiService.DataLayer.Test
{
	[TestFixture]
	public class NHTaxiRepositoryTest : NHibernateFixture
	{

		String _matricula = "AAAA-2222-3333", _licenca = "20130511";
		int _lugares = 10;
		Empresa _empresa;
		Zona _zona; 
		//Taxista _taxista;
//		DateTime _ano;
		IRepository _repo;

		int? _numero = 5;

		protected override void OnSetup ()
		{
			base.OnSetup ();
			_repo = new NHRepository (Session);
			_empresa = new Empresa () { Designacao ="EmpresaTeste", Alvara="1111111", Telefone = "222223"};
			_zona = new Zona (){ Designacao ="Lisboa", Contigente = 100, Codigo = "LSB"};
			_repo.Insert (_empresa);
			_repo.Insert (_zona);
			_session.Flush ();
			_session.Evict (_empresa);
			_session.Evict (_zona);
		}

		[Test]
		public void persistir_nova_entidade_taxi ()
		{
			int icount = _repo.Count<Taxi> ();
			var taxi = persisteTaxi (newTaxi ());
			int ecount = _repo.Count<Taxi> ();

			var taxi_db 	= _repo.SelectById<Taxi> (taxi.Id);

			var taxi_empresa = taxi_db.Empresa;
			var taxi_zona = taxi.Zona;

			Assert.That (taxi.Id != default(Guid));
			Assert.That ((icount + 1) == ecount);
			Assert.IsFalse (Object.ReferenceEquals (taxi, taxi_db));
			Assert.IsTrue (taxi.Equals (taxi_db));
			Assert.That ( taxi_empresa.Id != default(Guid));
			Assert.That ( taxi_empresa.Equals(_empresa ));

			Assert.That (taxi_zona.Id != default(Guid));
			Assert.That (taxi_zona.Equals(_zona));

		}

		[Test]
		public void persistir_entidade_taxi_sem_dados ()
		{

			Assert.Throws<DBException> (() => {
				persisteTaxi (new Taxi ()); });
		
		}

		[Test]
		public void persistir_entidade_taxi_duplicado ()
		{
			Assert.Throws <DBException> (
				() => {
				persisteTaxi (newTaxi ());
				persisteTaxi (newTaxi ());}
			);
		
		}

		[Test]
		public void eliminar_entidade_taxi_persistido ()
		{
			var taxi = newTaxi ();
			_repo.Insert (taxi);
			Session.Flush ();

			int icount = _repo.Count<Taxi> ();
			_repo.Delete (taxi);
			Session.Flush ();
			int ecount = _repo.Count<Taxi> ();
			Assert.That ((icount - 1) == (ecount));
			Assert.That (taxi.Id != default(Guid));
			Assert.IsFalse (_repo.Contains (taxi));
		}

		[Test]
		public void actualizar_propriedades_entidade_taxi ()
		{
			var taxi = persisteTaxi (newTaxi ());
			taxi.Lugares = 29;
			taxi.Licenca = "123444567";

			_repo.Update (taxi);
			Session.Flush ();
			Session.Evict (taxi);

			var taxi_db = _repo.SelectById<Taxi> (taxi.Id);


			Assert.That (taxi.Id != default(Guid));
			Assert.IsFalse (Object.ReferenceEquals (taxi, taxi_db));
			Assert.IsTrue (taxi.Equals (taxi_db));
			Assert.IsFalse (_lugares.Equals (taxi_db.Lugares));
			Assert.IsFalse (_licenca.Equals (taxi_db.Licenca));

		}

		[Test]
		public void listar_taxi_persistido ()
		{
			var taxi = persisteTaxi (newTaxi ());
			int count_db = _repo.Count<Taxi> ();
			IList<Taxi> taxis = _repo.List<Taxi> ();

			int count_list = taxis.Count;

			IEnumerator<Taxi> itTaxi = taxis.GetEnumerator ();
			itTaxi.MoveNext ();
			var taxi_db = itTaxi.Current;

			Assert.That (count_db == count_list);
			Assert.IsFalse (Object.ReferenceEquals (taxi, taxi_db));
			Assert.IsTrue (taxi.Equals (taxi_db));
		
		}

		private Taxi persisteTaxi (Taxi taxi)
		{
			_repo.Insert (taxi);
			Session.Flush ();
			Session.Evict (taxi);
			return taxi;
		}

		private Taxi newTaxi ()
		{
			return new Taxi (){ Matricula = _matricula, Licenca = _licenca, Lugares = _lugares, //Ano = _ano, 
				Empresa  = _empresa, Zona = _zona, Numero = _numero
			};
		}

	}
}

