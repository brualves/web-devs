using System;
using NUnit.Framework;
using System.Collections.Generic;
using TaxiService.DataLayer.Repository;
using TaxiService.Domain.RepoInterface;
using TaxiService.Domain.Entities;
using TaxiService.Domain.Exception;


namespace TaxiService.DataLayer.Test
{
	[TestFixture]
	public class NHEmpresaRepositoryTest : NHibernateFixture
	{

		int _empresa_count;
		string 	_designacao = "EmpresaTeste", 
			_alvara = "1234567", 
			_telefone = "999999999";
		IRepository _empresaRepo;

		protected override void OnSetup ()
		{
			base.OnSetup ();

			_empresaRepo = new NHRepository (Session);
			_empresa_count = _empresaRepo.Count<Empresa> (); 
		}

		[Test]
		public void inserir_nova_empresa ()
		{
			var empresa = new Empresa{
				Designacao = _designacao,
				Alvara = _alvara,
				Telefone = _telefone
			};

			_empresaRepo.Insert (empresa);

			Session.Flush ();

			int empresa_count = _empresaRepo.Count<Empresa> ();

			Assert.That (empresa.Id != default(Guid));
			Assert.That (empresa_count == (_empresa_count + 1));

		
		}

		[Test]
		public void inserir_empresas_duplicadas_provoca_erro ()
		{

			Assert.Throws <DBException> (() => {

				var empresa = new Empresa {Designacao = _designacao, Alvara = _alvara , Telefone = _telefone};
				_empresaRepo.Insert (empresa);
				Session.Flush ();
				var empresa_2 = new Empresa{ Designacao = _designacao, Alvara = _alvara , Telefone = _telefone};
				_empresaRepo.Insert (empresa_2);
				Session.Flush ();
			});
		}

		[Test]
		public void ler_empresa_previamente_persistida ()
		{
			var empresa = new Empresa {Designacao = _designacao, Alvara = _alvara, Telefone = _telefone };
			_empresaRepo.Insert (empresa);

			Session.Flush ();
			Session.Evict (empresa);

			Empresa empresa_per = _empresaRepo.SelectById<Empresa> (empresa.Id);

			Assert.True (empresa.Id.Equals (empresa_per.Id));
			Assert.False (Object.ReferenceEquals (empresa, empresa_per));
			Assert.True (empresa.Alvara.Equals (empresa_per.Alvara));
			Assert.True (empresa.Designacao.Equals (empresa_per.Designacao));
			Assert.True (empresa.Telefone.Equals (empresa_per.Telefone));


		}

		[Test]
		public void actualizar_propriedades_empresa ()
		{
			var empresa = new Empresa {Designacao = _designacao , Alvara = _alvara , Telefone = _telefone };

			_empresaRepo.Insert (empresa);
			Session.Flush ();
			Session.Evict (empresa);

			empresa.Designacao = "teste2";
			empresa.Telefone = "11111111";

			_empresaRepo.Update (empresa);

			Session.Evict (empresa);
			Session.Flush ();

			var update_empresa = _empresaRepo.SelectById<Empresa> (empresa.Id);

			Assert.True (empresa.Id.Equals (update_empresa.Id));
			Assert.False (Object.ReferenceEquals (empresa, update_empresa));
			Assert.False (update_empresa.Designacao.Equals (_designacao));
			Assert.False (update_empresa.Telefone.Equals (_telefone));


		}

		[Test]
		public void eliminar_entidade_persistida ()
		{
			var empresa = new Empresa {Designacao = "Teste3" , Alvara = _alvara , Telefone = _telefone };

			_empresaRepo.Insert (empresa);
			Session.Flush ();

			int init_count = _empresaRepo.Count<Empresa> ();

			_empresaRepo.Delete (empresa);
			int count = _empresaRepo.Count<Empresa> ();

			Assert.That ((init_count - 1) == count);


		}

		[Test]
		public void entidade_vazia_nao_altera_a_BD_ao_eliminar ()
		{
			var empresa = new Empresa {Designacao = "Teste3" , Alvara = _alvara , Telefone = _telefone };
			_empresaRepo.Insert (empresa);
			Session.Flush ();
		
			int init_count = _empresaRepo.Count<Empresa> ();
			_empresaRepo.Delete (new Empresa ());
			int count = _empresaRepo.Count<Empresa> ();
			Assert.That (init_count == count);

		}

		[Test]
		public void validar_existencia_duma_entidade_persistida ()
		{
			var empresa = new Empresa {Designacao = _designacao , Alvara = _alvara , Telefone = _telefone };
			_empresaRepo.Insert (empresa);
			Session.Flush ();
			Session.Evict (empresa);

			Assert.IsTrue (_empresaRepo.Contains (empresa));
		}

		[Test]
		public void listar_empresas_na_BD ()
		{
			var empresa = new Empresa {Designacao = _designacao , Alvara = _alvara , Telefone = _telefone };
			_empresaRepo.Insert (empresa);
			Session.Flush ();
			Session.Evict (empresa);

			int count_db = _empresaRepo.Count<Empresa> ();
			var list = _empresaRepo.List<Empresa> ();

			Assert.That (count_db == list.Count);
			IEnumerator<Empresa> liste = list.GetEnumerator ();
			liste.MoveNext ();
			Assert.IsTrue (empresa.Equals (liste.Current));
		}
//
//		[Test]
//		[ExpectedException("TaxiService.BussinessLayer.Exception.Data.DBInsertException")]
//		public	void insert_empresa_com_mesma_designacao(){
//			
//			var empresa_1 = new Empresa{ 
//				Designacao = _designacao, 
//				Alvara = _alvara, 
//				Telefone = _telefone,
//			};
//			
//			var empresa_2 = new Empresa{ 
//				Designacao = _designacao, 
//				Alvara = "111999111", 
//				Telefone = _telefone,
//			};
//			DBService.Insert (empresa_1);
//			DBService.Insert (empresa_2);
//		}
//
//		[Test]
//		[ExpectedException("TaxiService.BussinessLayer.Exception.Common.RequiredValuesException")]
//		public void insert_empresa_sem_campos_obrigatorios()
//		{
//			var empresa = new Empresa ();
//			DBService.Insert (empresa);
//		}



	}
}

