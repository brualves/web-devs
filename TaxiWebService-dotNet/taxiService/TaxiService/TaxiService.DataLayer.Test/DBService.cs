//using System;
//using System.Reflection;
//using TaxiService.Domain.Entities;
//using TaxiService.Domain.RepoInterface;
//using TaxiService.DataLayer.Repository;
//using TaxiService.DataLayer;
//using TaxiService.Domain.Exception;
//using System.Collections.Generic;
//using NHibernate;
//using NHibernate.Criterion;
//
//namespace TaxiService.BusinessLayer
//{
//	public static class DBService
//	{
//	
//		private static string LOG_HANDLER = "DAL_SERVICE";
//		private static ISession _session;
//
//		private  static IRepository Repository {
//			get { return  new NHRepository (Session);}
//		}
//
//		public static ISession Session {
//			get { 
//				if (_session == null || ! _session.IsOpen)
//					return (_session = NHConfigurator.Session);
//				return _session;
//			}
//		}
//	 
//		private static ILogHandler LogHandler { get { return LogFramework.GetLogHandler (LOG_HANDLER); } }
//
//		public static void Insert (Entity entety)
//		{
//		
//			var repo = Repository;
//
//			try {
//				repo.Insert (entety);
//			} catch (DBException dbex) {
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
//				throw;
//			} catch (System.Exception ex) {
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
//				throw;
//			}
//
//		
//		}
//
//		public static T SelectById<T> (Guid id) where T : Entity
//		{
//			var repo = Repository;
//
//			try {
//				return repo.SelectById<T> (id);
//
//			} catch (DBException dbex) {
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
//			} catch (System.Exception ex) {
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
//				//Send mail 
//
//				throw;
//			}
//			return default(T);
//		}
//
//		public static IList<Entity> List (Type type)
//		{
//			var repo = Repository;
//			if (typeof(Empresa).Equals (type)) 
//				return (IList<Entity>) repo.List<Empresa> ();
//			return null;
//			
//		}
//
//		public static IList<T> List<T> () where T: Entity
//		{
//
//			try {
//				var repo = Repository;
//				return repo.List<T> ();
//			
//			} catch (DBException dbex) {
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
//				throw;
//			} catch (Exception ex) {
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
//				//Send mail 
//				throw;
//			}
//		}
//
//		public static	int Count <T> () where T : Entity
//		{
//			try {
//		 	
//				var repo = Repository;
//				return repo.Count<T> ();
//			
//			} catch (DBException dbex) {
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
//			} catch (Exception ex) {
//				
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
//				//Send mail 
//				throw ex;
//			}
//			return 0;
//		}
//
//		public static void Delete (Entity entity)
//		{
//			try {
//				
//				var repo = Repository;
//
//				repo.Delete (entity);
//
//				Session.Evict(entity);
//
//				
//			} catch (DBException dbex) {
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
//			} catch (Exception ex) {
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
//				//Send mail 
//				throw ex;
//			}
//		}
//	
//		public static IList<T> SelectByCriteria<T> (string property, string value) where T:Entity
//		{
//
////			var empresas = Session.CreateCriteria<Empresa> ()
////							.Add (Restrictions.Eq ("Designacao", designacao))
////								.List<Empresa> ();
////			if (empresas == null)
////				return null;
////			IEnumerator<Empresa> enu = empresas.GetEnumerator ();
////			enu.MoveNext ();
////			return enu.Current;
//
//			return Repository.SelectByCriteria<T> (property, value);
//
//		
//		}
//
//		public static void Update (Entity entity)
//		{
//			try {
//				
//				var repo = Repository;
//				repo.Update (entity);
//				
//			} catch (DBException dbex) {
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
//				throw dbex;
//			} catch (Exception ex) {
//				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
//				//Send mail 
//				throw ex;
//			}
//		}
//	}
//}
//
