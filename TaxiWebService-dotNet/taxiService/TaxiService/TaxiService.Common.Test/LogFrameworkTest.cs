using System;
using NUnit.Framework;
using TaxiService.Common;

namespace TaxiService.Comman.Test
{
	[TestFixture]
	public class LogFrameworkTest : BaseFixture
	{
		private String _logName = "Teste";

	protected override void OnSetup ()
	{
			base.OnSetup ();
	}

	[Test]
	public void obter_log_handler()
	{
			ILogHandler log = LogFramework.GetLogHandler (_logName);

			Assert.NotNull (log);
	}

	[Test]
	public void adicionar_nova_mensagem_ao_log()
	{

			ILogHandler log = LogFramework.GetLogHandler (_logName);

			log.Add ( LogFramework.MSGTYPE.WARNING, "Test Log Msg...Mensagem tipo Warning" );
			log.Add (LogFramework.MSGTYPE.INFO ,"Test Log Msg...Mensagem tipo default[ERROR]");
	}

	[Test]
	public void adicionar_excepcao_ao_log()
	{
			ILogHandler log = LogFramework.GetLogHandler (_logName);
			log.Add ( LogFramework.MSGTYPE.ERROR, new InvalidOperationException("Teste nao suportado"));
	}
	}

}

