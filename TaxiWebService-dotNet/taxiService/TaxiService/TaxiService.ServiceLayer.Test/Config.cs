using System;

namespace TaxiService.ServiceLayer.Test
{

	public class Config
	{
		public const string AbsoluteBaseUri = "http://localhost:50000/";
		public const string ServiceStackBaseUri = AbsoluteBaseUri + "api";
	}

}

