using System;
using NUnit.Core;
using System.Diagnostics;
using NUnit.Framework;
using ServiceStack.Logging;
using ServiceStack.WebHost.Endpoints;
using Funq;
using TaxiService.ServiceLayer.Model;
using ServiceStack.ServiceClient.Web;
using TaxiService.BusinessLayer.Test;

namespace TaxiService.ServiceLayer.Test
{
	[TestFixture]
	public class ServicoTransporteServiceTest
	{
	
		public class AppHost : AppHostHttpListenerBase
		{
			public AppHost () : base("Servico Tranporte Service Tests", typeof(Empresa).Assembly)
			{
			}

			public override void Configure (Container container)
			{
			}
		}

		const string BaseUri = "http://localhost:8080/";
		private string 
			_IdServico, 
			_IdTaxista;
		String _idUtente, _IdServicoTaxi;
		AppHost appHost;

		[TestFixtureSetUp]
		public void TestFixtureSetUp ()
		{
			appHost = new AppHost ();
			appHost.Init ();
			appHost.Start (BaseUri);
		}

		[TestFixtureTearDown]
		public void TestFixtureTearDown ()
		{
			appHost.Dispose ();
			appHost = null;
		}

		[SetUp]
		public void TestSetup ()
		{
			ConfigDB.Config ();
			_IdServico = ConfigDB._transporte_db.Id.ToString ();
			_idUtente = ConfigDB._transporte_db.Utente.NomeUtilizador;
			_IdTaxista = ConfigDB._taxista_db.NomeUtilizador;
			_IdServicoTaxi = ConfigDB._servico_db.Id.ToString ();
			
			

		}

		[Test]
		public void ObterSolicitaoServicoTransporteEmTaxi ()
		{
//			var restClient = new JsonServiceClient (BaseUri);
//
//			var sTransporte = restClient.Get (new SolicitacaoTransporte { IdServico = _IdServico });
//
//			Assert.That (sTransporte, Is.Not.Null);
//			Assert.That (sTransporte.IdServico, Is.EqualTo (_IdServico));
//			Assert.That (sTransporte.Taxista, Is.Not.Null);
//			Assert.That (sTransporte.Utente, Is.Not.Null);
//			Assert.That (sTransporte.Estado, Is.Not.Null);
//			Assert.That (sTransporte.Longitude, Is.Not.Null);
//			Assert.That (sTransporte.Latitude, Is.Not.Null);
		

			throw new NotSupportedException ();
		}

		[Test]
		public void ObterUtenteAssociadoASolicitacaoTransporte ()
		{
//			var restClient = new JsonServiceClient (BaseUri);
//			
//			var utente = restClient.Get (new GetUtenteServico { IdServico = _IdServico });
//			
//			Assert.That (utente, Is.Not.Null);
//			Assert.That (utente.IdUtente, Is.EqualTo (_idUtente));
//			Assert.That (utente.Nome, Is.Not.Null);
//			Assert.That (utente.Username, Is.Not.Null);

		}

		[Test]
		public void ObterTaxistaAssociadoAoServicoTransporte ()
		{
//			var restClient = new JsonServiceClient (BaseUri);
//			
//			var utaxista = restClient.Get (new GetTaxistaServico { IdServico = _IdServico });
//			
//			Assert.That (utaxista, Is.Not.Null);
//			Assert.That (utaxista.IdTaxista, Is.EqualTo (_IdTaxista));
//			Assert.That (utaxista.Nome, Is.Not.Null);

			
		}

		[Test]
		public void ResponderAumServicoTransporte ()
		{
//			var restClient = new JsonServiceClient (BaseUri);
//
//			var servico = restClient.Post (
//			                             new PedidoTransporte{ IdUtente = _idUtente, 
//											Latitude = 10 , 
//											Longitude = 23, 
//											PontoReferencia = "Teste ref"
//			});
//
//
//
//
//			var trservico = restClient.Post (new ResponseServico{ 
//					IdServico = servico.IdServico, 
//				IdServicoTaxi = _IdServicoTaxi,
//					Longitude = 10,
//					Latitude = 10,
//					Distance = 1});
//
//
//			Assert.That (servico, Is.Not.Null); 
//			Assert.That (trservico, Is.Not.Null); 
//			Assert.That (trservico.IdServico, Is.EqualTo (servico.IdServico)); 


		}
	}
}

