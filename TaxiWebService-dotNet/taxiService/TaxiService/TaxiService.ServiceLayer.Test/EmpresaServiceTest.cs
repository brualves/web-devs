using System;
using NUnit.Core;
using System.Diagnostics;
using NUnit.Framework;
using ServiceStack.Logging;
using ServiceStack.WebHost.Endpoints;
using Funq;
using TaxiService.ServiceLayer.Model;
using ServiceStack.ServiceClient.Web;
using TaxiService.BusinessLayer.Test;

namespace TaxiService.ServiceLayer.Test
{

	[TestFixture]
	public class EmpresaServiceTest

	{
	
		public class AppHost : AppHostHttpListenerBase
		{
			public AppHost() : base("EmpresaService Tests", typeof(Empresa).Assembly) {}
			public override void Configure (Container container){}

		}

		private string _IdEmpresa = "EmpresaTeste";
		const string BaseUri = "http://localhost:8080/";
		
		AppHost appHost;
		
		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			appHost = new AppHost();
			appHost.Init();
			appHost.Start(BaseUri);
		}
		
		[TestFixtureTearDown]
		public void TestFixtureTearDown()
		{
			appHost.Dispose();
			appHost = null;
		}

		[SetUp]
		public void TestSetup(){
			ConfigDB.Config ();

			_IdEmpresa = ConfigDB._empresa_db.Designacao;
		}

		[Test]
		public void ObterEmpresaRemoto()
		{
			var _idEmpresa = _IdEmpresa;
			var restClient = new JsonServiceClient(BaseUri);

			EmpresaResponse empresa = restClient.Get( new Empresa{ IdEmpresa = _idEmpresa} );

			Assert.That(empresa, Is.Not.Null);
			Assert.That(empresa.Designacao, Is.EqualTo(_idEmpresa));
			Assert.That(empresa.Alvara, Is.Not.Null);
			Assert.That(empresa.Telefone, Is.Not.Null);
		
		}

		[Test]
		public void PostEmpresaRemoto(){

			var _idEmpresa = "EmpresaTest2";
			var restClient = new JsonServiceClient(BaseUri);

			Empresa empresa = restClient.Post( new ColecaoEmpresa{ Designacao = _idEmpresa, Telefone = "(221)9991234", Alvara = "1234567"} );

			Assert.That (empresa, Is.Not.Null);
			Assert.That (empresa.IdEmpresa, Is.EqualTo(_idEmpresa));

		
		}

		[Test]
		public void ActualizarEmpresaRemoto()
		{
			var _idEmpresa = _IdEmpresa;
			var newTele = "(221)88877744";

			var restClient = new JsonServiceClient(BaseUri);
			
			var empresa = restClient.Put( new UpdateEmpresa{ IdEmpresa = _idEmpresa, Telefone = newTele, Alvara = "1234567"} );

			Assert.That (empresa, Is.Not.Null);
			Assert.That (empresa.Designacao, Is.EqualTo(_idEmpresa));
			//Assert.That (empresa.Alvara, Is.EqualTo(_idEmpresa));
			Assert.That (empresa.Telefone, Is.EqualTo(newTele));
		}

		[Test]
		public void RemoverEmpresaRemoto()
		{
			var restClient = new JsonServiceClient(BaseUri);

			var empresa = restClient.Delete ( new DeleteEmpresa{IdEmpresa = _IdEmpresa });

			Assert.That (empresa, Is.Not.Null);
			Assert.That (empresa.Designacao, Is.EqualTo( _IdEmpresa ));

		}


		[Test]
		public void ObterTaxistasAssociadosAempreas(){

			var restClient = new JsonServiceClient (BaseUri);

			var taxistasEmpresa = restClient.Get ( new AllTaxistasEmpresa{ IdEmpresa = _IdEmpresa} );

			var taxEnum = taxistasEmpresa.GetEnumerator (); taxEnum.MoveNext ();
			var taxista = taxEnum.Current;

			var singleTaxista = restClient.Get (new GetEmpresaTaxista{ IdEmpresa =_IdEmpresa, IdTaxista = taxista.IdTaxista });

			Assert.That (taxistasEmpresa, Is.Not.Null);
			Assert.That (taxistasEmpresa.Count, Is.GreaterThan(0));
			Assert.That (singleTaxista, Is.Not.Null);
			Assert.That (singleTaxista.IdTaxista, Is.EqualTo(taxista.IdTaxista));
			Assert.That (singleTaxista.Empresa.IdEmpresa, Is.EqualTo(_IdEmpresa));

		}

		[Test]
		public void ObterTaxisAssociadosAempreas(){
			
			var restClient = new JsonServiceClient (BaseUri);
			
			var taxisEmpresa = restClient.Get ( new AllTaxisEmpresa{ IdEmpresa = _IdEmpresa} );
			
			var taxisEnum = taxisEmpresa.GetEnumerator (); taxisEnum.MoveNext ();
			var taxis = taxisEnum.Current;
			
			var singleTaxi = restClient.Get (new GetEmpresaTaxi{ IdEmpresa =_IdEmpresa, IdTaxi = taxis.IdTaxi });
			
			Assert.That (taxisEmpresa, Is.Not.Null);
			Assert.That (taxisEmpresa.Count, Is.GreaterThan(0));
			Assert.That (singleTaxi, Is.Not.Null);
			Assert.That (singleTaxi.IdTaxi, Is.EqualTo(taxis.IdTaxi));
			Assert.That (singleTaxi.Empresa.IdEmpresa, Is.EqualTo(_IdEmpresa));
			
			
		}

		[Test]
		public void ObterTaxisDisponiveisNaEmpreas(){
			
			var restClient = new JsonServiceClient (BaseUri);

			var taxisEmpresaAll = restClient.Get ( new AllTaxisEmpresa{ IdEmpresa = _IdEmpresa } );

			var taxisEmpresaAvailable = restClient.Get ( new AllTaxisEmpresa{ IdEmpresa = _IdEmpresa, IsActive = true } );

			var taxisEmpresaNotAvailable = restClient.Get ( new AllTaxisEmpresa{ IdEmpresa = _IdEmpresa, IsActive = false } );

			Assert.That (taxisEmpresaAvailable, Is.Not.Null);
			Assert.That (taxisEmpresaAll, Is.Not.Null);
			Assert.That (taxisEmpresaAvailable.Count, Is.GreaterThan(0));
			Assert.That (taxisEmpresaNotAvailable, Is.Null);
			Assert.That (taxisEmpresaAvailable.Count, Is.EqualTo(taxisEmpresaAll.Count ));

			
		}

//		[Test]
//
//		public void SendMsgToTaxista()
//		{
//			EmpresaService.SendMsgToTaxista ();
//		}


	}
}

