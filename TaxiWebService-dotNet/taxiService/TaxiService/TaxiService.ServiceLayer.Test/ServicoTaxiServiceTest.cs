using System;
using NUnit.Core;
using System.Diagnostics;
using NUnit.Framework;
using ServiceStack.Logging;
using ServiceStack.WebHost.Endpoints;
using Funq;
using TaxiService.ServiceLayer.Model;
using ServiceStack.ServiceClient.Web;
using TaxiService.BusinessLayer.Test;

namespace TaxiService.ServiceLayer.Test
{
	[TestFixture]
	public class ServicoTaxiServiceTest
	{
	
		public class AppHost : AppHostHttpListenerBase
		{
			public AppHost () : base("EmpresaService Tests", typeof(Empresa).Assembly)
			{
			}

			public override void Configure (Container container)
			{
			}

		}

		const string BaseUri = "http://localhost:8080/";
		private string _IdServico, _IdTaxi, _IdTaxista;
		AppHost appHost;
		
		[TestFixtureSetUp]
		public void TestFixtureSetUp ()
		{
			appHost = new AppHost ();
			appHost.Init ();
			appHost.Start (BaseUri);
		}
		
		[TestFixtureTearDown]
		public void TestFixtureTearDown ()
		{
			appHost.Dispose ();
			appHost = null;
		}

		[SetUp]
		public void TestSetup ()
		{
			ConfigDB.Config ();
			_IdServico = ConfigDB._servico_db.Id.ToString();
			_IdTaxi = ConfigDB._taxi_db.Matricula;
			_IdTaxista = ConfigDB._taxista_db.NomeUtilizador;

		}


		public void ListarTodosServicosTaxis()
		{
			var restClient = new JsonServiceClient (BaseUri);

			var servicos = restClient.Get (new ColecaoServicosTaxi());

			Assert.That (servicos, Is.Not.Null);
			Assert.That (servicos.Count,Is.GreaterThan(0));

		}

		[Test]
		public void ObterServicoRemoto ()
		{
			var restClient = new JsonServiceClient (BaseUri);

			ServicoTaxiResponse servico = restClient.Get (new ServicoTaxi{IdServico = _IdServico});

			Assert.That (servico, Is.Not.Null);
			Assert.That (servico.IdServico, Is.EqualTo (_IdServico));
			Assert.That (servico.Fim, Is.EqualTo (default(DateTime).ToString()));
			Assert.That (servico.Inicio, Is.Not.EqualTo (default(DateTime).ToString()));
			Assert.That (servico.IsActive, Is.True);
			Assert.That (servico.Taxi, Is.Not.Null);
			Assert.That (servico.Taxista, Is.Not.Null);

		}

		[Test]
		public void PostServicoRemoto ()
		{


			var restClient = new JsonServiceClient (BaseUri);

			ServicoTaxi servico = restClient.Post (new NovoServicoTaxi{ IdTaxi = _IdTaxi, IdTaxista = _IdTaxista});
			Assert.That (servico, Is.Not.Null);
		
		}

		[Test]
		public void FecharServicoTaxiRemoto ()
		{
			var restClient = new JsonServiceClient (BaseUri);
			
			var servico = restClient.Put (new UpdateServicoTaxi{ IdServico = _IdServico});

			Assert.That (servico, Is.Not.Null);
			Assert.That (servico.Fim, Is.Not.EqualTo (default(DateTime)));
			Assert.That (servico.IsActive, Is.False);
		}

		[Test]
		public void RemoverServicoTaxiRemoto ()
		{
			var restClient = new JsonServiceClient (BaseUri);

			var servico = restClient.Delete (new DeleteServicoTaxi{ IdServico = _IdServico});

			Assert.That (servico, Is.Not.Null);
			Assert.That (servico, Is.Not.Null);
			Assert.That (servico.IdServico, Is.EqualTo (_IdServico));
			Assert.That (servico.Fim, Is.EqualTo (default(DateTime).ToString()));
			Assert.That (servico.Inicio, Is.Not.EqualTo (default(DateTime).ToString()));
			Assert.That (servico.IsActive, Is.True);
			Assert.That (servico.Taxi, Is.Not.Null);
			Assert.That (servico.Taxista, Is.Not.Null);

		}

		[Test]
		public void ObterTaxistaAssociadosAoServico ()
		{

			var restClient = new JsonServiceClient (BaseUri);

			var taxistaServico = restClient.Get (new GetServicoTaxiTaxista{ IdServico = _IdServico});
	
			Assert.That (taxistaServico, Is.Not.Null);
			Assert.That (taxistaServico.IdTaxista, Is.EqualTo(_IdTaxista));

		}

		[Test]
		public void ObterTaxiAssociadoAoServico ()
		{
			
			var restClient = new JsonServiceClient (BaseUri);
			
			var taxiServico = restClient.Get (new GetServicoTaxiTaxi{ IdServico = _IdServico});
			
			Assert.That (taxiServico, Is.Not.Null);
			Assert.That (taxiServico.IdTaxi, Is.EqualTo(_IdTaxi));
			
			
		}


	}
}

