using System;
using NUnit.Core;
using System.Diagnostics;
using NUnit.Framework;
using ServiceStack.Logging;
using ServiceStack.WebHost.Endpoints;
using Funq;
using TaxiService.ServiceLayer.Model;
using ServiceStack.ServiceClient.Web;
using TaxiService.BusinessLayer.Test;
using System.Collections.Generic;

namespace TaxiService.ServiceLayer.Test
{

	[TestFixture]
	public class ZonaServiceTest
	{
	
		public class AppHost : AppHostHttpListenerBase
		{
			public AppHost() : base("ZonaService Tests", typeof(Zona).Assembly) {}
			public override void Configure (Container container){}

		}

		private string _IdZona = "";
		const string BaseUri = "http://localhost:8080/";
		
		AppHost appHost;
		
		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			appHost = new AppHost();
			appHost.Init();
			appHost.Start(BaseUri);
		}
		
		[TestFixtureTearDown]
		public void TestFixtureTearDown()
		{
			appHost.Dispose();
			appHost = null;
		}

		[SetUp]
		public void TestSetup(){
			ConfigDB.Config ();

			_IdZona = ConfigDB._zona_db.Codigo;
		}

		[Test]
		public void ObterZonaRemoto()
		{
			var restClient = new JsonServiceClient(BaseUri);

			ZonaResponse zona = restClient.Get( new Zona{ IdZona = _IdZona} );

			Assert.That(zona, Is.Not.Null);
			Assert.That(zona.IdZona, Is.EqualTo(_IdZona));
			Assert.That(zona.Contigente, Is.Not.Null);
			Assert.That(zona.Designacao, Is.Not.Null);


		}

		[Test]
		public void ObterListarZonasRemoto()
		{
			var restClient = new JsonServiceClient(BaseUri);
			
			IList<Zona> zonas = restClient.Get (new ColecaoZona ());
			
			Assert.That(zonas, Is.Not.Null);
			Assert.That(zonas.Count, Is.EqualTo(1));
		}

		[Test]
		public void PostZonaRemoto(){

			var _idZona = "POR";
			var restClient = new JsonServiceClient(BaseUri);

			var zona = restClient.Post(
				new NovaZona{ IdZona = _idZona, Contigente = 500, Designacao = "Porto" });

			Assert.That (zona, Is.Not.Null);
			Assert.That (zona.IdZona, Is.EqualTo(_idZona));

		
		}

		[Test]
		public void ActualizarZonaRemoto()
		{
			var contigente = 700;

			var restClient = new JsonServiceClient(BaseUri);
			
			var zona = restClient.Put( new UpdateZona{ IdZona = _IdZona, Contigente = contigente, Designacao = "Lisboa"});

			Assert.That (zona, Is.Not.Null);
			Assert.That (zona.IdZona, Is.EqualTo(_IdZona));
			//Assert.That (empresa.Alvara, Is.EqualTo(_idEmpresa));
			Assert.That (zona.Contigente, Is.EqualTo(contigente));
		}

		[Test]
		public void RemoverZonaRemoto()
		{
			var restClient = new JsonServiceClient(BaseUri);
			var zona = restClient.Delete (new DeleteZona{IdZona = _IdZona});
			Assert.That (zona.IdZona, Is.EqualTo(_IdZona));
		}

		[Test]
		public void ObterTaxisAssociadosAzona()
		{
			var restClient = new JsonServiceClient (BaseUri);
			
			var taxisZona = restClient.Get ( new AllTaxisZona{ IdZona = _IdZona} );
			
			var taxisEnum = taxisZona.GetEnumerator (); taxisEnum.MoveNext ();
			var taxis = taxisEnum.Current;
			
			var singleTaxi = restClient.Get (new GetZonaTaxi{ IdZona =_IdZona, IdTaxi = taxis.IdTaxi });
			
			Assert.That (taxisZona, Is.Not.Null);
			Assert.That (taxisZona.Count, Is.GreaterThan(0));
			Assert.That (singleTaxi, Is.Not.Null);
			Assert.That (singleTaxi.IdTaxi, Is.EqualTo(taxis.IdTaxi));
			Assert.That (singleTaxi.Empresa, Is.Not.Null);
			Assert.That (singleTaxi.Zona.IdZona, Is.EqualTo(_IdZona));

		}

	}
}

