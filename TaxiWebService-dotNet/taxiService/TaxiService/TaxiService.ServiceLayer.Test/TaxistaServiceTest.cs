using System;
using NUnit.Core;
using System.Diagnostics;
using NUnit.Framework;
using ServiceStack.Logging;
using ServiceStack.WebHost.Endpoints;
using Funq;
using TaxiService.ServiceLayer.Model;
using ServiceStack.ServiceClient.Web;
using TaxiService.BusinessLayer.Test;
using System.Collections.Generic;

namespace TaxiService.ServiceLayer.Test
{

	[TestFixture]
	public class TaxistaServiceTest
	{
	
		public class AppHost : AppHostHttpListenerBase
		{
			public AppHost() : base("TaxistaService Tests", typeof(Zona).Assembly) {}
			public override void Configure (Container container){}

		}

		private string _IdTaxista = "";
		const string BaseUri = "http://localhost:8080/";
		
		AppHost appHost;
		
		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			appHost = new AppHost();
			appHost.Init();
			appHost.Start(BaseUri);
		}
		
		[TestFixtureTearDown]
		public void TestFixtureTearDown()
		{
			appHost.Dispose();
			appHost = null;
		}

		[SetUp]
		public void TestSetup(){
			ConfigDB.Config ();

			_IdTaxista = ConfigDB._taxista_db.NomeUtilizador;
		}



		[Test]

		public void  SetApplicationIDdoTaxista(){


			var _appKey = "AAA128381273812748912784129858126589";

			var restClient = new JsonServiceClient(BaseUri);

			var prev_taxita = restClient.Get ( new Taxista { IdTaxista = _IdTaxista });
			 
			var taxita_updated = restClient.Put ( new SetTaxistaAppID { IdTaxista = _IdTaxista, ApplicationId = _appKey });

			Assert.That ( prev_taxita, Is.Not.Null);
			Assert.That ( prev_taxita.ApplicationId, Is.Not.Null);
			Assert.That ( taxita_updated, Is.Not.Null);
			Assert.That ( taxita_updated.ApplicationId, Is.Not.Null);
			Assert.That ( taxita_updated.ApplicationId, Is.EqualTo(_appKey));

		
		}
		[Test]
		public void ObterTaxistaRemoto()
		{
			var restClient = new JsonServiceClient(BaseUri);

			TaxistaResponse taxista = restClient.Get( new Taxista{ IdTaxista = _IdTaxista} );

			Assert.That(taxista, Is.Not.Null);
			Assert.That(taxista.IdTaxista, Is.EqualTo(_IdTaxista));
			Assert.That(taxista.CAP, Is.Not.Null);
			Assert.That(taxista.Idade, Is.Not.Null);
			Assert.That(taxista.Nome, Is.Not.Null);
			Assert.That (taxista.Empresa,Is.Not.Null);
		}

		[Test]
		public void ListarTaxistaRemoto()
		{
			var restClient = new JsonServiceClient(BaseUri);
			
			IList<Taxista> taxista = restClient.Get (new ColecaoTaxistas ());
			
			Assert.That(taxista, Is.Not.Null);
			Assert.That(taxista.Count, Is.EqualTo(1));
		}

		[Test]
		public void ObterServicosAssociadoAoTaxista(){
			
			var restClient = new JsonServiceClient(BaseUri);
			
			var servicos = restClient.Get ( new GetTaxistaServicos{ IdTaxista = _IdTaxista} );
			
			Assert.That (servicos, Is.Not.Null);
			Assert.That (servicos.Count, Is.GreaterThan(0));
			
		}


//		[Test]
//		public void PostTaxistaRemoto(){
//
//			var _idTaxista = "testeT";
//			var restClient = new JsonServiceClient(BaseUri);
//
//			restClient.Post();
//
//			Assert.That (zona, Is.Not.Null);
//			Assert.That (zona.IdZona, Is.EqualTo(_idZona));
//
//		
//		}

		[Test]
		public void ActualizarTaxistaRemoto()
		{
			var nome  = "testeUp";
			var idade = 80;


			var restClient = new JsonServiceClient(BaseUri);
			
			var taxista = restClient.Put( new UpdateTaxista{ IdTaxista = _IdTaxista, 
			                                                 Nome = nome,
			                                                Idade= idade	
			});

			Assert.That (taxista, Is.Not.Null);
			Assert.That (taxista.IdTaxista, Is.EqualTo(_IdTaxista));
			Assert.That (taxista.Nome, Is.EqualTo(nome));
			Assert.That (taxista.Idade, Is.EqualTo(idade));
			Assert.That (taxista.Empresa, Is.Not.Null);
		}

		[Test]
		public void RemoverTaxistaRemoto()
		{
			var restClient = new JsonServiceClient(BaseUri);
			var taxista = restClient.Delete (new DeleteTaxista{IdTaxista = _IdTaxista});
			Assert.That (taxista.IdTaxista, Is.EqualTo(_IdTaxista));
		}

		[Test]
		public void ObterEmpresaTaxista()
		{
			var restClient = new JsonServiceClient(BaseUri);
			
			var empresa = restClient.Get (new GetTaxistaEmpresa{  IdTaxista =  _IdTaxista });
			
			Assert.That (empresa, Is.Not.Null);
			
		}

	}
}

