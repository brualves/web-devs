using System;
using NUnit.Core;
using System.Diagnostics;
using NUnit.Framework;
using ServiceStack.Logging;
using ServiceStack.WebHost.Endpoints;
using Funq;
using TaxiService.ServiceLayer.Model;
using ServiceStack.ServiceClient.Web;
using TaxiService.BusinessLayer.Test;
using System.Collections.Generic;

namespace TaxiService.ServiceLayer.Test
{

	[TestFixture]
	public class TaxiServiceTest
	{
	
		public class AppHost : AppHostHttpListenerBase
		{
			public AppHost() : base("Taxi Tests", typeof(Zona).Assembly) {}
			public override void Configure (Container container){}

		}

		private string _IdTaxi = "";
		const string BaseUri = "http://localhost:8080/";
		
		AppHost appHost;
		
		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			appHost = new AppHost();
			appHost.Init();
			appHost.Start(BaseUri);
		}
		
		[TestFixtureTearDown]
		public void TestFixtureTearDown()
		{
			appHost.Dispose();
			appHost = null;
		}

		[SetUp]
		public void TestSetup(){
			ConfigDB.Config ();

			_IdTaxi = ConfigDB._taxi_db.Matricula;
		}

		[Test]
		public void ObterTaxiRemoto()
		{
			var restClient = new JsonServiceClient(BaseUri);

			TaxiResponse taxi = restClient.Get( new Taxi{ IdTaxi = _IdTaxi} );

			Assert.That(taxi, Is.Not.Null);
			Assert.That(taxi.IdTaxi, Is.EqualTo(_IdTaxi));
			Assert.That(taxi.Ano, Is.Not.Null);
			Assert.That(taxi.Licenca, Is.Not.Null);
			Assert.That(taxi.Lugares, Is.Not.Null);
			Assert.That(taxi.Matricula, Is.Not.Null);
			Assert.That(taxi.Numero, Is.Not.Null);
			Assert.That (taxi.Empresa, Is.Not.Null);
			Assert.That (taxi.Zona, Is.Not.Null);
		}


		[Test]
		public void ObterServicosAssociadoAoTaxi(){
		
			var restClient = new JsonServiceClient(BaseUri);

			var servicos = restClient.Get ( new GetTaxiServicos{ IdTaxi = _IdTaxi} );

			Assert.That (servicos, Is.Not.Null);
			Assert.That (servicos.Count, Is.GreaterThan(0));
		
		}

		[Test]
		public void ListarTaxisRemoto()
		{
			var restClient = new JsonServiceClient(BaseUri);
			
			IList<Taxi> taxis = restClient.Get (new ColecaoTaxis ());
			
			Assert.That(taxis, Is.Not.Null);
			Assert.That(taxis.Count, Is.EqualTo(1));
		}

//		[Test]
//		public void PostTaxistaRemoto(){
//
//			var _idTaxista = "testeT";
//			var restClient = new JsonServiceClient(BaseUri);
//
//			restClient.Post();
//
//			Assert.That (zona, Is.Not.Null);
//			Assert.That (zona.IdZona, Is.EqualTo(_idZona));
//
//		
//		}

		[Test]
		public void ActualizarTaxistaRemoto()
		{
			var lugares  = 30;
			var ano = DateTime.Today;
			var numero = 50;
			


			var restClient = new JsonServiceClient(BaseUri);
			
			var taxi = restClient.Put( new UpdateTaxi{ 
										IdTaxi = _IdTaxi,
										Lugares = lugares,
										Ano = ano,
										Numero = numero
									});

			Assert.That (taxi, Is.Not.Null);
			Assert.That (taxi.IdTaxi, Is.EqualTo(_IdTaxi));
			Assert.That (taxi.Lugares, Is.EqualTo(lugares));
			Assert.That (taxi.Ano, Is.EqualTo(ano));
			Assert.That (taxi.Numero, Is.EqualTo(numero));
			Assert.That(taxi.Licenca, Is.Not.Null);
			Assert.That (taxi.Empresa, Is.Not.Null);
			Assert.That (taxi.Zona, Is.Not.Null);
		}

		[Test]
		public void RemoverTaxiRemoto()
		{
			var restClient = new JsonServiceClient(BaseUri);
			var taxi = restClient.Delete (new DeleteTaxi{IdTaxi = _IdTaxi});
			Assert.That (taxi.IdTaxi, Is.EqualTo(_IdTaxi));
		}

		[Test]
		public void ObterZonaTaxi()
		{
			var restClient = new JsonServiceClient(BaseUri);

			var zona = restClient.Get (new GetTaxiZona{ IdTaxi = _IdTaxi });

			Assert.That (zona, Is.Not.Null);

		}

		[Test]
		public void ObterEmpresaTaxi()
		{
			var restClient = new JsonServiceClient(BaseUri);
			
			var empresa = restClient.Get (new GetTaxiEmpresa{ IdTaxi = _IdTaxi });
			
			Assert.That (empresa, Is.Not.Null);
			Assert.That (empresa.Designacao, Is.Not.Null);
				
		}

	}
}

