using System;
using NUnit.Core;
using System.Diagnostics;
using NUnit.Framework;
using ServiceStack.Logging;
using ServiceStack.WebHost.Endpoints;
using Funq;
using TaxiService.ServiceLayer.Model;
using ServiceStack.ServiceClient.Web;
using TaxiService.BusinessLayer.Test;
using System.Collections.Generic;
using TaxiService.BusinessLayer;

namespace TaxiService.ServiceLayer.Test
{
	[TestFixture]
	public class UtenteServiceTest
	{
	
		public class AppHost : AppHostHttpListenerBase
		{
			public AppHost () : base("UtenteService Tests", typeof(Zona).Assembly)
			{
			}

			public override void Configure (Container container)
			{
			}

		}

		private string _IdUtente = "", _idServico ="";

		const string BaseUri = "http://localhost:8090/";
		AppHost appHost;
		
		[TestFixtureSetUp]
		public void TestFixtureSetUp ()
		{
			appHost = new AppHost ();
			appHost.Init ();
			appHost.Start (BaseUri);
		}
		
		[TestFixtureTearDown]
		public void TestFixtureTearDown ()
		{
			appHost.Dispose ();
			appHost = null;
		}

		[SetUp]
		public void TestSetup ()
		{
			ConfigDB.Config ();

			_IdUtente = ConfigDB._utente_db.NomeUtilizador;
			_idServico = ConfigDB._transporte_db.Id.ToString();

		}



		[Test]
		public void SetUtenteAppKey(){
			
//			var _appKey = "AAA128381273812748912784129858126589";
//			
//			var restClient = new JsonServiceClient(BaseUri);
//			
//			var prev_utente = restClient.Get ( new Utente { IdUtente = _IdUtente });
//			
//			var utente_updated = restClient.Put ( new SetUtenteAppID { IdUtente = _IdUtente, ApplicationId = _appKey });
//			
//			Assert.That ( prev_utente, Is.Not.Null);
//			Assert.That ( prev_utente.IdAplicacao, Is.Null);
////			Assert.That ( utente_updated, Is.Not.Null);
////			Assert.That ( utente_updated.ApplicationId, Is.Not.Null);
////			Assert.That ( utente_updated.ApplicationId, Is.EqualTo(_appKey));
		
		}

		[Test]
		public void ObterUtenteRemoto ()
		{
//			var restClient = new JsonServiceClient (BaseUri);
//
//			Utente utente = restClient.Get (new Utente {IdUtente = _IdUtente });
//
//			Assert.That (utente, Is.Not.Null);
//			Assert.That (utente.IdUtente, Is.EqualTo (_IdUtente));
//			Assert.That (utente., Is.Not.Null);
//			Assert.That (utente.Nome, Is.Not.Null);

//	

			throw new NotImplementedException ();
		
		}

		[Test]

		public void AvaliarUmServico ()
		{
		
//			var restClient = new JsonServiceClient (BaseUri);
//			var rate = 4;
//
//			var servico = restClient.Put (new AvaliarServico{ 
//				IdServico = _idServico, IdUtente = _IdUtente, Rating = rate
//			});
//
//			Assert.That (servico, Is.Null);
		
			throw new NotImplementedException ();
		}

		[Test]
		public void SolicitarServicoTransporte ()
		{
		
			var restClient = new JsonServiceClient (BaseUri);
//
//			var servico = restClient.Post (new PedidoTransporte { IdUtente 	= _IdUtente ,
//				Latitude = 38.7556759, Longitude = -9.1162695,
//				PontoReferencia = "Largo De camoes",
//				MinRate = 3 });
//	
//			var servicoDetails = restClient.Get (new GetServicoUtente{ 
//				IdUtente = _IdUtente,
//				IdServico = servico.IdServico});
//
//			Assert.That (servico, Is.Not.Null);
//			Assert.That (servicoDetails, Is.Not.Null);
//			Assert.That (servicoDetails.Estado, Is.EqualTo (EstadoPedido.EmProcessamento));
//			Assert.That (servicoDetails.Utente, Is.Not.Null);
//			Assert.That (servicoDetails.Rating, Is.Null);
			throw new NotImplementedException ();

		
		}


		[Test]
		public void ObterListaDeServicosAssociadosAoUser ()
		{

//			var restClient = new JsonServiceClient (BaseUri);
//
//			IList<ServicoTransporte> servicos = restClient.Get (
//				new AllServicosUtente{IdUtente = _IdUtente }
//			);
//
//			Assert.That (servicos, Is.Not.Null);
//			Assert.That (servicos.Count, Is.GreaterThan (0));
			throw new NotImplementedException ();

		}

	}
}

