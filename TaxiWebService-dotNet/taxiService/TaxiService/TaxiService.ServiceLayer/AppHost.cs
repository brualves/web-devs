using System;
using ServiceStack.WebHost.Endpoints;
using ServiceStack.Text;
using ServiceStack.ServiceModel.Serialization;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using TaxiService.BusinessLayer.Notification;
using ServiceStack.ServiceHost;
using System.Net;
using TaxiService.ServiceLayer.Handler;

namespace TaxiService.ServiceLayer
{
	public class AppHost : AppHostBase
	{
		public const string AppHostName = "TaxiService", HostingPath = "api";

		public AppHost () : base(AppHostName, typeof(AppHost).Assembly )
		{
		}

		public override void Configure (Funq.Container container)
		{


			var endPointConfig = new EndpointHostConfig ();
			
			endPointConfig.ServiceStackHandlerFactoryPath = HostingPath;
			endPointConfig.DefaultRedirectPath = "api/metadata";
			
			//endPointConfig.GlobalHtmlErrorHttpHandler = new HttpErrorHandler ();

			SetConfig (endPointConfig);

			IUserAuthRepository userRepo = new InMemoryAuthRepository ();

			string salt, hash;

			new SaltedHash ().GetHashAndSaltString (
				"password", out hash, out salt);
			userRepo.CreateUserAuth (
				new UserAuth {
				Id = 1, 
				UserName  = "root",
				Email 	  = "root@isel.pt",
				FirstName = "Root",
				Language = "User",
				DisplayName = "rootUser",
				PasswordHash = hash,
				Salt = salt
			}, "password");
			
			container.Register<IUserAuthRepository> (userRepo);

			//RequestFilters.Add (AuthRequestFilter.Instance.RequestFilter);
			
			
			Plugins.Add (
				new AuthFeature (
				null, 
				new IAuthProvider[] {
				new BasicAuthProvider()
			})
			);

	
			JsConfig.ExcludeTypeInfo = true;
			JsConfig.EmitLowercaseUnderscoreNames = true;
			JsConfig.EmitCamelCaseNames = true;
			
							
			ContentTypeFilters.Register ("application/hal+json", 
			                             (r, o, s) => JsonDataContractSerializer.Instance.SerializeToStream (o, s),
			                             JsonDataContractDeserializer.Instance.DeserializeFromStream);
			                             
			ConfigExceptionHanling ();
			

		}

		public void ConfigExceptionHanling ()
		{
//			ServiceExceptionHandler = ( requestObj ,  exception ) => {
//				
//				var httException = exception as IHttpError; 
//				var responseError = new TaxiService.ServiceLayer.Model.ResponseError ();
//				
//				if (httException != null) {
//					responseError.Status = httException.ErrorCode;
//					responseError.Message = httException.Message;
//					responseError.DeveloperMessage = httException.StatusDescription;
//					responseError.MoreInfo = new Uri ("http://localhost.com");
//				 
//					return responseError;			 
//				
//				}
//												
//				responseError.Status = HttpStatusCode.InternalServerError.ToString ();
//				responseError.Message = HttpStatusCode.InternalServerError.ToString ();
//				
//				
//				return responseError;
//											
//				
//				
//			};
		
//			this.ExceptionHandler = (req, res, operationName, ex) => {
//				res.Write ("Error: {0}: {1}".Fmt (ex.GetType ().Name, ex.Message));
//				//res.EndServiceStackRequest (skipHeaders: true);
//				
//				res.End ();
//			};
		}
	}
}

