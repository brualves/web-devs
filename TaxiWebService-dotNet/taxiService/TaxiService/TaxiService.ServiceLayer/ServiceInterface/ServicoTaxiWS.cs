using System;
using ServiceStack.ServiceInterface;
using TaxiService.ServiceLayer.Model;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;
using TaxiService.BusinessLayer;
using TaxiService.ServiceLayer.Resources;
using ServiceStack.Common.Web;
using ServiceStack.Text;
using System.Net;
using ServiceStack.ServiceHost;

namespace TaxiService.ServiceLayer.ServiceInterface
{
	public class ServicoTaxiWS : Service
	{
	
	
		public static string ServicoTaxiBaseUri = "/servicostaxi";
		public static string ServicoTaxiBaseAbsUri = Application.Service.Config.BaseUrl + ServicoTaxiBaseUri;
		public static string SigleServicoTaxiUri = ServicoTaxiBaseUri + "/{0}";
		public static string SigleServicoTaxiAbsUri = ServicoTaxiBaseAbsUri + "/{0}";

		public ServicoTaxiResource Get (ServicoTaxi servico)
		{
			
			return  new ServicoTaxiResource (GetServicoTaxi (servico.IdServico));

		}

		public ServicoTaxiCollection Get (ColecaoServicosTaxi query)
		{
			var items = BLFacade.ListEnity<IServicoTaxi> ();

			return new ServicoTaxiCollection (items);
		}

		public ServicoTaxiResource Post (UpdateServicoTaxi servico)
		{
			var servicoDB = GetServicoTaxi (servico.IdServico);
			
			servicoDB.CloseService (Convert.ToDateTime (servico.DataFim));
			
			if (! BLFacade.UpdateEntity <IServicoTaxi> (servicoDB)) 
				throw HttpError.Conflict ("");
			
			return new ServicoTaxiResource (servicoDB);
		}

		public ServicoTaxiResource Delete (ServicoTaxi servico)
		{
			var servico_db = BLFacade.RemoveEntity<IServicoTaxi> (servico.IdServico);
			if (servico_db == null)
				throw HttpError.NotFound (
					Application.ErrorMessage.NotFound.Fmt (
					SigleServicoTaxiAbsUri.Fmt (servico.IdServico))	
				);
			
			return new ServicoTaxiResource (servico_db);
		}

		public ServicoTaxiResource Post (NovoServicoTaxi novoServico)
		{
			return NovoServicoTaxi (novoServico, Response);	
		}

		public static ServicoTaxiResource NovoServicoTaxi (NovoServicoTaxi novoServico, IHttpResponse httpRes)
		{
			var taxi = BLFacade.LoadEntity<ITaxi> (novoServico.IdTaxi);
			var taxista = BLFacade.LoadEntity<ITaxista> (novoServico.IdTaxista);
			if (taxi == null || taxista == null)
				throw new HttpError (HttpStatusCode.BadRequest, new ArgumentException ());
			var servico = BLFacade.CreateEntity<IServicoTaxi> (String.Empty);
			servico.Taxi = taxi;
			servico.Taxista = taxista;
			if (!BLFacade.InsertEntity (servico))
				throw HttpError.Conflict ("");
			var resource = new ServicoTaxiResource (servico);
			httpRes.AddHeader (HttpHeaders.Location, resource.GetSelfLink ().ToString ());
			httpRes.StatusCode = (int)HttpStatusCode.Created;
			return resource;
		}

		public static IServicoTaxi GetServicoTaxi (string idServico)
		{
			var servicoDB = BLFacade.LoadEntity<IServicoTaxi> (idServico);
			
			if (servicoDB == null)
				throw HttpError.NotFound (
					Application.ErrorMessage.NotFound.Fmt (
					SigleServicoTaxiAbsUri.Fmt (idServico))	
				);
			
			return  servicoDB;
		}
	}
}

