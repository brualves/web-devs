using System;
using ServiceStack.ServiceInterface;
using TaxiService.ServiceLayer.Model;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;
using TaxiService.BusinessLayer;
using TaxiService.ServiceLayer.Resources;
using ServiceStack.Common.Web;
using ServiceStack.Text;

namespace TaxiService.ServiceLayer.ServiceInterface
{
	public class EmpresaWS : Service
	{


		public static string EmpresaBaseUri = "/empresas";
		public static string EmpresaBaseAbsUri = Application.Service.Config.BaseUrl + EmpresaBaseUri;
		public static string SigleEmpresaUri = EmpresaBaseUri + "/{0}";
		public static string SigleEmpresaAbsUri = EmpresaBaseAbsUri + "/{0}";

		public EmpresaCollection Get (ColecaoEmpresa param)
		{
		
			var listaEmpresas = BLFacade.ListEnity<IEmpresa> ();
		
			return new EmpresaCollection (listaEmpresas);
		
		}

		public EmpresaResource Get (Empresa empresa)
		{	
			return new EmpresaResource (GetEmpresa (empresa.IdEmpresa));	
		}

		public EmpresaResource Delete (Empresa empresa)
		{
			var empresaDB = BLFacade.RemoveEntity<IEmpresa> (empresa.IdEmpresa);
			if (empresaDB == null)
				throw HttpError.NotFound (
					Application.ErrorMessage.NotFound.Fmt (EmpresaBaseAbsUri.Fmt (empresa.IdEmpresa)));
			return new EmpresaResource (empresaDB);
		}

		public EmpresaResource Post (NovaEmpresa empresa)
		{
		
			var empresaDB = BLFacade.CreateEntity<IEmpresa> (empresa.Designacao);
			MapDTO (empresaDB, empresa);
			
			if (! BLFacade.InsertEntity <IEmpresa> (empresaDB))
				throw HttpError.Conflict ("");
				
			var resource = new EmpresaResource (empresaDB);
			
			Response.AddHeader (HttpHeaders.Location, resource.GetSelfLink ().ToString ());
			
			return resource;
			
		}

		public EmpresaResource Post (UpdateEmpresa empresa)
		{
			var empresaDB = GetEmpresa (empresa.IdEmpresa);
			
			MapDTO (empresaDB, empresa);
			
			if (! BLFacade.UpdateEntity<IEmpresa> (empresaDB))
				throw HttpError.Conflict ("");
				
			return new EmpresaResource (empresaDB);
			
		}

		public TaxiCollection Get (TaxisEmpresa taxi)
		{
			var empresa = GetEmpresa (taxi.IdEmpresa);
			
			return new TaxiCollection 
				(empresa.Taxis,  
			  SigleEmpresaAbsUri.Fmt (taxi.IdEmpresa) + TaxiWS.TaxiBaseUri);
		}

		public TaxistaCollection Get (TaxistasEmpresa taxistas)
		{
			var empresa = GetEmpresa (taxistas.IdEmpresa);
			
			return new TaxistaCollection 
				(empresa.Taxistas,  
			  SigleEmpresaAbsUri.Fmt (taxistas.IdEmpresa) + TaxistaWS.TaxistaBaseUri);
		}

		public static IEmpresa GetEmpresa (string empresaID)
		{
			var empresaDB = BLFacade.LoadEntity <IEmpresa> (empresaID);
			if (empresaDB == null)
				throw HttpError.NotFound (
					Application.ErrorMessage.NotFound.Fmt (EmpresaBaseAbsUri.Fmt (empresaID)));
					
			return empresaDB;
	
		}

		public static void MapDTO (IEmpresa empresaDB, EmpresaDTO empresa)
		{
			empresaDB.Alvara = empresa.Alvara;
			empresaDB.Telefone = empresa.Telefone;
			
		}
	}
}
//		public EmpresaResponse Get (Empresa empresa)
//		{
//			if (empresa == null || string.IsNullOrEmpty (empresa.IdEmpresa))
//				throw new ArgumentException ("Argumentos inválidos");
//
//			var reqEmpresa = BLFacade.LoadEntity<IEmpresa> (
//				empresa.IdEmpresa);
//
//			//Devolver 404 not found 
//			if (reqEmpresa == null) 
//				throw HttpError.NotFound (String.Format ("Empresa com o identificador{0} não existe", empresa.IdEmpresa));
//
//			return EmpresaResponse (reqEmpresa);
//
//		}
//		public Empresa Post (ColecaoEmpresa newEmpresa)
//		{
//			if (String.IsNullOrEmpty (newEmpresa.Alvara) || String.IsNullOrEmpty (newEmpresa.Designacao))
//				throw new ArgumentException ("Argumentos inválidos");
//
//			var empresa = BLFacade.CreateEntity<IEmpresa> (newEmpresa.Designacao);
//
//			empresa.Alvara = newEmpresa.Alvara;
//			empresa.Telefone = newEmpresa.Telefone;
//
//			bool inserted = 
//				BLFacade.InsertEntity<IEmpresa> (empresa);
//			if (!inserted)
//				throw new ApplicationException ("Erro ao inserir entidade");
//
//			return new Empresa { IdEmpresa = empresa.Designacao };
//}
//		public EmpresaResponse Put (UpdateEmpresa empresa)
//		{
//			if (empresa == null || String.IsNullOrEmpty (empresa.IdEmpresa))
//				throw new ArgumentException ("Argumentos inválidos");
//
//			var upEmpresa = BLFacade.LoadEntity<IEmpresa> (empresa.IdEmpresa);
//
//			if (upEmpresa == null)
//				throw HttpError.NotFound (String.Format ("Empresa com o identificador{0} não existe", empresa.IdEmpresa));
//
//			upEmpresa.Alvara = empresa.Alvara;
//			upEmpresa.Telefone = empresa.Telefone;
//
//			var updated = BLFacade.UpdateEntity<IEmpresa> (upEmpresa);
//
//			if (!updated)
//				throw new ApplicationException ("Não foi possivel actualizar entidade");
//
//			return EmpresaResponse (upEmpresa);
//		}
//
//		public EmpresaResponse Delete (DeleteEmpresa empresa)
//		{
//			if (empresa == null || String.IsNullOrEmpty (empresa.IdEmpresa))
//				throw new ArgumentException ("Argumentos inválidos");
//
//			var delEempresa = BLFacade.RemoveEntity<IEmpresa> (empresa.IdEmpresa);
//
//			if (empresa == null)
//				throw HttpError.NotFound (String.Format ("Empresa com o identificador{0} não existe", empresa.IdEmpresa));
//
//			return EmpresaResponse (delEempresa);
//		}
//
//		public Taxi Post (AddTaxi taxi)
//		{
//			if (taxi == null || string.IsNullOrEmpty (taxi.IdEmpresa) ||
//				string.IsNullOrEmpty (taxi.Matricula))
//				throw new ArgumentException ("Argumentos inválidos");
//
//
//			var empresa = BLFacade.LoadEntity<IEmpresa> (taxi.IdEmpresa);
//
//			if (empresa == null)
//				throw HttpError.NotFound (String.Format ("Empresa com o identificador{0} não existe", taxi.IdEmpresa));
//
//			var taxiDB = BLFacade.CreateEntity<ITaxi> (taxi.Matricula);
//
//			taxiDB.Licenca = taxi.Licenca;
//			taxiDB.Lugares = taxi.Lugares;
//			taxiDB.Numero = taxi.Numero;
//			//taxiDB. = taxi.Licenca;
//
//			var inserted = empresa.AddTaxis (taxiDB);
//
//			if (! inserted)
//				throw new ApplicationException ("Erro ao inserir entidade");
//
//			return new Taxi { IdTaxi = taxiDB.Matricula };
//
//		}
//
//		public Taxista Post (AddTaxista taxista)
//		{
//
//			if (taxista == null || string.IsNullOrEmpty (taxista.IdEmpresa) ||
//				string.IsNullOrEmpty (taxista.UserName) ||
//				string.IsNullOrEmpty (taxista.PassWord)
//			    )
//				throw new ArgumentException ("Argumentos inválidos");
//
//			var empresa = BLFacade.LoadEntity<IEmpresa> (taxista.IdEmpresa);
//
//			if (empresa == null)
//				throw HttpError.NotFound (String.Format ("Empresa com o identificador{0} não existe", taxista.IdEmpresa));
//			
//			var taxistaDB = BLFacade.CreateEntity<ITaxista> (taxista.UserName);
//			
//			taxistaDB.PrimeiroNome = taxista.Nome;
//			taxistaDB.CAP = taxista.CAP;
//			taxistaDB.Idade = taxista.Idade;
//			taxistaDB.PassWord = taxista.PassWord;
//			taxistaDB.Telefone = taxista.Telefone;
//
//			var inserted = empresa.AddTaxista (taxistaDB);
//			
//			if (! inserted)
//				throw new ApplicationException ("Erro ao inserir entidade");
//			
//			return new Taxista { IdTaxista = taxistaDB.NomeUtilizador };
//		}
//
//		public List<Taxista> Get (AllTaxistasEmpresa taxistaEmpresa)
//		{
//			if (taxistaEmpresa == null || string.IsNullOrEmpty (taxistaEmpresa.IdEmpresa))
//				throw new ArgumentException ("Argumentos inválidos");
//			
//			var empresa = BLFacade.LoadEntity<IEmpresa> (taxistaEmpresa.IdEmpresa);
//			
//			if (empresa == null)
//				throw HttpError.NotFound (String.Format ("Empresa com o identificador{0} não existe", taxistaEmpresa.IdEmpresa));
//
//			var taxistasDB = empresa.Taxistas;
//			
//			if (taxistasDB == null || taxistasDB.Count == 0)
//				throw HttpError.NotFound (String.Format ("Não existe taxistas associados a empresa", taxistaEmpresa.IdEmpresa));
//			;
//			
//			var retTaxistas = new List<Taxista> ();
//			
//			foreach (var item in taxistasDB) {
//				retTaxistas.Add (
//					new Taxista { IdTaxista = item.NomeUtilizador }
//				);				
//			}
//			
//			return retTaxistas;
//			
//		}
//
//		public List<Taxi> Get (AllTaxisEmpresa taxis)
//		{
//			if (taxis == null || string.IsNullOrEmpty (taxis.IdEmpresa))
//				throw new ArgumentException ("Argumentos inválidos");
//
//			var empresa = BLFacade.LoadEntity<IEmpresa> (taxis.IdEmpresa);
//
//			if (empresa == null)
//				throw HttpError.NotFound (String.Format ("Empresa com o identificador{0} não existe", taxis.IdEmpresa));
//
//
//			IList<ITaxi> taxisDB = null;
//
//			if (taxis.IsActive == null)
//				taxisDB = empresa.Taxis;
//			else
//				taxisDB = empresa.GetTaxis (taxis.IsActive);
//
//			if (taxisDB == null || taxisDB.Count == 0)
//				throw HttpError.NotFound (String.Format ("Não existem taxis associados", taxis.IdEmpresa));
//
//			var retTaxi = new List<Taxi> ();
//
//			foreach (var item in taxisDB) {
//				retTaxi.Add (
//					new Taxi { IdTaxi = item.Matricula }
//				);
//
//			}
//
//			return retTaxi;
//
//		}
//
//		public TaxiResponse Get (GetEmpresaTaxi taxiEmpresa)
//		{
//			if (taxiEmpresa == null || string.IsNullOrEmpty (taxiEmpresa.IdEmpresa) || 
//				string.IsNullOrEmpty (taxiEmpresa.IdTaxi))
//				throw new ArgumentException ("Argumentos inválidos");
//
//			var empresa = BLFacade.LoadEntity<IEmpresa> (taxiEmpresa.IdEmpresa);
//			
//			if (empresa == null)
//				throw HttpError.NotFound (String.Format ("Não existem taxis associados", taxiEmpresa.IdEmpresa));
//
//			var taxiDB = empresa.GetTaxi (taxiEmpresa.IdTaxi);
//
//			return new TaxiResponse { 
//		
//				IdTaxi = taxiDB.Matricula,
//				Ano = taxiDB.Ano,
//				Licenca = taxiDB.Licenca,
//				Lugares = taxiDB.Lugares,
//				Numero = taxiDB.Numero,
//				Matricula = taxiDB.Matricula,
//				Empresa = new Empresa { IdEmpresa = taxiDB.Empresa.Designacao},
//				Zona = new Zona { IdZona = taxiDB.Zona.Codigo },
//			};
//		}
//
//		public TaxistaResponse Get (GetEmpresaTaxista taxistaEmpresa)
//		{
//			if (taxistaEmpresa == null || string.IsNullOrEmpty (taxistaEmpresa.IdEmpresa) || 
//				string.IsNullOrEmpty (taxistaEmpresa.IdTaxista))
//				throw new ArgumentException ("Argumentos inválidos");
//			
//			var empresa = BLFacade.LoadEntity<IEmpresa> (taxistaEmpresa.IdEmpresa);
//			
//			if (empresa == null)
//				throw HttpError.NotFound (String.Format ("Não existem taxis associados", taxistaEmpresa.IdEmpresa));
//			
//			var taxistaDB = empresa.GetTaxista (taxistaEmpresa.IdTaxista);
//			
//			return new TaxistaResponse { 
//				
//				IdTaxista = taxistaDB.NomeUtilizador,
//				CAP = taxistaDB.CAP,
//				Idade = taxistaDB.Idade,
//				Telefone = taxistaDB.Telefone,
//				Nome = taxistaDB.PrimeiroNome,
//				Empresa =  new Empresa{ IdEmpresa = taxistaDB.Empresa.Key}
//			};
//		}
//
//		public static EmpresaResponse EmpresaResponse (IEmpresa reqEmpresa)
//		{
//			return new EmpresaResponse {
//				Designacao = reqEmpresa.Designacao,
//				Alvara = reqEmpresa.Alvara,
//				Telefone = reqEmpresa.Telefone,
//				IdEmpresa = reqEmpresa.Key
//			};
//		}
//
//		public static List<Empresa> EmpresaList (IList<IEmpresa> listaEmpresas)
//		{
//			var retList = new List<Empresa> ();
//			foreach (var item in listaEmpresas)
//				retList.Add (new Empresa {
//					IdEmpresa = item.Designacao
//				});
//			return retList;
//		}
//}
//}
