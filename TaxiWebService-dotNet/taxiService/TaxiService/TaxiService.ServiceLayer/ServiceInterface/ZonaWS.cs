using System;
using ServiceStack.ServiceInterface;
using TaxiService.ServiceLayer.Model;
using TaxiService.BusinessLayer;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.ServiceLayer.Resources;
using ServiceStack.Common.Web;
using System.Net;
using ServiceStack.Text;

namespace TaxiService.ServiceLayer.ServiceInterface
{
	public class ZonaWS : Service
	{
		public const string ServiceName = "Zona Service";
		public static string ZonaBaseUri = "/zonas";
		public static string ZonaBaseAbsUri = Application.Service.Config.BaseUrl + ZonaBaseUri;
		public static string SingleZonaUri = ZonaBaseUri + "/{0}";
		public static string SingleZonaAbsUri = ZonaBaseAbsUri + "/{0}";

		public ZonaResource Get (Zona zona)
		{
			return new ZonaResource (GetZona (zona.IdZona));
		}

		public ZonaCollection Get (ColecaoZona zonas)
		{

			var zonasDB = BLFacade.ListEnity <IZona> ();

			return new ZonaCollection (zonasDB);
		}

		public ZonaResource Put (NovaZona zona)
		{

			var zonaDB = BLFacade.CreateEntity<IZona> (zona.IdZona);

			MapDto (zona, zonaDB);
			
			if (! BLFacade.InsertEntity<IZona> (zonaDB))
				throw HttpError.Conflict ("");
				
			var resource = new ZonaResource (zonaDB);
			
			Response.AddHeader (HttpHeaders.Location, resource.GetSelfLink ().ToString ());
			Response.StatusCode = (int)HttpStatusCode.Created;
				
			return resource;

		}

		public  ZonaResource Post (UpdateZona zona)
		{
			var zonaDB = GetZona (zona.IdZona);
			
			MapDto (zona, zonaDB);

			if (! BLFacade.UpdateEntity<IZona> (zonaDB)) 
				HttpError.Conflict ("");
	
			return new ZonaResource (zonaDB);
		}

		public  ZonaResource Delete (Zona zona)
		{
			var zonaDB = BLFacade.RemoveEntity<IZona> (zona.IdZona);

			if (zonaDB == null)
				throw HttpError.NotFound (
					Application.ErrorMessage.NotFound.Fmt (SingleZonaAbsUri.Fmt (zona.IdZona))
				);
			
			return new ZonaResource (zonaDB);
		}

		public	TaxiCollection Get (TaxisZona taxi)
		{
			var zona = GetZona (taxi.IdZona);
			return new TaxiCollection (zona.Taxis, SingleZonaAbsUri.Fmt (taxi.IdZona) + TaxistaWS.TaxistaBaseUri);
		}

		public static void MapDto (ZonaDTO zona, IZona zonaDB)
		{
			zonaDB.Contigente = zona.Contigente;//?? zonaDB.Contigente;
			zonaDB.Designacao = zona.Designacao ?? zonaDB.Designacao;
		}

		public static IZona GetZona (string idZona)
		{
			var zonaDB = BLFacade.LoadEntity<IZona> (idZona);
			if (zonaDB == null)
				throw HttpError.NotFound (
					Application.ErrorMessage.NotFound.Fmt (SingleZonaAbsUri.Fmt (idZona))
				);
				
			return zonaDB;
		}
	}
}

