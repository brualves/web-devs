using System;
using ServiceStack.ServiceInterface;
using TaxiService.ServiceLayer.Model;
using TaxiService.BusinessLayer;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;
using TaxiService.ServiceLayer.Resources;
using ServiceStack.Common.Web;
using ServiceStack.Text;
using System.Net;
using ServiceStack.ServiceHost;

namespace TaxiService.ServiceLayer.ServiceInterface
{
	public class TaxistaWS : Service
	{
	
		
		public const string ServiceName = "Taxista Service";
		public static string TaxistaBaseUri = "/taxistas";
		public static string TaxistaBaseAbsUri = Application.Service.Config.BaseUrl + TaxistaBaseUri;
		public static string SingleTaxistaUri = TaxistaBaseUri + "/{0}";
		public static string SingleTaxistaAbsUri = TaxistaBaseAbsUri + "/{0}";

		public TaxistaResource Get (Taxista taxista)
		{
			return new TaxistaResource (GetTaxista (taxista.IdTaxista));
		}

		public TaxistaCollection Get (ColecaoTaxistas queryParam)
		{
			var items = BLFacade.ListEnity <ITaxista> ();
			
			return new TaxistaCollection (items);
		}

		public	TaxistaResource Put (NovoTaxista novoTaxista)
		{
			
			return NovoTaxista (novoTaxista, Response);
		
		}

		public TaxistaResource Post (UpdateTaxista upTaxista)
		{
			var taxista = GetTaxista (upTaxista.IdTaxista);
			
			MapDTO (taxista, upTaxista);
			
			if (! BLFacade.UpdateEntity <ITaxista> (taxista)) 
				throw HttpError.Conflict ("");
				
			return new TaxistaResource (taxista);			
		}

		public static TaxistaResource NovoTaxista (NovoTaxista novoTaxista, IHttpResponse httpRes)
		{
			var empresa = BLFacade.LoadEntity<IEmpresa> (novoTaxista.IdEmpresa);
			
			if (empresa == null)
				throw new HttpError (HttpStatusCode.BadRequest, new ArgumentException ());
			
			var taxistaDB = BLFacade.CreateEntity<ITaxista> (novoTaxista.IdTaxista);
			
			MapDTO (taxistaDB, novoTaxista);
			
			taxistaDB.PassWord = novoTaxista.PassWord;
			
			if (! empresa.AddTaxista (taxistaDB))
				throw HttpError.Conflict ("");
				
			var resource = new TaxistaResource (taxistaDB);
			
			httpRes.AddHeader (HttpHeaders.Location, resource.GetSelfLink ().ToString ());
			httpRes.StatusCode = (int)HttpStatusCode.Created;
			
			return resource;
		}

		public TaxistaResource Delete (Taxista taxista)
		{
			var taxistaDB = BLFacade.RemoveEntity<ITaxista> (taxista.IdTaxista);

			if (taxistaDB == null)
				throw HttpError.NotFound (
					Application.ErrorMessage.NotFound.Fmt (
					SingleTaxistaAbsUri.Fmt (taxista.IdTaxista))
				);
				
			return  new TaxistaResource (taxistaDB);
		}

		public ServicoTaxiCollection Get (TaxistaServicos servicos)
		{
			var taxista = GetTaxista (servicos.IdTaxista);
			
			return new ServicoTaxiCollection (taxista.Servicos, 
			                                  SingleTaxistaAbsUri.Fmt (servicos.IdTaxista) + ServicoTaxiWS.ServicoTaxiBaseUri);
		
		}

		static void MapDTO (ITaxista taxistaDB, TaxistaDTO novoTaxista)
		{
			taxistaDB.CAP = novoTaxista.CAP ?? taxistaDB.CAP;
			taxistaDB.Email = novoTaxista.Email ?? taxistaDB.Email;
			taxistaDB.Idade = novoTaxista.Idade ?? taxistaDB.Idade;
			taxistaDB.PrimeiroNome = novoTaxista.PrimeiroNome ?? taxistaDB.PrimeiroNome;
			taxistaDB.Telefone = novoTaxista.Telefone ?? taxistaDB.Telefone;
			taxistaDB.IdAplicacao = novoTaxista.IdAplicacao ?? taxistaDB.IdAplicacao;
		}

		public static ITaxista GetTaxista (string taxistaID)
		{
			var taxistaDB = BLFacade.LoadEntity<ITaxista> (taxistaID);
			if (taxistaDB == null)
				throw HttpError.NotFound (
					Application.ErrorMessage.NotFound.Fmt (
					SingleTaxistaAbsUri.Fmt (taxistaID))
				);
				
			return taxistaDB;
		}
	}
}

