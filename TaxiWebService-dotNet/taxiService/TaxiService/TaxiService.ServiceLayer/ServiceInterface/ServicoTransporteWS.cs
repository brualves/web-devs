using System;
using ServiceStack.ServiceInterface;
using TaxiService.ServiceLayer.Resources;
using TaxiService.ServiceLayer.Model;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.BusinessLayer;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;

namespace TaxiService.ServiceLayer.ServiceInterface
{
	public class ServicoTransporteWS : Service
	{


		public static string ServiceName = "Servico Web Servico Transporte";
		public static string ServicoTransporteBaseUri = "/servicotransportes";
		public static string ServicoTransporteAbsUri = Application.Service.Config.BaseUrl + ServicoTransporteBaseUri;
		public static string SigleServicoTransporteUri = ServicoTransporteBaseUri + "/{0}";
		public static string SigleServicoTransporteAbsUri = ServicoTransporteAbsUri + "/{0}";

		public ServicoTransporteResource Get (ServicoTransporte servico)
		{

			return new ServicoTransporteResource (GetServicoTransporte (servico));
		}

		public ServicoTransporteResource Delete (ServicoTransporte servico)
		{

			var servicoDB = BLFacade.RemoveEntity<IServicoTransporte>
				(servico.IdServicoTransporte); 
			if (servicoDB == null)
				throw HttpError.NotFound (Application.ErrorMessage.NotFound);

			return new ServicoTransporteResource (servicoDB); 
		}

		public ServicoTransporteResource Post (NovoServicoTransporte novoServico)
		{

			return NovoServicoTransporte (novoServico, Response); 
		
		}

		public static ServicoTransporteResource NovoServicoTransporte 
			(NovoServicoTransporte servico, IHttpResponse response)
		{
				

			var servicoTransporte = BLFacade.CreateEntity<IServicoTransporte> (String.Empty);

			MapDto (servicoTransporte, servico);

			if (! BLFacade.InsertEntity (servicoTransporte))
				throw HttpError.Conflict (Application.ErrorMessage.Conflit);

			response.AddHeader (HttpHeaders.Location, 
			                    "Teste");

			response.StatusCode = 201;

			return new ServicoTransporteResource (servicoTransporte);

		}

		static void MapDto (IServicoTransporte servicoTransporte, NovoServicoTransporte servico)
		{
			var servicoTaxi = BLFacade.LoadEntity<IServicoTaxi> (servico.IdServicoTaxi);
			
			var solicitacaoTransporte = BLFacade.LoadEntity<ISolicitacaoTransporte> (servico.IdSolicitacao);
			
			if (solicitacaoTransporte == null)
				throw HttpError.Conflict (Application.ErrorMessage.Conflit);

			servicoTransporte.SolicitacaoTransporte = solicitacaoTransporte;
			servicoTransporte.ServicoTaxi = servicoTaxi;
			servicoTransporte.DistanciaTempo = servico.TimeDistance;

		}

		public static IServicoTransporte GetServicoTransporte (ServicoTransporte servico)
		{

			var servicoDB = BLFacade.LoadEntity<IServicoTransporte> (servico.IdServicoTransporte);
			if (servicoDB == null)
				throw HttpError.NotFound (Application.ErrorMessage.NotFound);

			return servicoDB;
		}
	}
}

