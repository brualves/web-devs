using System;
using ServiceStack.ServiceInterface;
using TaxiService.ServiceLayer.Model;
using TaxiService.BusinessLayer;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;
using TaxiService.BusinessLayer.Notification;
using ServiceStack.Common.Web;
using TaxiService.ServiceLayer.Resources;
using System.Net;
using TaxiService.ServiceLayer;
using ServiceStack.ServiceHost;

namespace TaxiService.ServiceLayer.ServiceInterface
{
	public class UtenteWS : Service
	{
		public static string ServiceName = "Servico Web Utente";
		public static string UtenteBaseUri = "/utentes";
		public static string UtenteBaseAbsUri = Application.Service.Config.BaseUrl + UtenteBaseUri;
		public static string SigleUtenteUri = UtenteBaseUri + "/{0}";
		public static string SigleUtenteAbsUri = UtenteBaseAbsUri + "/{0}";
		//		public Utente Put( SetUtenteAppID utente ){
		//
		//			if (utente == null || string.IsNullOrEmpty (utente.IdUtente) ||
		//			    string.IsNullOrEmpty (utente.ApplicationId )
		//			    )
		//				throw new ArgumentException ("Campos obrigatórios vazios");	
		//			
		//			var utenteDB = BLFacade.LoadEntity<IUtente>( utente.IdUtente);
		//			
		//			if (utenteDB == null)
		//				return null;
		//			
		//			utenteDB.ApplicationId = utente.ApplicationId;
		//			
		//			if (! BLFacade.UpdateEntity<IUtente> (utenteDB))
		//				throw new ApplicationException ();
		//			
		//			return UtenteResponse (utenteDB );
		//		}
		public  UtenteResource Get (Utente utente)
		{
		
			return new UtenteResource (GetUtenteDB (utente.IdUtente));
	

		}

		public  UtenteResource Post (Utente utente)
		{

			var utenteDB = GetUtenteDB (utente.IdUtente);

			MapDto (utenteDB, utente);

			if (! BLFacade.UpdateEntity<IUtente> (utenteDB))
				throw HttpError.Conflict (Application.ErrorMessage.Conflit);

			return new UtenteResource (utenteDB);

		}

		public  Object Get (ColecaoUtentes utente)
		{
			IList<IUtente> items = BLFacade.ListEnity<IUtente> ();

			return new UtenteCollection (items);
			//base.RequestContext.ToOptimizedResult (new UtenteCollection (items));
			
				
			
		}

		public  UtenteResource Delete (Utente utente)
		{
			 
			var utenteDB = BLFacade.RemoveEntity<IUtente> (GetUtenteDB (utente.IdUtente));

			return new UtenteResource (utenteDB);

		}

		public  UtenteResource Post (ColecaoUtentes utente)
		{

			var newUtente = BLFacade.CreateEntity<IUtente> (utente.IdUtente);

			MapDto (newUtente, utente);

			if (! BLFacade.InsertEntity<IUtente> (newUtente))
				throw HttpError.Conflict (Application.ErrorMessage.Conflit);

			var resource = new UtenteResource (newUtente);	
			
			Response.AddHeader (
				HttpHeaders.Location, resource.GetSelfLink ().ToString ());
			
			Response.StatusCode = (int)HttpStatusCode.Created;
	
			return 	resource;

		}

		public static void MapDto (IUtente utenteDB, UtenteDTO utenteDTO)
		{
		
			utenteDB.PrimeiroNome = utenteDTO.PrimeiroNome;
			utenteDB.UltimoNome = utenteDTO.UltimoNome;
			utenteDB.Email = utenteDTO.Email;
			utenteDB.IdAplicacao = utenteDTO.IdAplicacao;
			utenteDB.Telefone = utenteDTO.Telefone;
			utenteDB.PassWord = utenteDTO.PassWord;
		}

		public static IUtente GetUtenteDB (string username)
		{
			var utenteDB = BLFacade.LoadEntity<IUtente> (username);
			if (utenteDB == null)
				throw  HttpError.NotFound (String.Format (Application.ErrorMessage.NotFound, "Utente"));

			return utenteDB;

		}
		//		public ServicoTransporteResponse Get(GetServicoUtente servico ){
		//
		//			if( servico == null || string.IsNullOrEmpty (servico.IdUtente ) 
		//			   || string.IsNullOrEmpty (servico.IdServico ) )
		//				throw new ArgumentException ("Campos obrigatorios vazio");
		//
		//			var utente = BLFacade.LoadEntity<IUtente> (servico.IdUtente);
		//
		//			if (utente == null)
		//				return null;
		//			var tr = utente.GetServico (servico.IdServico );
		//			if (tr == null)
		//				return null;
		//
		//			return ServicoTransporteService.ServiceResponse (tr);
		//		
		//		}
		//
		//		public IList<ServicoTransporte> Get ( AllServicosUtente servicos){
		//
		//			if( servicos == null || string.IsNullOrEmpty (servicos.IdUtente ))
		//				throw new ArgumentException ("IdUtente vazio");
		//
		//			var utente = BLFacade.LoadEntity<IUtente> (servicos.IdUtente);
		//
		//			if (utente == null) 
		//				return null;
		//
		//			var tranportes = utente.Transportes;
		//
		//			return ServicoTransporteService.ListServicoTransporte ( tranportes);
		//
		//
		//		}
		//	
		//		public ServicoTransporte Post(PedidoTransporte pedido){
		//
		//			if(pedido == null || string.IsNullOrEmpty( pedido.IdUtente) )
		//				throw new ArgumentException ("IdUtente vazio");
		//
		//
		//			var utenteDB = BLFacade.LoadEntity<IUtente> (pedido.IdUtente);
		//			if (utenteDB == null)
		//				return null;
		//
		//			var idServico = utenteDB.SolicitarServico (pedido.Latitude,
		//			                                           pedido.Longitude,
		//			                                           pedido.MinRate,
		//			                                           pedido.PontoReferencia,			                                        
		//			                                           (pedido.TimeOut == null ) ? RQManager.Config.MaxRequestTimeOut : (int) pedido.TimeOut );
		//			                                        			                                        
		//
		//			return new ServicoTransporte{ IdServico = idServico };
		//		}
		//
		//
		//		public ServicoTransporteResponse Put(AvaliarServico avaliar){
		//
		//			if(avaliar == null || string.IsNullOrEmpty( avaliar.IdUtente) || string.IsNullOrEmpty(avaliar.IdServico) ||
		//			   avaliar.Rating == null )
		//				throw new ArgumentException ("Campos obrigatórios vazio");
		//
		//			var utenteDB = BLFacade.LoadEntity<IUtente> (avaliar.IdUtente);
		//			if (utenteDB == null)
		//				return null;
		//			var servico = utenteDB.GetServico ( avaliar.IdServico );
		//
		//			if (servico == null)
		//				return null;
		//
		//			if (servico.Estado != EstadoPedido.Concluido)
		//				return null;
		//
		//			servico.Rating = ( avaliar.Rating < 2 ) ? 2 : avaliar.Rating;
		//			servico.Estado = EstadoPedido.Avaliado;
		//
		//			var updadeted = BLFacade.UpdateEntity<IServicoTransporte>(servico );
		//			if (!updadeted) 
		//				throw new ApplicationException ();
		//
		//			return ServicoTransporteService.ServiceResponse(servico);
		//
		//		}
		//
		//		public static UtenteResource UtenteResorce (IUtente utente)
		//		{
		//			return(UtenteResource)new UtenteResource {
		//				nomeUtilizador 	= utente.NomeUtilizador,
		////				IdUtente = utente.Key,
		//				primeiroNome 	= utente.PrimeiroNome,
		//				email    		= utente.Email,
		//				idAplicacao 	= utente.IdAplicacao
		//			}.AddLink (new Link { href = new Uri("http://localhost/api/utentes/" + utente.Key) }, "solicitarServicos");
		//		}
	}
}

