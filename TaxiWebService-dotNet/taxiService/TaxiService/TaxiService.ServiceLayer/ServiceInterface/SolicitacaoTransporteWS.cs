using System;
using ServiceStack.ServiceInterface;
using TaxiService.ServiceLayer.Model;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;
using TaxiService.BusinessLayer;
using TaxiService.BusinessLayer.Notification;
using ServiceStack.Common.Web;
using TaxiService.ServiceLayer.Resources;
using ServiceStack.ServiceHost;

namespace TaxiService.ServiceLayer.ServiceInterface
{
	public class SolicitacaoTransporteWS : Service
	{
		
		
		public static readonly string SolicitacaoTransporteBaseUri = "/solicitacaotransportes";
		public static readonly string SolicitacaoTransporteAbsUri = Application.Service.Config.BaseUrl + SolicitacaoTransporteBaseUri;
		public static readonly string SingleSolicitacaoTrBaseUri = SolicitacaoTransporteBaseUri + "/{0}";
		public static readonly string SingleSolicitacaoTrAbsUri = SolicitacaoTransporteAbsUri + "/{0}";

		public SolicitacaoResource Get (SolicitacaoTransporte solicitacao)
		{

			return GetSolicitacaoTransporte (solicitacao);
	
		}

		public SolicitacaoTrCollection Get (ColecaoSolicitacaoTransporte queryParam)
		{
		
			return ListarSolicitacaoTransporte (queryParam);
		}

		public SolicitacaoResource Post (SolicitarTransporte solicitacao)
		{
			return SolicitarServicoTransporte (solicitacao, Response);	
			
		}

		public ServicoTransporteResource Get (Confirmacao confirmacao)
		{
			var solicitacao = BLFacade.LoadEntity<ISolicitacaoTransporte> (confirmacao.IdSolicitacao);
			if (solicitacao == null)
				throw HttpError.NotFound (Application.ErrorMessage.NotFound);

			confirmacao.IdServicoTransporte = solicitacao.Key;

			return  new ServicoTransporteResource (
				ServicoTransporteWS.GetServicoTransporte (confirmacao));
		}

		public ServicoTransporteResource Post (ConfirmarSolicitacao confirmacao)
		{
			var solicitacao = BLFacade.LoadEntity<ISolicitacaoTransporte> (confirmacao.IdSolicitacao);
			if (solicitacao == null)
				throw HttpError.NotFound (Application.ErrorMessage.NotFound);

			return ServicoTransporteWS.NovoServicoTransporte (confirmacao, Response);
			
		}

		public static SolicitacaoTrCollection ListarSolicitacaoTransporte (ColecaoSolicitacaoTransporte queryParam)
		{
			var items = BLFacade.ListEnity<ISolicitacaoTransporte> ();

			return new SolicitacaoTrCollection (items);
		}

		public static SolicitacaoResource GetSolicitacaoTransporte (SolicitacaoTransporte solicitacao)
		{
			var servico_db = BLFacade.LoadEntity<ISolicitacaoTransporte> (solicitacao.IdSolicitacao);
			
			if (servico_db == null) 
				throw HttpError.NotFound (
					String.Format (Application.ErrorMessage.NotFound, 
				                String.Format (SingleSolicitacaoTrAbsUri, servico_db.Key))); 
		
			return new SolicitacaoResource (servico_db);
		}

		public static SolicitacaoResource SolicitarServicoTransporte (SolicitarTransporte solicitacao, IHttpResponse response)
		{
			//Garantias:  
			// o utente existe e  tem permissoes  para solicitar o servico 
			// 

			var solicitacaoDB = BLFacade.CreateEntity<ISolicitacaoTransporte> (String.Empty);

			MapDTO (solicitacaoDB, solicitacao);			 

			if (! BLFacade.InsertEntity<ISolicitacaoTransporte> (solicitacaoDB))
				throw new ApplicationException ("Solicitar transporte exception");

			var resource = new SolicitacaoResource (solicitacaoDB);

			//informar o taxista do novo recurso 

			GcmNotifier.Instance ().AddNotification (
				new TaxistaAppNotifier (
				(taxista) => {
				return  taxista.Rating >= ((solicitacaoDB.MinRate == 0) ? 1 : solicitacaoDB.MinRate);},
				new { 
					type 			= "RESOURCE_AVAILABLE",
					resourceType 	= "SolicitacaoTransporte",	 
					resoureLink 	=  resource.GetSelfLink ().href					
				},
				solicitacaoDB));

			response.AddHeader (HttpHeaders.Location, 
			                    String.Format (SingleSolicitacaoTrAbsUri, solicitacaoDB.Key));

			response.StatusCode = 201;

			return resource;
		
		}

		public static void MapDTO (
			ISolicitacaoTransporte solicitacao, SolicitarTransporte solicitacaoDTO
		)
		{
			solicitacao.Latitude = solicitacaoDTO.Latitude; 
			solicitacao.Longitude = solicitacaoDTO.Longitude;
			solicitacao.MinRate = solicitacaoDTO.MinRate;
			solicitacao.PontoReferencia = solicitacaoDTO.PontoReferencia;
			solicitacao.TimeOut = solicitacaoDTO.TimeOut;

			solicitacao.Utente = BLFacade.LoadEntity<IUtente> (solicitacaoDTO.IdUtente);
		}
	}
}

