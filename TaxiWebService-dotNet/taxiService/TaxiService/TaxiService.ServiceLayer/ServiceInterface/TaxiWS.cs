using System;
using ServiceStack.ServiceInterface;
using TaxiService.ServiceLayer.Model;
using TaxiService.BusinessLayer;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;
using ServiceStack.Common.Web;
using ServiceStack.Text;
using TaxiService.ServiceLayer.Resources;
using System.Net;
using ServiceStack.ServiceHost;

namespace TaxiService.ServiceLayer.ServiceInterface
{
	public class TaxiWS : Service
	{
		public const string ServiceName = "Taxi Service";
		public static string TaxiBaseUri = "/taxis";
		public static string TaxiBaseAbsUri = Application.Service.Config.BaseUrl + TaxiBaseUri;
		public static string SingleTaxiUri = TaxiBaseUri + "/{0}";
		public static string SingleTaxiAbsUri = TaxiBaseAbsUri + "/{0}";

		public TaxiResource Get (Taxi taxi)
		{
			return new TaxiResource (GetTaxi (taxi.IdTaxi));
		}

		public TaxiCollection Get (ColecaoTaxis query)
		{
			var taxis = BLFacade.ListEnity<ITaxi> ();
			
			return new TaxiCollection (taxis);
			
		}

		public TaxiResource Put (NovoTaxi novoTaxi)
		{
	
			return NovoTaxi (novoTaxi, Response);
			
		}

		public TaxiResource Post (UpdateTaxi upTaxi)
		{
			var taxi = GetTaxi (upTaxi.IdTaxi);
			
			MapDTO (taxi, upTaxi);
			
			if (! BLFacade.UpdateEntity <ITaxi> (taxi))
				throw HttpError.Conflict ("");
				
			return new TaxiResource (taxi);

		}

		public TaxiResource Delete (Taxi taxi)
		{
		
			var taxiDB = BLFacade.RemoveEntity <ITaxi> (taxi.IdTaxi);
			
			if (taxiDB == null)
				HttpError.NotFound (
					Application.ErrorMessage.NotFound.Fmt (
					SingleTaxiAbsUri.Fmt (taxi.IdTaxi)
				));
				
				
			return new TaxiResource (taxiDB);
		}

		public static TaxiResource NovoTaxi (NovoTaxi novoTaxi, IHttpResponse httpRes)
		{
			var taxiDB = BLFacade.CreateEntity<ITaxi> (novoTaxi.IdTaxi);
			
			MapDTO (taxiDB, novoTaxi);
			
			taxiDB.Zona = BLFacade.LoadEntity <IZona> (novoTaxi.IdZona);
			
			if (taxiDB.Zona == null)
				throw new HttpError (HttpStatusCode.BadRequest, new ArgumentException ());
			
			var empresa = BLFacade.LoadEntity<IEmpresa> (novoTaxi.IdEmpresa);
			
			if (taxiDB.Empresa == null)
				throw new HttpError (HttpStatusCode.BadRequest, new ArgumentException ());
			
			if (! empresa.AddTaxis (taxiDB))
				throw HttpError.Conflict ("");
				
			var resource = new TaxiResource (taxiDB);
			
			httpRes.AddHeader (HttpHeaders.Location, resource.GetSelfLink ().ToString ());
			
			httpRes.StatusCode = (int)HttpStatusCode.Created;
			
			return resource;
		}

		public static ITaxi GetTaxi (string taxiId)
		{
			var taxiDB = BLFacade.LoadEntity<ITaxi> (taxiId);
			if (taxiDB == null)
				HttpError.NotFound (
					Application.ErrorMessage.NotFound.Fmt (
					SingleTaxiAbsUri.Fmt (taxiId)
				)
				);
			return taxiDB;
		}

		public static void  MapDTO (ITaxi taxiDB, TaxiDTO novoTaxi)
		{
			taxiDB.Ano = Convert.ToDateTime (novoTaxi.Ano);
			taxiDB.Licenca = novoTaxi.Licenca;
			taxiDB.Lugares = novoTaxi.Lugares;
			taxiDB.Numero = novoTaxi.Numero;
			//taxiDB.Empresa = BLFacade.LoadEntity<IEmpresa>(novoTaxi.);
		}
	}
}

