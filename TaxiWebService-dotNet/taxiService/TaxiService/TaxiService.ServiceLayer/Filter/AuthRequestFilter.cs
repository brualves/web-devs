using System;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.Common.Web;

namespace TaxiService.ServiceLayer
{
	public class AuthRequestFilter : IHasRequestFilter
	{

		public static AuthRequestFilter Instance = new AuthRequestFilter ();

		public void RequestFilter (IHttpRequest httpReq, IHttpResponse httpRes, object requestDto)
		{
			//"authorization"
		
			var basicAuth = httpReq.GetBasicAuthUserAndPassword ();
			if (basicAuth == null) 
				throw 
					HttpError.Unauthorized ("Invalid BasicAuth credentials");					
							
		}

		public IHasRequestFilter Copy ()
		{
			return (IHasRequestFilter)MemberwiseClone ();
		}

		public int Priority {
			get { return -1;}
		}
	}
}

