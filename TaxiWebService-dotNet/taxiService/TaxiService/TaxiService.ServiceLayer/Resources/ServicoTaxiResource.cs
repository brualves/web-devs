using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.ServiceLayer.ServiceInterface;
using System.Collections.Generic;

namespace TaxiService.ServiceLayer.Resources
{
	public class ServicoTaxiResource : SingleResource<IServicoTaxi>
	{
	
		//		ITaxi 	 Taxi { get; set; }
		//
		//		ITaxista Taxista { get; set; }
		public string Inicio { get { return (_entity == null) ? null : _entity.Inicio.ToString (); } }

		public string Fim 	{ get { return (_entity == null) ? null : _entity.Fim.ToString (); } }

		public bool IsActive { get { return (_entity == null) ? false : _entity.IsActive; } }

		public ServicoTaxiResource (IServicoTaxi servico)
		:base(servico, ServicoTaxiWS.SigleServicoTaxiAbsUri)
		{
		}
	}

	public class ServicoTaxiCollection : CollectionResource<ServicoTaxiResource>
	{
	
		public ServicoTaxiCollection (IList<IServicoTaxi> items, string selfLink)
		:base( 
			selfLink,
			ToResourceList ( items, (servico)=>{ return new ServicoTaxiResource(servico);})
		)
		{
			
		}

		public ServicoTaxiCollection (IList<IServicoTaxi> items) : this ( items , ServicoTaxiWS.ServicoTaxiBaseAbsUri)
		{
			
		}
	}
}

