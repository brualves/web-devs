using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.BusinessLayer;
using System.Collections.Generic;
using TaxiService.ServiceLayer.ServiceInterface;

namespace TaxiService.ServiceLayer.Resources
{
	public class ZonaResource : SingleResource<IZona>
	{		
		const string TaxisLink = "taxis";

		public  string Designacao { get { return (_entity == null) ? null : _entity.Designacao; } }

		public  int?	Contigente { 
			get { return (_entity == null) ? null : _entity.Contigente;}
		}

		public  string Codigo { 
			get { return (_entity == null) ? null : _entity.Codigo;}
		}

		public ZonaResource (IZona zona)
			: base( zona, ZonaWS.SingleZonaAbsUri )
		{
			AddLink (AddLink (TaxiWS.TaxiBaseUri), TaxisLink);
		}
	}

	public class ZonaCollection : CollectionResource<ZonaResource >
	{
		public ZonaCollection (IList<IZona> items)
		: base( ZonaWS.ZonaBaseAbsUri, 
			ToResourceList ( items , (zona)=>{return new ZonaResource( zona);})
		 	 )
		{
		
		}
	}
}

