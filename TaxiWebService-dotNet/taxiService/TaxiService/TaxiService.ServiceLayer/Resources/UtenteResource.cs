using System;
using ServiceStack.ServiceInterface.ServiceModel;
using System.Collections.Generic;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.ServiceLayer.ServiceInterface;

namespace TaxiService.ServiceLayer.Resources
{
	public class UtenteResource : SingleResource<IUtente>
	{


		public UtenteResource (IUtente utente) 
			:base(utente, UtenteWS.SigleUtenteAbsUri )
		{
		}

		public  string  PrimeiroNome {
			get {
				return _entity.PrimeiroNome;
			}
		}

		public  string  UltimoNome { 
			get {
				return _entity.UltimoNome;
			}
		}

		public  string  NomeUtilizador {
			get {
				return _entity.NomeUtilizador;
			}
		}

		public  string  Email { 
			get {
				return _entity.Email;
			}
		}

		public  string  Telefone { 
			get {
				return _entity.Telefone;
			} 
		}

		public  string IdAplicacao { 
			get {
				return _entity.IdAplicacao;
			}
		}
		//		public static UtenteResource ToResource (IUtente item)
		//		{
		//			return new UtenteResource {
		//				primeiroNome = item.PrimeiroNome, 
		//				ultimoNome = item.UltimoNome, 
		//				nomeUtilizador = item.NomeUtilizador,
		//				email = item.Email,
		//				telefone = item.Telefone,
		//				idAplicacao = item.IdAplicacao
		//			};
		//		}
	}

	public class UtenteCollection : CollectionResource<UtenteResource>
	{

		public UtenteCollection (IList<IUtente> utentes) : 
			base( UtenteWS.UtenteBaseAbsUri,   
			 ToResourceList<IUtente>(utentes, ( utente ) => {return new UtenteResource ( utente );})
			)
		{
		
		}
		//protected override IList<UtenteResource> Resource { get { return _resources;}}
		//		public static List<UtenteResource> ToResourceList (IList<IUtente> utentes)
		//		{
		//			var items = new List<UtenteResource> ();
		//
		//			if (utentes == null)
		//				return items;
		//
		//			foreach (var item in utentes) 
		//				items.Add (UtenteResource.ToResource (item));
		//
		//			return items;
		//		}
	}
}

