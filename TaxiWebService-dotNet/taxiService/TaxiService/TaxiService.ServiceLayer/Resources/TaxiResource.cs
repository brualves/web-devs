using TaxiService.BusinessLayer.BOEntities;
using TaxiService.ServiceLayer.ServiceInterface;
using System.Collections.Generic;

namespace TaxiService.ServiceLayer.Resources
{
	public class TaxiResource : SingleResource<ITaxi>
	{
	
		public string IdTaxi { get { return (_entity == null) ? null : _entity.Key; } }

		public  string Matricula { get { return (_entity == null) ? null : _entity.Matricula; } }

		public  string Licenca { get { return (_entity == null) ? null : _entity.Licenca; } }

		public  int? Lugares { get { return (_entity == null) ? null : _entity.Lugares; } }

		public  string Ano { get { return (_entity == null) ? null : _entity.Ano.ToString (); } }

		public  int? Numero { get { return (_entity == null) ? null : _entity.Numero; } }
		//		public   Empresa { get; set; }
		//
		//		public  Zona Zona { get; set; }
		public TaxiResource (ITaxi taxi)
		: base( taxi,TaxiWS.SingleTaxiAbsUri)
		{
			if (_entity != null) {
				AddEmbeddedResource (new EmpresaResource (_entity.Empresa), "empresa");				
				AddEmbeddedResource (new ZonaResource (_entity.Zona), "zona");
			} 
		}
	}

	public class TaxiCollection : CollectionResource<TaxiResource >
	{
		public TaxiCollection (IList<ITaxi> items, string selfLink)
		: base(  
			selfLink,
		 	 ToResourceList ( items, 
		 	 (taxi)=>{ return new TaxiResource( taxi);}))
		{
			
		}

		public  TaxiCollection (IList<ITaxi> items) 
			: this (items, TaxiWS.TaxiBaseAbsUri)
		{
			
		}
	}
}

