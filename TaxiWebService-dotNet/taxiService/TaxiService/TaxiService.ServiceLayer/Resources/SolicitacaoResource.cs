using System;
using TaxiService.ServiceLayer.Model;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;
using TaxiService.ServiceLayer.ServiceInterface;

namespace TaxiService.ServiceLayer.Resources
{
	public class SolicitacaoResource : SingleResource<ISolicitacaoTransporte>
	{
		ISolicitacaoTransporte _solicitacao;

		public SolicitacaoResource (ISolicitacaoTransporte solicitacao)
			: base (solicitacao, SolicitacaoTransporteWS.SingleSolicitacaoTrAbsUri)
		{
			_solicitacao = solicitacao;
			AddEmbeddedResource (new UtenteResource (_solicitacao.Utente), "utente"); 

		}

		public double Latitude { get { return (_solicitacao == null) ? 0 : _solicitacao.Latitude; } }

		public double Longitude { get { return (_solicitacao == null) ? 0 : _solicitacao.Longitude; } }

		public string PontoReferencia { get { return (_solicitacao == null) ? null : _solicitacao.PontoReferencia; } }

		public int MinRate { get { return (_solicitacao == null) ? 0 : _solicitacao.MinRate; } }

		public int TimeOut { get { return (_solicitacao == null) ? 0 : _solicitacao.TimeOut; } }
	}

	public class SolicitacaoTrCollection : CollectionResource<SolicitacaoResource>
	{
		
		public SolicitacaoTrCollection (IList<ISolicitacaoTransporte> items)
			: base( SolicitacaoTransporteWS.SolicitacaoTransporteAbsUri, 
			
			ToResourceList(items, (solicitacao)=>{ return new SolicitacaoResource( solicitacao );} )
			)
		{
			
		}
		//		public static List<SolicitacaoResource> ToResourceList (IList<ISolicitacaoTransporte> items)
		//		{
		//		
		//			var resources = new List<SolicitacaoResource> ();
		//
		//			if (items == null)
		//				return resources;
		//
		//			foreach (var item in items)
		//				resources.Add (new SolicitacaoResource (item));	
		//			
		//			return resources;
		//			
		//		}
	}
}

