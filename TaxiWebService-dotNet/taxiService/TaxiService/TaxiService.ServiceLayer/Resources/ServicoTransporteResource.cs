using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.ServiceLayer.ServiceInterface;

namespace TaxiService.ServiceLayer.Resources
{
	public class ServicoTransporteResource : SingleResource<IServicoTransporte>
	{
		IServicoTransporte _servicoTr;

		public ServicoTransporteResource (IServicoTransporte servico)
			: base(servico, ServicoTransporteWS.SigleServicoTransporteAbsUri)
		{
			_servicoTr = servico;
		}

		public SolicitacaoResource SolicitacaoTransporte { 
			get{ return (_servicoTr == null) ? null : new SolicitacaoResource (_servicoTr.SolicitacaoTransporte);}
		}

		public int? Avalicao {
			get{ return (_servicoTr == null) ? null : _servicoTr.Avalicao;}

		}

		public int DistanciaTempo { 
			get{ return (_servicoTr == null) ? 0 : _servicoTr.DistanciaTempo;}
		}
	}
}

