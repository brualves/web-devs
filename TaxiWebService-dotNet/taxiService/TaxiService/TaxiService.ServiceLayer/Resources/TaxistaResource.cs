using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.ServiceLayer.ServiceInterface;
using System.Collections.Generic;

namespace TaxiService.ServiceLayer.Resources
{
	public class TaxistaResource : SingleResource<ITaxista>
	{
	
		const string ServicosLink = "servicostaxi";
		//		public  string  IdTaxista {
		//			get { return (_entity == null) ? null : _entity.Key;}
		//		}
		public string IdAplicacao { get { return (_entity == null) ? null : _entity.IdAplicacao; } }

		public string NomeUtilizador { get { return (_entity == null) ? null : _entity.Key; } }

		public  string  PrimeiroNome { get { return (_entity == null) ? null : _entity.PrimeiroNome; } }

		public  int?    Idade { get { return (_entity == null) ? null : _entity.Idade; } }

		public  string  CAP { get { return (_entity == null) ? null : _entity.CAP; } }

		public string Email { get { return (_entity == null) ? null : _entity.Email; } }

		public  string  Telefone { get { return (_entity == null) ? null : _entity.Telefone; } }
		//public string IdEmpresa { get; set; }
		public TaxistaResource (ITaxista taxista)
		: base(taxista, TaxistaWS.SingleTaxistaAbsUri )
		{
			if (_entity != null) {
				AddEmbeddedResource (new EmpresaResource (_entity.Empresa), "empresa");				
			} 
			
			AddLink (AddLink (ServicoTaxiWS.ServicoTaxiBaseUri), ServicosLink);
		
		}
	}

	public class TaxistaCollection : CollectionResource<TaxistaResource>
	{
		public TaxistaCollection (IList<ITaxista> items, string selfLink)
			: base( selfLink,
				ToResourceList ( items, (taxista)=>{return new TaxistaResource( taxista);}))
		{
			
		}

		public TaxistaCollection (IList<ITaxista> items)
		 : this(  items, TaxistaWS.TaxistaBaseAbsUri)
		{
			
		}
	}
}

