using System;
using ServiceStack.ServiceInterface.ServiceModel;
using System.Collections.Generic;
using TaxiService.BusinessLayer;
using ServiceStack.Text;

namespace TaxiService.ServiceLayer.Resources
{
	public interface IResource
	{
		Dictionary<string,Link> 	 _links	   { get; }

		Dictionary<string,IResource> _embedded { get; }

		ResponseStatus ResponseStatus { get; set; }

		Link GetSelfLink ();
	}

	public class Link
	{
		public Uri href { get; set; }

		public override string ToString ()
		{
			return href.ToString ();
		}
	}

	public abstract class Resource : IResource
	{

		Dictionary<string,Link> _linksResources;
		Dictionary<string,IResource> _embeddedResources;
		int _count;
		Link _selfLink;

		protected Resource (Uri meref)
		{			
			
			_linksResources = new Dictionary<string,Link > ();					
			_linksResources ["self"] = _selfLink = new Link { href =  meref };			

		}

		public Link GetSelfLink ()
		{
			return _selfLink;
		}

		public Link AddLink (string relLink)
		{
			return new Link { 
				href = new Uri ( GetSelfLink ().href.ToString () + relLink )
			};
		}

		public Dictionary<string, Link> _links {
			get { return _linksResources;}
		}

		public Dictionary<string, IResource> _embedded {
			get { return _embeddedResources;}
		}

		public void AddLink (Link uri, string link)
		{
			_linksResources [link] = uri;
		}

		public void AddEmbeddedResource (IResource resource, string rel)
		{
		
			if (_embeddedResources == null)
				_embeddedResources = new Dictionary <string,IResource> ();
	
			_embeddedResources [rel] = resource;
			
		}

		public ResponseStatus ResponseStatus { get; set; }
	}

	public abstract class SingleResource<T> : Resource where T : IBOEntity
	{
		protected T _entity;

		protected SingleResource (T entity, string uriTemplate)
			: base ( 
				new Uri( uriTemplate.Fmt (entity.Key))
			)
		{
			_entity = entity;
		}
		//		public static SingleResource NewInstance<K> (K entity)
		//		{
		//		
		//			return null;
		//		}
	}
}

