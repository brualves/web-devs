using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;
using TaxiService.BusinessLayer;
using TaxiService.ServiceLayer.ServiceInterface;

namespace TaxiService.ServiceLayer.Resources
{
	public class EmpresaResource : SingleResource<IEmpresa>
	{
		
		const string TaxistaLink = "taxistas";
		const string TaxisLink = "taxis";

		public string IdEmpresa { get { return  (_entity != null) ? _entity.Key : null; } }

		public string Designacao { get { return  (_entity != null) ? _entity.Designacao : null; } }

		public string Alvara { get { return  (_entity != null) ? _entity.Alvara : null; } }

		public string Telefone { get { return  (_entity != null) ? _entity.Telefone : null; } }
		//		public  ISet<Taxi> Taxis { get; set; }
		//
		//		public  ISet<Taxista> Taxista { get; set; }
		public EmpresaResource (IEmpresa empresa) :
			base( empresa, EmpresaWS.SigleEmpresaAbsUri)
		{
			//Link para a coleção de taxistas 
			AddLink (AddLink (TaxistaWS.TaxistaBaseUri), TaxistaLink);
			AddLink (AddLink (TaxiWS.TaxiBaseUri), TaxisLink);
				
		}
	}

	public class EmpresaCollection : CollectionResource<EmpresaResource>
	{
		
		public EmpresaCollection (IList<IEmpresa> items) 
			: base( EmpresaWS.EmpresaBaseAbsUri, 
					ToResourceList (
						items, 
						(empresa )=>{ return new EmpresaResource( empresa );})
			)
		{
		 	
		}
	}
}
