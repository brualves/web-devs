using System;
using TaxiService.ServiceLayer.Resources;
using System.Collections.Generic;
using TaxiService.BusinessLayer;
using ServiceStack.Text;

namespace TaxiService.ServiceLayer.Resources
{
	public interface ICollectionResource : IResource
	{

		int offset { get; }

		int limite { get; }

		int total  { get; }
	}

	public abstract class CollectionResource<T> : Resource, ICollectionResource  where T : IResource
	{

		string offsetString = "?offset={0}&limite{1}";
		//static string searchString = "?q{0}&limite{1}" ;
		int _offset;
		int _total;
		int _limite;
		List<T> _items;
		Link _self;
		protected List<T> _resources;

		protected CollectionResource (string meref, List<T> collection)
			: this( meref,collection, 0, 10)
		{
		}

		protected CollectionResource (string meref, List<T> collection, int offset, int limite)
			: base( new Uri( meref ) )
		{
			_items = new List<T> ();
			_resources = collection;
			_total = collection.Count;
			_limite = limite;
			_offset = offset;
			_self = new Link { href = new Uri (meref) };
			FillItems ();
		}

		void FillItems ()
		{

			int prevOfset = (_offset - _limite < 0) ? 0 : _offset - _limite;
			int nextOffset = (_offset + _limite > _total) ? _total : _offset + _limite;

			if (_offset > 0)
				AddLink (new Link { href =  new Uri( _self.href + offsetString.Fmt(prevOfset,limite))
				}, "prev");

			if (_offset + _limite < _total)
				AddLink (new Link { href =  new Uri( _self.href + offsetString.Fmt (nextOffset,limite) )
				}, "next");

			try {
			
				_items.AddRange (_resources.GetRange (offset, limite));

			} catch (ArgumentException) {
				_items.AddRange (_resources);
			}
		}

		public static List<T> ToResourceList<TK> (IList<TK> list, Func<TK,T> newInstance) 
			where TK : IBOEntity
		{
			var items = new List<T> ();

			if (list == null)
				return null;

			foreach (var item in list) 
				items.Add (newInstance.Invoke (item));

			return items;
		}

		public int offset { get { return _offset; } }

		public int limite { get { return _limite; } }

		public int total { get { return _total; } }

		public List<T> items { get { return _items; } }
	}
}

