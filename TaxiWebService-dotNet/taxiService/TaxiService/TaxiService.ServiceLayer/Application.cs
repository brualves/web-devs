using System;
using ServiceStack.ServiceInterface;
using TaxiService.ServiceLayer.Model;
using TaxiService.BusinessLayer;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;
using TaxiService.BusinessLayer.Notification;
using ServiceStack.Common.Web;
using TaxiService.ServiceLayer.Resources;

namespace TaxiService.ServiceLayer
{
	public static class Application
	{



		public static class Service
		{
			public static class Config
			{
				public static readonly String BaseUrl = "http://localhost:8080/api"	;								

			}
		}
		public static class ErrorMessage
		{
			public static readonly String NotFound = "O recurso [URL:{0}]pretendido não foi encontrado";

			public static readonly string Conflit = "Conflito na insercão do recurso" ;
				
		}

	}

}

