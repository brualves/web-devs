using System;
using ServiceStack.ServiceHost;
using System.Collections.Generic;
using TaxiService.ServiceLayer.Resources;

namespace TaxiService.ServiceLayer.Model
{
	public interface ISolicitacaoTr
	{
		string IdSolicitacao { get; }

		string IdUtente { get; }

		double Latitude{ get; }

		double Longitude{ get; }

		String PontoReferencia { get; }

		int MinRate { get; }

		int TimeOut { get; }
	}

	public abstract class SolicitacaoDTO : ISolicitacaoTr
	{

		public string IdSolicitacao { get; set; }

		public string IdUtente { get; set; }

		public double Latitude { get ; set ; }

		public double Longitude { get; set; }

		public string PontoReferencia { get; set; }

		public int MinRate { get; set; }

		public int TimeOut { get; set; }
	}

	[Route("/solicitacaotransportes/{IdSolicitacao}","GET,DELETE")]
	public class SolicitacaoTransporte : SolicitacaoDTO, IReturn<SolicitacaoResource>
	{
	}

	[Route("/solicitacaotransportes" ,"GET")]
	public class ColecaoSolicitacaoTransporte : CollectionDTO
	{
	}

	[Route("/solicitacaotransportes" ,"POST")]
	public class SolicitarTransporte : SolicitacaoDTO
	{
	}

	[Route("/solicitacaotransportes/{IdSolicitacao}/confirmacao" ,"POST")]
	public class ConfirmarSolicitacao : NovoServicoTransporte, IReturn<ServicoTransporteResource>
	{

	}

	[Route("/solicitacaotransportes/{IdSolicitacao}/confirmacao" ,"GET")]
	public class Confirmacao : ServicoTransporte, IReturn<ServicoTransporteResource>
	{
		public string IdSolicitacao { get; set; }
	}
	//public class SolicitarTransporte : SolicitacaoDTO, IReturn<SolicitacaoResource>{}
	//	[Route("/servicotransporte/{IdServico}","PUT")]
	//	public class UpdateServicoTransporte : IReturn<ServicoTransporteResponse>
	//	{
	//		public String IdServico { get; set;}
	//
	//	}
	//	[Route("/servicotransporte/{IdServico}/utente","GET")]
	//	public class GetUtenteServico: IReturn<Utente>
	//	{
	//		public String IdServico { get; set; }
	//	}
	//
	//	[Route("/servicotransporte/{IdServico}/taxista","GET")]
	//	public class GetTaxistaServico: IReturn<TaxistaResponse>
	//	{
	//		public String IdServico { get; set; }
	//	}
	//
	//	[Route("/servicotransporte/{IdServico}/response","POST")]
	//	public class ResponseServico: IReturn<ServicoTransporte>
	//	{
	//		public String IdServico 	{ get; set; }
	//
	//		public String IdServicoTaxi { get; set; }
	//
	//		public double? Latitude { get; set; }
	//
	//		public double? Longitude { get; set; }
	//
	//		public double? Distance { get; set; }
	//
	//	}
	//
	//	[Route("/servicotransporte/{IdServico}/confirmation","POST")]
	//	public class ConfirmService : IReturn<ServicoTransporte>
	//	{
	//		public String IdServico 	{ get; set; }
	//
	//		public String IdServicoTaxi { get; set; }
	//
	//		public bool   Confirme 		{ get; set; }
	//		
	//	}
	//	[Route("/servicos/{IdServico}","DELETE")]
	//	public class DeleteServicoTaxi : IReturn<ServicoTaxiResponse>
	//	{
	//		public String IdServico { get; set;}
	//	}
	//	[Route("/servicos/{IdServico}/close","PUT")]
	//	public class CloseServicoTaxi : IReturn<ServicoTaxiResponse>
	//	{
	//		public String IdServico { get; set;}
	//		public String IsActive { get; set;}
	//	}
	//
	//	[Route("/servicos/{IdServico}/taxi","GET")]
	//	public class GetServicoTaxiTaxi : IReturn<TaxiResponse>
	//	{
	//		public String IdServico { get; set;}
	//	}
	//
	//	[Route("/servicos/{IdServico}/taxista","GET")]
	//	public class GetServicoTaxiTaxista : IReturn<TaxistaResponse>
	//	{
	//		public String IdServico { get; set;}
	//	}
	//
	//	[Route("/servicos","GET")]
	//	public class AllServicos: IReturn<IList<ServicoTaxi>>{}
	//
	//	[Route("/servicos","POST")]
	//	public class AddServico : IReturn<ServicoTaxi>
	//	{
	//		public string IdTaxi 	{ get; set;}
	//		public string IdTaxista { get; set;}
	//	}
	//	public class ServicoTransporte
	//	{
	//		public string IdServico { get; set; }
	//	}
	//	
	//	public class ServicoTransporteResponse
	//	{
	//
	//		public  string  IdServico{ get; set; }
	//
	//		public 	Utente  Utente { get; set; }
	//
	//		public 	Taxi 	Taxi{ get; set; }
	//
	//		public 	Taxista Taxista { get; set; }
	//
	//		public 	double? Latitude{ get; set; }
	//
	//		public 	double? Longitude{ get; set; }
	//
	//		public	int? 	Rating{ get; set; }
	//
	//		public	int? 	Estado{ get; set; }
	//		
	//		public	String 	PontoReferencia { get; set; }
	//	
	//	}
}

