using System;
using ServiceStack.ServiceHost;
using System.Collections.Generic;
using TaxiService.ServiceLayer.Resources;

namespace TaxiService.ServiceLayer.Model
{
	public  abstract class EmpresaDTO
	{
		//		public string IdEmpresa { get; set; }
		public string Designacao { get; set; }

		public string Alvara 	 { get; set; }

		public string Telefone 	 { get; set; }
	}

	[Route("/empresas/{IdEmpresa}","GET,DELETET")]
	public class Empresa : IReturn<EmpresaResource>
	{
		public string IdEmpresa { get; set; }
	}

	[Route("/empresas/{IdEmpresa}","POST")]
	public class UpdateEmpresa : EmpresaDTO, IReturn<EmpresaResource>
	{
		public string IdEmpresa { get; set; }
	}

	[Route("/empresas","POST")]
	public class NovaEmpresa : EmpresaDTO //,IReturn<Empresa>
	{
	}

	[Route("/empresas","GET")]
	public class ColecaoEmpresa : CollectionDTO //,IReturn<Empresa>
	{
	}

	[Route("/empresas/{IdEmpresa}/taxis","GET")]
	public class TaxisEmpresa : CollectionDTO, IReturn<TaxiCollection>
	{
		public string IdEmpresa { get; set; }
	}

	[Route("/empresas/{IdEmpresa}/taxistas","GET")]
	public class TaxistasEmpresa : CollectionDTO, IReturn<TaxistaCollection>
	{
		public string IdEmpresa { get; set; }
	}
	//	[Route("/empresas/{IdEmpresa}/taxis","PUT")]
	//	public class NovoTaxi : TaxiDTO, IReturn<TaxiResource>
	//	{
	//	}
	//	[Route("/empresas/{IdEmpresa}/taxistas","POST")]
	//	public class AddTaxista : IReturn<Taxista>
	//	{
	//
	//		public String IdEmpresa { get; set; }
	//
	//		public string UserName { get; set; }
	//
	//		public string PassWord { get; set; }
	//
	//		public  string  Nome { get; set; }
	//
	//		public  int?    Idade { get; set; }
	//
	//		public  string  CAP { get; set; }
	//
	//		public  string  Telefone { get; set; }
	//	}
	//	[Route("/empresas/{IdEmpresa}","PUT")]
	//	public class UpdateEmpresa:IReturn<EmpresaResponse>
	//	{
	//		public string IdEmpresa { get; set; }
	//
	//		public string Alvara 	 { get; set; }
	//
	//		public string Telefone 	 { get; set; }
	//	}
	//	[Route("/empresas/{IdEmpresa}/taxis","GET")]
	//	public class AllTaxisEmpresa : IReturn<List<Taxi>>
	//	{
	//		public string IdEmpresa { get; set; }
	//
	//		public bool? IsActive { get; set; }
	//	}
	//
	//	[Route("/empresas/{IdEmpresa}/taxistas","GET")]
	//	public class AllTaxistasEmpresa : IReturn<List<Taxista>>
	//	{
	//		public string IdEmpresa { get; set; }
	//	}
	//
	//	[Route("/empresas/{IdEmpresa}/taxis/{IdTaxi}","GET")]
	//	public class GetEmpresaTaxi : IReturn<TaxiResponse>
	//	{
	//		public string IdEmpresa { get; set; }
	//
	//		public string IdTaxi { get; set; }
	//	}
	//
	//	[Route("/empresas/{IdEmpresa}/taxistas/{IdTaxista}","GET")]
	//	public class GetEmpresaTaxista : IReturn<TaxistaResponse>
	//	{
	//		public string IdEmpresa { get; set; }
	//
	//		public string IdTaxista { get; set; }
	//	}
}
