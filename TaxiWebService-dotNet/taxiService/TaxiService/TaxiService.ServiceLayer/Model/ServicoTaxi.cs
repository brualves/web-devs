using System;
using ServiceStack.ServiceHost;
using System.Collections.Generic;
using TaxiService.ServiceLayer.Resources;

namespace TaxiService.ServiceLayer.Model
{
	public abstract class ServicoTaxiDTO
	{
		public string IdTaxi 	{ get; set; }

		public string IdTaxista { get; set; }
	}

	[Route("/servicostaxi/{IdServico}","GET, DELETE")]
	public class ServicoTaxi : IReturn<ServicoTaxiResource>
	{
		public String IdServico { get; set; }
	}

	[Route("/servicostaxi","GET")]
	public class ColecaoServicosTaxi: CollectionDTO,IReturn<ServicoTaxiCollection>
	{
		
	}

	[Route("/servicostaxi/{IdServico}","POST")]
	public class UpdateServicoTaxi : IReturn<ServicoTaxiResource>
	{
		public String IdServico { get; set; }

		public string DataFim{ get; set; }
	}

	[Route("/servicostaxi","POST")]
	[Route("/taxistas/{IdTaxista}/servicostaxi","POST")]
	public class NovoServicoTaxi : ServicoTaxiDTO, IReturn<ServicoTaxiResource>
	{
	}
}

