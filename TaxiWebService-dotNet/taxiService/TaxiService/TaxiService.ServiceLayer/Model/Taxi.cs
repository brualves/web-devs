using System;
using ServiceStack.ServiceHost;
using System.Collections.Generic;
using TaxiService.ServiceLayer.Resources;

namespace TaxiService.ServiceLayer.Model
{
	public class TaxiDTO
	{
		
		public string IdTaxi { get; set; }
		//public  string Matricula { get; set; }
		public  string Licenca { get; set; }

		public string IdEmpresa { get; set; }

		public string IdZona { get; set; }

		public  int? Lugares { get; set; }

		public  string Ano { get; set; }

		public virtual int? Numero{ get; set; }
	}

	[Route("/taxis/{IdTaxi}","GET,DELETE")]
	public class Taxi : IReturn<TaxiResource>
	{
		public String IdTaxi { get; set; }
	}
	//	[Route("/taxis/{IdTaxi}/zona","GET")]
	//	public class GetTaxiZona : IReturn<TaxiResponse>
	//	{
	//		public String IdTaxi { get; set; }
	//	}
	//
	//	[Route("/taxis/{IdTaxi}/empresa","GET")]
	//	public class GetTaxiEmpresa //: IReturn<EmpresaResponse>
	//	{
	//		public String IdTaxi { get; set; }
	//	}
	//	[Route("/taxis/{IdTaxi}/servicos","GET")]
	//	public class GetTaxiServicos : IReturn<IList<ServicoTaxi>>
	//	{
	//		public String IdTaxi { get; set; }
	//	}
	[Route("/taxis","GET")]
	public class ColecaoTaxis: IReturn<TaxiCollection>
	{
	}

	[Route("/taxis","PUT")]
	[Route("/empresas/{IdEmpresa}/taxis","PUT")]
	public class NovoTaxi: TaxiDTO, IReturn<TaxiResource>
	{
		//public string IdTaxi { get; set; }
	}

	[Route("/taxis/{IdTaxi}","POST")]
	public class UpdateTaxi: TaxiDTO, IReturn<TaxiResource>
	{
		//public string IdTaxi { get; set; }
	}
	//
	//	public class TaxiResponse
	//	{
	//		public string IdTaxi { get; set; }
	//
	//		public  string Matricula { get; set; }
	//
	//		public  string Licenca { get; set; }
	//
	//		public  int? Lugares { get; set; }
	//
	//		public  DateTime Ano { get; set; }
	//
	//		public  int? Numero{ get; set; }
	//
	//		public  Empresa Empresa { get; set; }
	//
	//		public  Zona Zona { get; set; }
	//		//		public virtual Taxista Condutor { get; set; }
	//		//		public virtual ISet<ServicoTaxi> Servicos { get; set; } 
	//	}
	//
	//	public class Taxi
	//	{
	//		public string IdTaxi { get; set; }
	//	}
}

