using System;

namespace TaxiService.ServiceLayer.Model
{
	public class ResponseError
	{	
		
		public string Status { get; set; }

		public string Message {
			get;
			set;
		}

		public string DeveloperMessage {
			get;
			set;
		}

		public Uri MoreInfo {
			get;
			set;
		}
	}
}

