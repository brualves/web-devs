using System;
using ServiceStack.ServiceHost;
using System.Collections.Generic;
using TaxiService.ServiceLayer.Resources;

namespace TaxiService.ServiceLayer.Model
{
	public abstract class ZonaDTO
	{
		public  string Designacao { get; set; }

		public  int	Contigente{ get; set; }
		//public  string IdZona { get; set; }
	}

	[Route("/zonas/{IdZona}","GET,DELETE")]
	public class Zona :IReturn<ZonaResource>
	{
		public string IdZona { get; set; }
	}

	[Route("/zonas/{IdZona}","POST")]
	public class UpdateZona : ZonaDTO, IReturn<ZonaResource>
	{
		public string IdZona { get; set; }
	}

	[Route("/zonas","GET")]
	public class ColecaoZona: CollectionDTO, IReturn<ZonaCollection>
	{
	}

	[Route("/zonas","PUT")]
	public class NovaZona : ZonaDTO, IReturn<ZonaResource>
	{
		public  string IdZona { get; set; }
	}

	[Route("/zonas/{IdZona}/taxis","GET")]
	public class TaxisZona : CollectionDTO, IReturn<TaxiCollection>
	{
		public  string IdZona { get; set; }
	}
	//	[Route("/zonas/{IdZona}","PUT")]
	//	public class UpdateZona : IReturn<ZonaResponse>
	//	{
	//		public virtual string IdZona { get; set; }
	//
	//		public virtual int?	Contigente{ get; set; }
	//
	//		public virtual string Designacao { get; set; }
	//		//Rever 
	//	}
	//	[Route("/zonas/{IdZona}","DELETE")]
	//	public class DeleteZona : IReturn<ZonaResponse>
	//	{
	//		public string IdZona { get; set; }
	//	}
	//	[Route("/zonas/{IdZona}/taxis","GET")]
	//	public class AllTaxisZona : IReturn<List<Taxi>>
	//	{
	//		public string IdZona { get; set; }
	//	}
	//
	//	[Route("/zonas/{IdZona}/taxis/{IdTaxi}","GET")]
	//	public class GetZonaTaxi : IReturn<TaxiResponse>
	//	{
	//		public string IdZona { get; set; }
	//
	//		public string IdTaxi { get; set; }
	//	}
	//
	//	public class ZonaResponse
	//	{
	//		public virtual string Designacao { get; set; }
	//
	//		public virtual int?	Contigente{ get; set; }
	//
	//		public virtual string IdZona { get; set; }
	//		//public virtual ISet<Taxi> Taxis { get; set; }
	//	}
	//
	//	public class Zona
	//	{
	//		public string IdZona { get; set; }
	//	}
}

