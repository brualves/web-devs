using System;
using ServiceStack.ServiceHost;
using System.Collections.Generic;
using TaxiService.ServiceLayer.Resources;

namespace TaxiService.ServiceLayer.Model
{
	public abstract class TaxistaDTO
	{
	
		public  string  IdTaxista { get; set; }

		public string IdAplicacao { get; set; }
		//public string NomeUtilizador { get; set; }
		public  string  PrimeiroNome { get; set; }

		public  int?    Idade { get; set; }

		public  string  CAP { get; set; }

		public string Email { get; set; }

		public  string  Telefone { get; set; }

		public string IdEmpresa { get; set; }
	}

	[Route("/taxistas/{IdTaxista}","GET,DELETE")]
	public class Taxista : IReturn<TaxistaResource>
	{
		public String IdTaxista { get; set; }
	}

	[Route("/taxistas","GET")]
	public class ColecaoTaxistas: CollectionDTO, IReturn<TaxistaCollection>
	{
	}

	[Route("/taxistas","PUT")]
	[Route("/empresas/{IdEmpresa}/taxistas","PUT")]
	public class NovoTaxista: TaxistaDTO, IReturn<TaxistaResource>
	{
		public string PassWord { get; set; }
	}

	[Route("/taxistas/{IdTaxista}/servicostaxi","GET")]
	public class TaxistaServicos : CollectionDTO, IReturn<ServicoTaxiCollection>
	{
		public String IdTaxista { get; set; }
	}
	//
	//	[Route("/taxistas/{IdTaxista}/empresa","GET")]
	//	public class GetTaxistaEmpresa // : IReturn<EmpresaResponse>
	//	{
	//		public String IdTaxista { get; set; }
	//	}
	[Route("/taxistas/{IdTaxista}","POST")]
	public class UpdateTaxista: TaxistaDTO, IReturn<TaxistaResource>
	{
	}
}

