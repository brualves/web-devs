using System;
using System.Web.Routing;
using ServiceStack.ServiceHost;
using TaxiService.ServiceLayer.Resources;

namespace TaxiService.ServiceLayer.Model
{
	public abstract class ServicoTransporteDTO
	{
		public string IdSolicitacao { get; set; }

		public string IdServicoTaxi { get; set; }

		public int TimeDistance { get; set; }

		public int TimeOut { get; set; }
	}

	[Route("/servicotransportes/{IdServicoTransporte}","GET,DELETE")]
	public class ServicoTransporte : IReturn<ServicoTransporteResource >
	{
		public string IdServicoTransporte { get; set; }
	}

	[Route("/servicotransportes","GET")]
	public class ColecaoServicoTransporte : EmptyDTO
	{
	}

	[Route("/servicotransportes", "POST")]
	public class NovoServicoTransporte : ServicoTransporteDTO, IReturn<ServicoTransporteResource >
	{

	}
}

