using System;
using ServiceStack.ServiceHost;

using TaxiService.ServiceLayer.Resources;

namespace TaxiService.ServiceLayer.Model

{

	public abstract class UtenteDTO
	{
		public String IdUtente 		{ get; set;}
		public string PrimeiroNome 	{ get; set;}
		public string UltimoNome 	{ get; set;}
		public string Telefone 		{ get; set; }		
		public string IdAplicacao	{ get; set;}
		public string Email 		{ get; set;}
		public string PassWord 		{ get; set;}
	}

	[Route("/utentes/{IdUtente}","GET, DELETE, POST")]
	public class Utente : UtenteDTO,IReturn<UtenteResource>{}

	[Route ("/utentes","GET,POST")]
	public	class ColecaoUtentes   : UtenteDTO,IReturn<UtenteCollection>{}

	[Route("/utentes/{IdUtente/historicoservicos}", "GET")]
	public	class HistoricoServico : UtenteDTO,IReturn<UtenteCollection>{}
	

}

