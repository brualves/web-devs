using System;
using NUnit.Framework;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer.Test
{
	[TestFixture]
	public class AdmTaxistaTest : BaseFixture
	{
		private string _userName, 
					   _nome,
					   _cap,
//		 			   _telefone,
		 			   _passWord;
				int?   _idade; 


		protected override void OnSetup ()
		{
			base.OnSetup ();
			ConfigDB.Config ();
			_nome 	  = ConfigDB._taxista_db.PrimeiroNome;
			_cap 	  = ConfigDB._taxista_db.CAP;
			_userName = ConfigDB._taxista_db.NomeUtilizador;
//			_telefone = ConfigDB._taxista_db.Telefone;
			_passWord = ConfigDB._taxista_db.Telefone;
			_idade 	  = ConfigDB._taxista_db.Idade;
		}
	

		[Test]
		public	void RemoverTaxista()
		{

			int idbCount = BLFacade.CountEntity<ITaxista> (  );

			var taxista = BLFacade.RemoveEntity<ITaxista> (_userName);

			int edbCount = BLFacade.CountEntity<ITaxista> (  );

			var taxista_db = BLFacade.LoadEntity<ITaxista> (_userName);

			Assert.That ( (idbCount -1) == edbCount );
			Assert.IsNotNull ( taxista );
			Assert.IsNull ( taxista_db );


		}

		[Test]
		public void ActualizarPropriedadesTaxistas()
		{
//			var cap = 	ConfigDB._taxista_db.CAP;
//			var idade = ConfigDB._taxista_db.Idade;
//			var nome = ConfigDB._taxista_db.Nome;
//			var password = ConfigDB._taxista_db.PassWord;
//			var telefone = ConfigDB._taxista_db.Telefone;

			var taxista_bo =  BLFacade.LoadEntity<ITaxista> (_userName );

			taxista_bo.CAP 		= "1234566";
			taxista_bo.Idade 	= 50;
			taxista_bo.PrimeiroNome 	= "Jose da Cruz";
			taxista_bo.PassWord = "aaa33446";
			taxista_bo.Telefone = "5556667";

			var updated = BLFacade.UpdateEntity<ITaxista> (taxista_bo);

			DBService.Session.Flush ();
			taxista_bo = BLFacade.LoadEntity<ITaxista> (_userName);
			Assert.IsTrue (updated);
			Assert.IsFalse ( taxista_bo.CAP.Equals(_cap));
			Assert.IsFalse ( taxista_bo.Idade.Equals(_idade));
			Assert.IsFalse ( taxista_bo.PrimeiroNome.Equals(_nome));
			Assert.IsFalse ( taxista_bo.PassWord.Equals(_passWord));
//			Assert.IsFalse ( taxista_bo.Telefone.Equals(telefone));
		
		}

		[Test]
		public void SelectTaxistaInserido()
		{
			var taxista_bo = BLFacade.LoadEntity<ITaxista> (_userName);

			Assert.IsNotNull (taxista_bo);
			Assert.That (taxista_bo.Key.Equals(ConfigDB._taxista_db.NomeUtilizador));
			Assert.That (taxista_bo.PrimeiroNome.Equals(ConfigDB._taxista_db.PrimeiroNome));
			Assert.That (taxista_bo.CAP.Equals(ConfigDB._taxista_db.CAP));
//			Assert.That (taxista_bo.Telefone.Equals(ConfigDB._taxista_db.Telefone));
			Assert.That (taxista_bo.Idade.Equals(ConfigDB._taxista_db.Idade));
			Assert.That (taxista_bo.Empresa.Key .Equals(ConfigDB._taxista_db.Empresa.Designacao));
		
		}
//
//
		public void ListarTaxitasExistentes()
		{
			var icountdb = BLFacade.CountEntity<ITaxista> ();
		
			
			IList<ITaxista> taxistas_bo = BLFacade.ListEnity<ITaxista>();
	
			Assert.IsNotNull (taxistas_bo);
			Assert.That (taxistas_bo.Count > 0 );
	
			foreach (var taxista in taxistas_bo) {
				
				Assert.That (taxista.Key.Equals(ConfigDB._taxista_db.NomeUtilizador));
				Assert.That (taxista.PrimeiroNome.Equals(ConfigDB._taxista_db.PrimeiroNome));
				Assert.That (taxista.CAP.Equals(ConfigDB._taxista_db.CAP));
//				Assert.That (taxista.Telefone.Equals(ConfigDB._taxista_db.Telefone));

			}
			
			Assert.That (icountdb.Equals(taxistas_bo.Count ));

		}

		[Test]
		public void RatingTaxistaDiferenteZero()
		{
			var taxista = BLFacade.LoadEntity<ITaxista> (_userName);

			int? rating = taxista.Rating;

			Assert.That (rating, Is.GreaterThan(0));

		
		}


		[Test]
		public void RemoverTaxistaQueNaoExistente()
		{
			int idbCount = BLFacade.CountEntity<ITaxista> ();

			var taxista = BLFacade.RemoveEntity<ITaxista> ("FakeTaxista" );

			int edbCount = BLFacade.CountEntity<ITaxista> ();

			Assert.That ( idbCount == edbCount );
			Assert.IsNull (taxista );


		}
	
	}
}

