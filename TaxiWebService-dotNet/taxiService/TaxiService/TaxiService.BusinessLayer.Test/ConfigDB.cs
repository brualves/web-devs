using System;
using TaxiService.Domain.Entities;
using TaxiService.Domain.RepoInterface;
using TaxiService.DataLayer.Repository;
using TaxiService.DataLayer;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace TaxiService.BusinessLayer.Test
{
	public class ConfigDB
	{
		public static Empresa _empresa_db;
		public static Taxista _taxista_db;
		public static Zona _zona_db;
		public static Taxi _taxi_db;
		public static ServicoTaxi _servico_db;
		public static SolicitacaoTranporte _transporte_db;
		public static ServicoTransporte _servico_tr_db;
		public static Utente _utente_db;

		public static RespostaServico _respota_db;

		private static ISession _session;



		private static void BuildSchema ()
		{
			var cfg = NHConfigurator.Configuration;
			var schemaExport = new SchemaExport (cfg);
			schemaExport.Create (false, true);
		}

		public static void Config ()
		{
			BuildSchema ();
			_empresa_db = PersisteEmpresa (NewEmpresa ());
			_zona_db = PersistirZona (NewZona ());
			_taxista_db = PersisitirTaxista (NewTaxista ());
			_taxi_db = PersisteTaxi (NewTaxi ());

			_servico_db = PersisteServico (NewServico ());
			_utente_db = PersisteUtente (NewUtente ());
			_transporte_db = PersistirSolicitacaoTransporteTaxi (NewSolicitacaoTransporteTaxi ());
			_respota_db = PersistirResposta ( NewRespostaServico());
			_servico_tr_db = PersistirServicoTransporte (NewServicoTransporte());


		}

		public static IRepository Repository {
			get { return new NHRepository (Session);}
		}

		public static void CloseSession ()
		{
			if (_session != null && _session.IsOpen) {
				_session.Flush ();
				_session.Dispose ();
			}
		}

		private static ISession Session {
			get { 
				if (_session == null || ! _session.IsOpen)
					return (_session = NHConfigurator.Session);
				return _session;
			}
		}

		public static Empresa NewEmpresa ()
		{
			return new Empresa {Designacao = "EmpresaTest" , Alvara = "222 3333/444" , Telefone = "999888999" };
		}

		public static Empresa PersisteEmpresa (Empresa empresa)
		{
			Repository.Insert (empresa);
			Session.Flush ();
			Session.Evict (empresa);
			return empresa;
		}

		public static Zona NewZona ()
		{
			return new Zona (){ Designacao = "Lisboa", Contigente = 500, Codigo = "LSB" };
			
		}
		
		public static Zona PersistirZona (Zona zona)
		{
			Repository.Insert (zona);
			Session.Flush ();
			Session.Evict (zona);
			return zona;
		}

		public static Taxista NewTaxista ()
		{
			return new Taxista () { PrimeiroNome = "Taxista de Teste", NomeUtilizador = "tteste", Idade = 35, Telefone = "999888999", 
				Email = "teste@isel.pt", PassWord = "123467", CAP = "1234587", Empresa = _empresa_db ,
				ApplicationID = "APA91bFw_ugDkJxoTyqrU2BRBLDFSHF7s0nzCfsWQx4SWETnGVOV3jGfTR1oqsWqAr3fljgIq6a21nycqtJycGu7IL1f3ro-io_0xw9v_-o1eV41v-tsTcol_YJtfGcO314hhmTIpzM-Aj7cyBA9OsfGIXsseFPOKw"
			};
		}

		public static Taxista PersisitirTaxista (Taxista taxista)
		{
			Repository.Insert (taxista);
			Session.Flush ();
			Session.Evict (taxista);
			return taxista;

		}
		
		public static Taxi PersisteTaxi (Taxi taxi)
		{
			Repository.Insert (taxi);
			Session.Flush ();
			Session.Evict (taxi);
			return taxi;
		}

		public static ServicoTaxi NewServico ()
		{
		
			return new ServicoTaxi{ Taxi = _taxi_db, Taxista = _taxista_db, 
				InicioServico = DateTime.Now, 
				FimServico 	  = default(DateTime) };
		
		}

		public static ServicoTaxi PersisteServico (ServicoTaxi servico)
		{
			Repository.Insert (servico);
			Session.Flush ();
			Session.Evict (servico);
			return servico;

		}

		public static Taxi NewTaxi ()
		{
			return new Taxi (){ Matricula = "22-22-45", Licenca = "123456", Lugares = 4, //Ano = _ano, 
				Empresa  = _empresa_db, Zona = _zona_db, Numero = 5
			};
		}

		public static SolicitacaoTranporte NewSolicitacaoTransporteTaxi ()
		{
			return new SolicitacaoTranporte{ 
				Utente 			= _utente_db, 
				Estado 			= (int)EstadoPedido.Concluido, 
				Longitude 		=  -(80*3600 + 37*60 + 49.5),
				Latitude 		= -(5*3600 + 13*60 + 1.3),
				PontoReferencia = "Perto do ISEL" 

			};

		}
		
		public static SolicitacaoTranporte PersistirSolicitacaoTransporteTaxi (SolicitacaoTranporte ttaxi)
		{
			Repository.Insert (ttaxi);
			Session.Flush ();
			Session.Evict (ttaxi);
			return ttaxi;
		}

		public static Utente PersisteUtente (Utente utente)
		{
			Repository.Insert (utente);
			Session.Flush ();
			Session.Evict (utente);
			return utente;
		}

		public static ServicoTransporte PersistirServicoTransporte(ServicoTransporte servico)
		{
			Repository.Insert (servico);
			Session.Flush ();
			Session.Evict (servico);
			return servico;
		}

		public static ServicoTransporte NewServicoTransporte(){
			return new ServicoTransporte
			{
				SolicitacaoTranporte = _transporte_db,
				ServicoTaxi = _servico_db, 
				Avaliacao = 3,
				DistanciaTempo  = 10
			};
		
		}

		public static RespostaServico NewRespostaServico(){
			return new RespostaServico
			{
				ServicoTaxi = _servico_db,
				Latitude = 17,
				Longitude = 19, 
				SolicitacaoTranporte = _transporte_db,
			};	
		}

		public static RespostaServico PersistirResposta( RespostaServico resposta)
		{
			Repository.Insert (resposta);
			Session.Flush ();
			Session.Evict (resposta);
			return resposta;
		}

		public static Utente NewUtente ()
		{
			return new Utente (){ NomeUtilizador = "uteste", PassWord = "12345", Email = "uteste@isel.pt" ,  PrimeiroNome = "User Test" , 
				IdAplicacao = "APA91bHQyyBaL4D8hN0GXW9gU5PPG357U2kSD0e57YtZ_cWFLGxAuF5gZMBltZJnrzKdO9TDPhSz6o8YzD16_Cve3r23tDtHy2jiDyPCSKcTio7Mq9cNQffpg0MLAzmfuhamQa5-SyoZ9gCM8mthpANnWVRRv9MAGA"};
		}

	}
}

