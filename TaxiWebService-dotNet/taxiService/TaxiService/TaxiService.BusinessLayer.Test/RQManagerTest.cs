using System;
using NUnit.Framework;
using System.Threading;
using TaxiService.BusinessLayer.Notification;

namespace TaxiService.BusinessLayer.Test
{
	public class RQManagerTest : BaseFixture
	{

	
		string _iDServico, _appID;
		string _taxistaApp,_taxistaID;

		protected override void OnSetup ()
		{
			base.OnSetup ();
			RQManager.Clear ();
			ConfigDB.Config ();

			_iDServico = ConfigDB._transporte_db.Id.ToString ();
			_appID = ConfigDB._transporte_db.Utente.IdAplicacao;
			_taxistaApp = ConfigDB._taxista_db.ApplicationID;
			_taxistaID = ConfigDB._taxista_db.Id.ToString();
		
		}

		[Test]
		public void AdicionarUmNovoPedido ()
		{

//			var pedidosEmCurso = RQManager.Count;
//
//			RequestObj reqObj = null;
//
//			new Thread (() => {
//
//				reqObj = RQManager.AddRequest (_iDServico);
//
//				Thread.Sleep (5 * 1000);
//
//				var response = new TaxistaResponseMsg (){
//					AppID = _taxistaApp, 
//					Distance = 10,
//					TaxistaID = _iDServico
//				};
//				
//				RQManager.AddRequestResponse (_iDServico, response);
//				Thread.Sleep (5 * 1000);
//				
//				RQManager.AddRequestConfirmation (_iDServico, _iDServico, true);
//
//			}).Start ();
//
//			Thread.Sleep (1*1000);
//
//			var aposInsercao = RQManager.Count;
//
//			reqObj.WaitForEvent (RQManager.Config.MaxRequestTimeOut);
//
//			Assert.That (reqObj, Is.Not.Null);
//			Assert.That (pedidosEmCurso + 1, Is.EqualTo (aposInsercao));
//			Assert.That (reqObj.IdServico, Is.EqualTo (_iDServico));
//			Assert.That (reqObj.AppID, Is.Not.Null);
//			Assert.That (reqObj.Timeout, Is.EqualTo (default(int)));
////			Assert.That (reqObj.Estado, Is.EqualTo (EstadoPedido.EmProcessamento));
//			Assert.That (reqObj.TaxistaID, Is.Null);
//			Assert.That (reqObj.WaitEvent, Is.Not.Null);
//			Assert.That (reqObj.Estado, Is.EqualTo(EstadoPedido.Aceite));
//			//Assert.That ( reqObj.WaitEvent Is.False());
//
//		}
//	
//		[Test]
//		public void ProcessarPedidoInserido ()
//		{
//				
//			var rqObj = new RequestObj{
//				IdServico = _iDServico,
//				AppID = _appID,
//				Estado = EstadoPedido.EmProcessamento,
//				WaitEvent =  new AutoResetEvent(false)
//			};
//
//			var context = new RQContext (){ Request = rqObj, Response = new ResponseObj{} };
//
//			RQManager._requestQueue [_iDServico] = context;
//
//			new Thread (
//				() => {
//				RQManager.ProcessRequest (context, RQManager.Config.MaxRequestTimeOut, new MocNotifier ());
//			}).Start ();
//
//			//..1s 
//			Thread.Sleep (5000);
//
//			var response = new TaxistaResponseMsg(){
//				AppID = _taxistaApp, 
//				Distance = 10,
//				TaxistaID = _taxistaID
//			};
//
//			RQManager.AddRequestResponse ( _iDServico, response );
//
//			Thread.Sleep (5000);
//
//			RQManager.AddRequestConfirmation (_iDServico,_taxistaID, true );
//
//
//			rqObj.WaitForEvent (10*1000);
//
//			Assert.That ( rqObj.Estado, Is.EqualTo( EstadoPedido.Aceite));
//
//		
		}

		[Test]
		public void CancelarUmpedidoEmCurso ()
		{
			Assert.True (true, "Not Implemented!!!");
		}


		public class MocNotifier : INotifier
		{

			public string Notify (string dest, object data)
			{
				return "Teste";
			}

			public string Notify (System.Collections.Generic.IList<string> dest, object data)
			{
				return "Teste";
			}		

		}
	}
}

