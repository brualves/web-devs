using System;
using NUnit.Framework;
using TaxiService.BussinessLayer;
using System.Collections.Generic;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.Domain.Entities;
using TaxiService.Domain.RepoInterface;

namespace TaxiService.BusinessLayer.Test
{
	[TestFixture]
	public class AdmEmpresaTest : BaseFixture
	{

		string _designacao = "EmpresaTest";
		string _alvara = "222 3333/444";
		string _telefone = "999888999";

		protected override void OnSetup ()
		{
			base.OnSetup ();
			ConfigDB.Config ();
		}

		protected override void OnTeardown ()
		{
			base.OnTeardown ();
			ConfigDB.CloseSession ();
		}


		[Test]
		public void InserirNovaEmpresa(){
		
			var empresa = BLFacade.CreateEntity<IEmpresa> ("EmpresaTest2");

			empresa.Alvara 	   = "test 2013/06";
			empresa.Telefone   = _telefone;
			BLFacade.InsertEntity<IEmpresa> (empresa);
			DBService.Session.Flush ();
			var empresa_saved = BLFacade.LoadEntity<IEmpresa> ("EmpresaTest2");
			Assert.IsNotNull (empresa_saved );

		}

		[Test]
		public void SelecionarUmaEmpresaPelaDesignacao ()
		{

			IEmpresa empresa_by_name = BLFacade.LoadEntity<IEmpresa>(_designacao);

			Assert.IsNotNull (empresa_by_name);
			Assert.That ( empresa_by_name.Key.Equals(_designacao));
			Assert.That (empresa_by_name.Designacao.Equals (_designacao));
			Assert.That (empresa_by_name.Telefone.Equals (_telefone));
			Assert.That (empresa_by_name.Alvara.Equals (_alvara));

		}

		[Test]
		public void ListarEmpresasRegistadas ()
		{
			int idbcount = BLFacade.CountEntity<IEmpresa>();
			IList<IEmpresa> listaEmpresa = BLFacade.ListEnity<IEmpresa> ();

			Assert.IsNotNull (listaEmpresa);
		
			foreach (var empresa in listaEmpresa) {
				Assert.That (empresa.Designacao.Equals (_designacao));
				Assert.That (empresa.Telefone.Equals (_telefone));
				Assert.That (empresa.Alvara.Equals (_alvara));
			}
			Assert.That (idbcount == listaEmpresa.Count );
		}

		[Test]
		public void RemoverEmpresaPelaDesignacao ()
		{
			//numero de empresa na BD
			var idbcount = BLFacade.CountEntity<IEmpresa> ();

			var empresa_rem =  BLFacade.RemoveEntity<IEmpresa>(_designacao); //_admEmpresa.Remove ( _designacao );

			DBService.Session.Flush ();
		
			var edbcount 	 = BLFacade.CountEntity<IEmpresa> ();
			var empresa_name = BLFacade.LoadEntity<IEmpresa> (_designacao );

			Assert.That ( (idbcount - 1 ) == edbcount );
			Assert.IsNotNull (empresa_rem);
			Assert.IsNull (empresa_name);
		}


		[Test]
		public void RemoverEmpresaQueNaoExiste(){
		
			var idbcount 	 = BLFacade.CountEntity<IEmpresa> ();

			var empresa_rem =  BLFacade.RemoveEntity<IEmpresa>("EmpresaFake"); 
			
			DBService.Session.Flush ();
			
			var edbcount 	 = BLFacade.CountEntity<IEmpresa> ();
		
			Assert.IsNull (empresa_rem);	
			Assert.That ( idbcount == edbcount );
		
		
		
		}

		[Test]
		public void ActualizarPropriedadesDaEmpresa ()
		{
		
			var empresa = BLFacade.LoadEntity<IEmpresa> (_designacao);

			empresa.Telefone = "12345568";
			empresa.Alvara = "333 2008/123"; 

			var update = BLFacade.UpdateEntity<IEmpresa>(empresa);

			DBService.Session.Flush ();
	
			empresa = BLFacade.LoadEntity<IEmpresa> (_designacao);


			Assert.IsTrue (update);
			Assert.IsNotNull (empresa);
			//Assert.That (Object.ReferenceEquals(empresa, empresa_updated));
			Assert.That (empresa.Designacao.Equals (_designacao));
			Assert.IsFalse (empresa.Telefone.Equals (_telefone));
			Assert.IsFalse (empresa.Alvara.Equals (_alvara));

		}

		[Test]
		public void InserirNovoTaxistaAEmpresa ()
		{
			var cap = "12344566";
			var tele = "999888123";
			var nome = "taxi teste 2";
			var passWord = "123455";
			var idade = 60;
			var email = "taxista@isel.pt";

			var empresa = BLFacade.LoadEntity<IEmpresa>(_designacao);

			var taxitas_bo = BLFacade.CreateEntity<ITaxista>("ttest2");

			taxitas_bo.PrimeiroNome = nome;
			taxitas_bo.Idade = idade;
			taxitas_bo.Telefone = tele;
			taxitas_bo.PassWord = passWord;
			taxitas_bo.CAP = cap;
			taxitas_bo.Email = email;
	
			var inserted = empresa.AddTaxista (taxitas_bo);

			DBService.Session.Flush ();

			var taxistas = empresa.Taxistas; 

			var item = empresa.GetTaxista (taxitas_bo.Key);

			Assert.IsTrue (inserted);
			Assert.IsNotNull (taxistas);
			Assert.IsNotNull (item);
				
			Assert.That (taxitas_bo.Key.Equals (item.Key));
			Assert.That (taxitas_bo.PrimeiroNome.Equals (item.PrimeiroNome));
			Assert.That (taxitas_bo.Idade.Equals (item.Idade));
			Assert.That (taxitas_bo.Telefone.Equals (item.Telefone));
			Assert.That (taxitas_bo.PassWord.Equals (item.PassWord));
		}

		[Test]
		public void InserirNovoTaxiAEmpresa ()
		{

			var _codigo = "LSB";
			var matricula = "22-AA-44";
			var lugares = 6;
			var licenca = "2222/2013";
			var numero = 15;

			var empresa = BLFacade.LoadEntity<IEmpresa> ( _designacao );
			var zona = BLFacade.LoadEntity<IZona> ( _codigo );

				
			var taxi_bo = BLFacade.CreateEntity<ITaxi>(matricula);

			taxi_bo.Ano = DateTime.Now;
			taxi_bo.Lugares = lugares;
			taxi_bo.Licenca = licenca;
			taxi_bo.Numero = numero;

			taxi_bo.Zona = zona;

			var inserted = empresa.AddTaxis (taxi_bo);
			
			DBService.Session.Flush ();

			var taxis = empresa.Taxis;
			var item = empresa.GetTaxi (taxi_bo.Key);

			Assert.IsTrue (inserted);
			Assert.IsNotNull (taxis);
			Assert.IsNotNull (item);
			
			Assert.That (taxi_bo.Key.Equals (item.Key));
			Assert.That (taxi_bo.Licenca.Equals (item.Licenca));
			Assert.That (taxi_bo.Lugares.Equals (item.Lugares));
			Assert.That (taxi_bo.Ano.Equals (item.Ano));
			Assert.That (taxi_bo.Zona.Key.Equals (item.Zona.Key));
			Assert.That (taxi_bo.Numero.Equals (item.Numero));
		
		}

	}
}

