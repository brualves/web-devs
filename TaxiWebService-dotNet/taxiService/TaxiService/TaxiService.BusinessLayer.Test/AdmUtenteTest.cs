using System;
using NUnit.Framework;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;
using System.Threading;

namespace TaxiService.BusinessLayer.Test
{
	[TestFixture]
	public class AdmUtenteTest : BaseFixture
	{
		private string _userName;		 		
		private string _iD_servicoTaxi;


		protected override void OnSetup ()
		{
			base.OnSetup ();
			ConfigDB.Config ();
			_userName = ConfigDB._utente_db.NomeUtilizador;
			_iD_servicoTaxi = ConfigDB._servico_db.Id.ToString ();
		
		}
	

		[Test]

		public void SelecionarUtente(){

			var utente = BLFacade.LoadEntity<IUtente> ( _userName);

			Assert.That (utente, Is.Not.Null);			
			Assert.That (utente.Email, Is.Not.Null);
			Assert.That (utente.PrimeiroNome, Is.Not.Null);
			Assert.That (utente.NomeUtilizador, Is.Not.Null);
			//Assert.That ( utente.ServicosPendentes(), Is.False);
		}


//		[Test]
//
//		public void EnviarNotificacaoAoUtente(){
//		
//
////			double latitude = 38.7556759,longitude = -9.1162695;
////			var pref = "Largo do Isel";
////			var minRate = 2;
////			
//////			var idbcount = BLFacade.CountEntity<IServicoTransporte> ();
////			var utente = BLFacade.LoadEntity<IUtente> (_userName);
////			
////			//var autoEve = new AutoResetEvent (false);
////			
////			var servico = utente.SolicitarServico (latitude, longitude,minRate,pref, 30*1000);
////			
////			//gerar responsta 
////
////			var servico_db     = BLFacade.LoadEntity<IServicoTransporte> ( servico );
////			var servicoTaxi_db = BLFacade.LoadEntity<IServicoTaxi> ( _iD_servicoTaxi );
////
////			var serviceResponse = new ServiceResponse ();
////
////			Assert.IsTrue (false);
//
////			serviceResponse.RequestID = servico_db.Key; 
////			serviceResponse.Taxista   = servicoTaxi_db.Taxista;
////			serviceResponse.Rating 	  = servicoTaxi_db.Taxista.Rating;
////			serviceResponse.Distance = 10;
////			serviceResponse.Latitude  =  10;
////			serviceResponse.Longitude = 10;
//			
////			ServiceRequestManager.AddServiceResponse (servico, serviceResponse );
//
//			//autoEve.WaitOne ();
//			
//		}

		[Test]
		public void SolicitarServicoTransporte()
		{


			double latitude  = 38.7556759,
				   longitude = -9.1162695;
			var pref 		 = "Largo do Isel";
			var minRate 	 = 2;

			var idbcount = BLFacade.CountEntity<ISolicitacaoTransporte> ();
			var utente   = BLFacade.LoadEntity<IUtente> (_userName);


			var servicoID = utente.SolicitarServico (latitude,longitude,minRate,pref,default(int));

			var transportDB = BLFacade.LoadEntity<ISolicitacaoTransporte> ( servicoID.Key );

			var fdbcount = BLFacade.CountEntity<ISolicitacaoTransporte> ();

			Assert.That( servicoID, Is.Not.Null);
			Assert.That(transportDB, Is.Not.Null);
			Assert.That(fdbcount, Is.EqualTo(idbcount + 1 ));
			Assert.That(transportDB.Estado, Is.EqualTo( EstadoPedido.EmProcessamento));


		



			//var autoEve = new AutoResetEvent (false);

			//Assert.That (1, Is.EqualTo (2));

//			var servico = utente.SolicitarServico (latitude, longitude,minRate,pref, autoEve);
//
//
//
//
//			autoEve.WaitOne ();
//
//			var transporte = utente.GetServico (servico);
//
//			var edbcount = BLFacade.CountEntity<IServicoTransporte> ();
//
//			Assert.That ( servico, Is.Not.Null);
//			Assert.That ( transporte.Key, Is.EqualTo( servico));
//			Assert.That ( transporte.Estado, Is.EqualTo( EstadoPedido.EmProcessamento));
//			Assert.That ( transporte.Utente, Is.Not.Null);
//			Assert.That ( transporte.Taxi, Is.Null);
//			Assert.That ( transporte.Taxista, Is.Null);
//			Assert.That ( utente.ServicosPendentes(), Is.True );
//			Assert.That (idbcount + 1, Is.EqualTo( edbcount));

		}

		[Test]
		public void ListarTransportesAssociadosaAoUtente()
		{
			var utente = BLFacade.LoadEntity<IUtente> (_userName);
			var transportes = utente.Transportes;

			Assert.That (utente, Is.Not.Null);
			Assert.That (transportes, Is.Not.Null);
			Assert.That (transportes.Count, Is.GreaterThan(0));

		}

//		[Test]
//		public	void RemoverTaxista()
//		{
//
//			int idbCount = BLFacade.CountEntity<ITaxista> (  );
//
//			var taxista = BLFacade.RemoveEntity<ITaxista> (_userName);
//
//			int edbCount = BLFacade.CountEntity<ITaxista> (  );
//
//			var taxista_db = BLFacade.LoadEntity<ITaxista> (_userName);
//
//			Assert.That ( (idbCount -1) == edbCount );
//			Assert.IsNotNull ( taxista );
//			Assert.IsNull ( taxista_db );
//
//
//		}
//
//		[Test]
//		public void ActualizarPropriedadesTaxistas()
//		{
////			var cap = 	ConfigDB._taxista_db.CAP;
////			var idade = ConfigDB._taxista_db.Idade;
////			var nome = ConfigDB._taxista_db.Nome;
////			var password = ConfigDB._taxista_db.PassWord;
////			var telefone = ConfigDB._taxista_db.Telefone;
//
//			var taxista_bo =  BLFacade.LoadEntity<ITaxista> (_userName );
//
//			taxista_bo.CAP 		= "1234566";
//			taxista_bo.Idade 	= 50;
//			taxista_bo.Nome 	= "Jose da Cruz";
//			taxista_bo.PassWord = "aaa33446";
//			taxista_bo.Telefone = "5556667";
//
//			var updated = BLFacade.UpdateEntity<ITaxista> (taxista_bo);
//
//			DBService.Session.Flush ();
//			taxista_bo = BLFacade.LoadEntity<ITaxista> (_userName);
//			Assert.IsTrue (updated);
//			Assert.IsFalse ( taxista_bo.CAP.Equals(_cap));
//			Assert.IsFalse ( taxista_bo.Idade.Equals(_idade));
//			Assert.IsFalse ( taxista_bo.Nome.Equals(_nome));
//			Assert.IsFalse ( taxista_bo.PassWord.Equals(_passWord));
////			Assert.IsFalse ( taxista_bo.Telefone.Equals(telefone));
//		
//		}
//
//		[Test]
//		public void SelectTaxistaInserido()
//		{
//			var taxista_bo = BLFacade.LoadEntity<ITaxista> (_userName);
//
//			Assert.IsNotNull (taxista_bo);
//			Assert.That (taxista_bo.Key.Equals(ConfigDB._taxista_db.UserName));
//			Assert.That (taxista_bo.Nome.Equals(ConfigDB._taxista_db.Nome));
//			Assert.That (taxista_bo.CAP.Equals(ConfigDB._taxista_db.CAP));
////			Assert.That (taxista_bo.Telefone.Equals(ConfigDB._taxista_db.Telefone));
//			Assert.That (taxista_bo.Idade.Equals(ConfigDB._taxista_db.Idade));
//			Assert.That (taxista_bo.Empresa.Key .Equals(ConfigDB._taxista_db.Empresa.Designacao));
//		
//		}
////
////
//		public void ListarTaxitasExistentes()
//		{
//			var icountdb = BLFacade.CountEntity<ITaxista> ();
//		
//			
//			IList<ITaxista> taxistas_bo = BLFacade.ListEnity<ITaxista>();
//	
//			Assert.IsNotNull (taxistas_bo);
//			Assert.That (taxistas_bo.Count > 0 );
//	
//			foreach (var taxista in taxistas_bo) {
//				
//				Assert.That (taxista.Key.Equals(ConfigDB._taxista_db.UserName));
//				Assert.That (taxista.Nome.Equals(ConfigDB._taxista_db.Nome));
//				Assert.That (taxista.CAP.Equals(ConfigDB._taxista_db.CAP));
////				Assert.That (taxista.Telefone.Equals(ConfigDB._taxista_db.Telefone));
//
//			}
//			
//			Assert.That (icountdb.Equals(taxistas_bo.Count ));
//
//		}
//
//		[Test]
//		public void RatingTaxistaDiferenteZero()
//		{
//			var taxista = BLFacade.LoadEntity<ITaxista> (_userName);
//
//			int? rating = taxista.Rating;
//
//			Assert.That (rating, Is.GreaterThan(0));
//
//		
//		}
//
//
//		[Test]
//		public void RemoverTaxistaQueNaoExistente()
//		{
//			int idbCount = BLFacade.CountEntity<ITaxista> ();
//
//			var taxista = BLFacade.RemoveEntity<ITaxista> ("FakeTaxista" );
//
//			int edbCount = BLFacade.CountEntity<ITaxista> ();
//
//			Assert.That ( idbCount == edbCount );
//			Assert.IsNull (taxista );
//
//
//		}
	
	}
}

