using System;
using TaxiService.BusinessLayer.BOEntities;
using NUnit.Framework;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer.Test
{
	[TestFixture]
	public class AdmZonaTest : BaseFixture
	{

		string _codigo_db = "LSB",
			   _designacao_db = "Lisboa";
		int	   _contigente_db = 500;


		protected override void OnSetup ()
		{
			base.OnSetup ();
			ConfigDB.Config ();
		}

		[Test]
		public void InserirNovaZona()
		{

			int idbCount = BLFacade.CountEntity<IZona> ();

			IZona newZona = BLFacade.CreateEntity <IZona>( "ABT");
			newZona.Contigente = 600;
			newZona.Designacao = "Abrantes";
		
			bool added = BLFacade.InsertEntity (newZona);
	
			DBService.Session.Flush ();

			int edbCount = BLFacade.CountEntity<IZona> ();

			newZona = BLFacade.LoadEntity<IZona>("ABT");

			Assert.That (added);
			Assert.NotNull(newZona);
			Assert.That( ( idbCount + 1 ) == edbCount );



		}

		[Test]
		public void SelecionarZonaPorCodigo()
		{
			IZona zona_bo = BLFacade.LoadEntity<IZona> (_codigo_db);

			Assert.IsNotNull (zona_bo);
			Assert.That (zona_bo.Key.Equals(_codigo_db));
			Assert.That (zona_bo.Codigo.Equals(_codigo_db));
			Assert.That (zona_bo.Designacao.Equals(_designacao_db));
			Assert.That (zona_bo.Contigente.Equals(_contigente_db));
		}

		[Test]
		public void ActualizarPropriedadesDaZona()
		{
			IZona zona_bo = BLFacade.LoadEntity<IZona> (_codigo_db);

			zona_bo.Contigente = 600;
			zona_bo.Designacao = "Lisboa Campo GR";

			BLFacade.UpdateEntity<IZona> (zona_bo);
			DBService.Session.Flush ();

			zona_bo = BLFacade.LoadEntity<IZona> (_codigo_db);

			Assert.IsNotNull (zona_bo);
			Assert.That (zona_bo.Key.Equals(_codigo_db));
			Assert.That (zona_bo.Codigo.Equals(_codigo_db));
			Assert.IsFalse (zona_bo.Designacao.Equals(_designacao_db));
			Assert.IsFalse (zona_bo.Contigente.Equals(_contigente_db));

		}

		[Test]
		public void ListarZonasExistentes()
		{
			var icountdb = BLFacade.CountEntity<IZona> ();
		

			IList<IZona> zonas_bo = BLFacade.ListEnity<IZona> ();

			Assert.IsNotNull (zonas_bo);

			foreach (var zona in zonas_bo) {
	
				Assert.That (zona.Key.Equals(_codigo_db));
				Assert.That (zona.Codigo.Equals(_codigo_db));
				Assert.That (zona.Designacao.Equals(_designacao_db));
				Assert.That (zona.Contigente.Equals(_contigente_db));
			}

			Assert.That (icountdb.Equals(zonas_bo.Count));
		}


		[Test]
		public void RemoverZona(){

			int idbCount = BLFacade.CountEntity<IZona> ();

			var zona 	 = BLFacade.RemoveEntity<IZona> (_codigo_db);

			var zona_db  = BLFacade.LoadEntity<IZona> (_codigo_db);

			int edbCount = BLFacade.CountEntity<IZona> ();


			Assert.That ( (idbCount-1) == edbCount );
			Assert.IsNotNull ( zona);
			Assert.IsNull (zona_db);

		}

		[Test]
		public void ListarTaxisAssociadosAZona()
		{

			var taxi_db = ConfigDB._taxi_db;

			var zona = BLFacade.LoadEntity<IZona> (_codigo_db);

			var taxis = zona.Taxis;

			Assert.IsNotNull (taxis);

			foreach (var item in taxis) {

				Assert.That (item.Key.Equals (taxi_db.Matricula));
				Assert.That (item.Matricula.Equals (taxi_db.Matricula));
				Assert.That (item.Licenca.Equals (taxi_db.Licenca));
				Assert.That (item.Zona.Key.Equals (zona.Key));
				Assert.That (item.Ano.Equals (taxi_db.Ano));			
				Assert.That (item.Lugares.Equals (taxi_db.Lugares));
				Assert.That (item.Numero.Equals (taxi_db.Numero));
			}
		}

			
		[Test]
		public void ObterTaxiAssociadoAZona()
		{
				
			var taxi_db = ConfigDB._taxi_db;
				
			var zona = BLFacade.LoadEntity<IZona> (_codigo_db);
				
			var taxi = zona.GetTaxi( taxi_db.Matricula);
	
			Assert.IsNotNull ( taxi);
			Assert.That (taxi.Key.Equals( taxi_db.Matricula ));
			Assert.That (taxi.Matricula.Equals( taxi_db.Matricula ));
			Assert.That (taxi.Licenca.Equals(taxi_db.Licenca ));
			Assert.That (taxi.Zona.Key.Equals( zona.Key ));
			Assert.That (taxi.Ano.Equals( taxi_db.Ano));			
			Assert.That (taxi.Lugares.Equals( taxi_db.Lugares));
			Assert.That (taxi.Numero.Equals( taxi_db.Numero));

		}

	}
}

