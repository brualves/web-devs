using System;
using NUnit.Framework;
using TaxiService.Domain.Entities;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer.Test
{

	[TestFixture]
	public class AdmTaxiTest : BaseFixture
	{


		string _matricula,  _licenca;
		int? _lugares, _numero; 
		Zona _zona;
		DateTime _ano ;

		protected override void OnSetup ()
		{
			base.OnSetup ();

			ConfigDB.Config ();

			_matricula 	= ConfigDB._taxi_db.Matricula;
			_licenca 	= ConfigDB._taxi_db.Licenca;
			_lugares 	= ConfigDB._taxi_db.Lugares;
			_zona 		= ConfigDB._taxi_db.Zona;
			_ano		= ConfigDB._taxi_db.Ano;
			_numero     = ConfigDB._taxi_db.Numero;
			
		}

		[Test]
		public void SeleccionarTaxiPersistido()
		{
			var taxi_bo = BLFacade.LoadEntity<ITaxi> (_matricula );

			Assert.IsNotNull (taxi_bo );
			Assert.That (taxi_bo.Key.Equals( _matricula ));
			Assert.That (taxi_bo.Matricula.Equals( _matricula ));
			Assert.That (taxi_bo.Licenca.Equals(_licenca ));
			Assert.That (taxi_bo.Zona.Key.Equals( _zona.Codigo ));
			Assert.That (taxi_bo.Ano.Equals( _ano));
			Assert.That (taxi_bo.Lugares.Equals( _lugares));
			Assert.That (taxi_bo.Numero.Equals( _numero));

		}

		[Test]
		public void ListarTaxisPersistido()
		{
			var icountdb = BLFacade.CountEntity<ITaxi> ();
	
			
			IList<ITaxi> taxi_bo = BLFacade.ListEnity<ITaxi>();
			
			Assert.IsNotNull (taxi_bo);
			Assert.That (taxi_bo.Count > 0 );
			
			foreach (var item in taxi_bo) {
			
				Assert.IsNotNull (taxi_bo );
				Assert.That (item.Key.Equals( _matricula ));
				Assert.That (item.Matricula.Equals( _matricula ));
				Assert.That (item.Licenca.Equals(_licenca ));
				Assert.That (item.Zona.Key.Equals( _zona.Codigo ));
				Assert.That (item.Ano.Equals( _ano));			
				Assert.That (item.Lugares.Equals( _lugares));
				Assert.That (item.Numero.Equals( _numero));
		
			}
			Assert.That (icountdb.Equals(taxi_bo.Count));
		}

		[Test]
		public void RemoverTaxi(){

			int idbCount = BLFacade.CountEntity<ITaxi> ();

			var taxi = BLFacade.RemoveEntity<ITaxi> (_matricula);

			int edbCount = BLFacade.CountEntity<ITaxi> ();

			var taxi_bo = BLFacade.LoadEntity<ITaxi> ( _matricula);

			Assert.That ( (idbCount - 1) == edbCount );
			Assert.IsNotNull ( taxi );
			Assert.IsNull (taxi_bo);
		}

		[Test]
		public void RemoverTaxiQueNaoExiste(){
			
			int idbCount = BLFacade.CountEntity<ITaxi> ();
			
			var taxi = BLFacade.RemoveEntity<ITaxi> ("22-11-30");
			
			int edbCount = BLFacade.CountEntity<ITaxi> ();
						
			Assert.That ( idbCount == edbCount );
			Assert.IsNull ( taxi );
		}

		[Test]
		public void ActualizarPropriedadesTaxi()
		{
			var taxi_bo = BLFacade.LoadEntity<ITaxi> (_matricula );

			taxi_bo.Ano = DateTime.Now;
			taxi_bo.Licenca = "333/555";
			taxi_bo.Lugares = 10;
			taxi_bo.Numero = 20;

			var updated = BLFacade.UpdateEntity<ITaxi> (taxi_bo);
	
			DBService.Session.Flush ();

			taxi_bo = BLFacade.LoadEntity<ITaxi> (_matricula );

			Assert.IsNotNull (taxi_bo );
			Assert.IsTrue (updated);

			Assert.That (taxi_bo.Key.Equals( _matricula ));
			Assert.That (taxi_bo.Matricula.Equals( _matricula ));
			Assert.IsFalse (taxi_bo.Licenca.Equals(_licenca ));
			Assert.True (taxi_bo.Zona.Key.Equals( _zona.Codigo ));
			Assert.IsFalse (taxi_bo.Lugares.Equals( _lugares));
			Assert.IsFalse (taxi_bo.Ano.Equals( _ano));
			Assert.IsFalse (taxi_bo.Numero.Equals(_numero));

		}

	}
}

