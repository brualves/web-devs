using System;
using NUnit.Framework;
using TaxiService.BusinessLayer.BOEntities;

namespace TaxiService.BusinessLayer.Test
{
	public class AdmServicoTransporteTeste : BaseFixture
	{
	
		string _servicoID;
		//string _solicitacaoID;
		string _sTaxiID;

		string _utenteID;

		protected override void OnSetup ()
		{
			base.OnSetup ();
			ConfigDB.Config ();
			_servicoID = ConfigDB._servico_tr_db.Id.ToString();
			//_solicitacaoID = ConfigDB._transporte_db.Id.ToString ();
			_sTaxiID = ConfigDB._servico_db.Id.ToString ();
			_utenteID = ConfigDB._utente_db.NomeUtilizador;
		}

		[Test]
		public void ObterServicoPreviamenteInserido()
		{

			var servico_db = BLFacade.LoadEntity<IServicoTransporte> ( _servicoID);

			Assert.That ( servico_db, Is.Not.Null);
			Assert.That ( servico_db.SolicitacaoTransporte, Is.Not.Null );
			Assert.That ( servico_db.ServicoTaxi , Is.Not.Null );
		}

		[Test]
		public void AdicionarNovoServicoTransporte()
		{
			var solicitacao = BLFacade.CreateEntity<ISolicitacaoTransporte> (String.Empty);

			solicitacao.Latitude = 10;
			solicitacao.Longitude = 15;
			solicitacao.PontoReferencia = "Ao pe da ki";
			solicitacao.MinRate = 2;
			solicitacao.TimeOut = 3; 
			solicitacao.Utente = BLFacade.LoadEntity<IUtente> (_utenteID);

			BLFacade.InsertEntity<ISolicitacaoTransporte> (solicitacao);

//			var newResposta = BLFacade.CreateEntity<IRespostaServico> ( String.Empty );

			var servicoTaxi = BLFacade.LoadEntity<IServicoTaxi> (_sTaxiID);

//			newResposta.ServicoTaxi = servicoTaxi;
//			newResposta.Latitude = 19;
//			newResposta.Longitude = 89;
//			newResposta.SolicitacaoTranporte = solicitacao;
//			newResposta.Tipo = (int)TipoResposta.Confirmar;

//			BLFacade.InsertEntity<IRespostaServico> ( newResposta);

			var servico_new = BLFacade.CreateEntity<IServicoTransporte> (String.Empty);

			var initCount = BLFacade.CountEntity<IServicoTransporte> ();

			servico_new.Avalicao = 20;
			servico_new.SolicitacaoTransporte = solicitacao;
			servico_new.ServicoTaxi = servicoTaxi;
			servico_new.DistanciaTempo = 10;

			var inseterd = BLFacade.InsertEntity<IServicoTransporte> (servico_new);

			var endCount = BLFacade.CountEntity<IServicoTransporte> ();

			var servico_db = BLFacade.LoadEntity<IServicoTransporte> ( servico_new.Key );

			solicitacao = BLFacade.LoadEntity<ISolicitacaoTransporte> (solicitacao.Key);

			Assert.That ( servico_db, Is.Not.Null );
			Assert.That ( servico_db.ServicoTaxi, Is.Not.Null );
			Assert.That ( servico_db.SolicitacaoTransporte, Is.Not.Null );
			Assert.That (inseterd, Is.True);
			Assert.That ( endCount, Is.EqualTo(initCount + 1 ));

			Assert.That ( servico_db.ServicoTaxi.Key, Is.EqualTo( servicoTaxi.Key));
			Assert.That ( servico_db.SolicitacaoTransporte.Key, Is.EqualTo(solicitacao.Key ));

			//Assert.That ( solicitacao.ServicoTransporte.Key, Is.EqualTo(servico_db.Key));


		}

		[Test]
		public void RemoverServicoNaBD()
		{
			var initCount = BLFacade.CountEntity<IServicoTransporte> ();

			var removed = BLFacade.RemoveEntity<IServicoTransporte> (_servicoID);

			var endCount = BLFacade.CountEntity<IServicoTransporte> ();

			Assert.That (removed, Is.Not.Null );
			Assert.That (removed.Key, Is.EqualTo(_servicoID));
			Assert.That (endCount, Is.EqualTo( initCount - 1 ) );	

		}
	}
}

