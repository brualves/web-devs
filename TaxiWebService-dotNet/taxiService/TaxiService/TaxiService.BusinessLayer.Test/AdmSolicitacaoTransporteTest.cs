using System;
using NUnit.Framework;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer.Test
{
	[TestFixture]
	public class AdmSolicitacaoTransporteTest : BaseFixture
	{

		Guid _idTransporte;
	//	string _IdTaxi,_IdTaxista;
		string _IdUtente ;

		protected override void OnSetup ()
		{
			base.OnSetup ();
			ConfigDB.Config ();
			_idTransporte = ConfigDB._transporte_db.Id;
//			_IdTaxi    = ConfigDB._servico_db.Taxi.Matricula;
//			_IdTaxista = ConfigDB._servico_db.Taxista.UserName;
			_IdUtente = ConfigDB._utente_db.NomeUtilizador;
		}
	

		[Test]
		public void InserirNovoServicoTransporte(){
		
			var idbcount = BLFacade.CountEntity<ISolicitacaoTransporte> ();

			var transporte = BLFacade.CreateEntity<ISolicitacaoTransporte> (String.Empty);
			var utente = BLFacade.LoadEntity<IUtente> (_IdUtente);

			transporte.Estado = (EstadoPedido) 1;
			transporte.Latitude  =  4;
			transporte.Longitude =  5;
			transporte.Utente = utente;

			var inserted = BLFacade.InsertEntity<ISolicitacaoTransporte> (transporte);

			DBService.Session.Flush ();
		
			var edbcount = BLFacade.CountEntity<ISolicitacaoTransporte> ();

			var transporte_db = BLFacade.LoadEntity<ISolicitacaoTransporte> (transporte.Key);


			Assert.That ( inserted, Is.True);
			Assert.That ( transporte_db, Is.Not.Null);
			Assert.That ( (idbcount + 1) , Is.EqualTo(edbcount));
			Assert.That ( transporte_db.Key, Is.EqualTo(transporte.Key));

			Assert.That (transporte_db.Utente, Is.Not.Null);
			Assert.That ( transporte_db.Estado, Is.Not.Null);
			Assert.That (transporte_db.Latitude, Is.Not.Null);
			Assert.That (transporte_db.Longitude,Is.Not.Null);

			throw new NotImplementedException();
			//Assert.That (transporte_db.Rating, Is.Null);
			//Assert.That (transporte_db.Taxi, Is.Null);
			//Assert.That (transporte_db.Taxista, Is.Null);
		
		
		}

		[Test]
		public	void RemoverServicoTransporte()
		{

			int idbCount = BLFacade.CountEntity<ISolicitacaoTransporte> (  );

			var servicotaxi = BLFacade.RemoveEntity<ISolicitacaoTransporte> (_idTransporte.ToString());

			int edbCount = BLFacade.CountEntity<ISolicitacaoTransporte> (  );

			var servico_db = BLFacade.LoadEntity<ISolicitacaoTransporte> (_idTransporte.ToString());

			Assert.That ( (idbCount -1) == edbCount );
			Assert.IsNotNull ( servicotaxi );
			Assert.IsNull ( servico_db );


		}

//		[Test]
//		public void FecharServico()
//		{
//
//			var servicot_bo =  BLFacade.LoadEntity<IServicoTaxi> (_idTransporte.ToString());
//		
//			var closed = servicot_bo.CloseService ();
//
//			var updated = BLFacade.UpdateEntity<IServicoTaxi> (servicot_bo);
//
//			DBService.Session.Flush ();
//
//			servicot_bo = BLFacade.LoadEntity<IServicoTaxi> (_idTransporte.ToString());
//
//
//			Assert.That(servicot_bo, Is.Not.Null);
//			Assert.That(updated, Is.True);
//			Assert.That (closed, Is.True);
//			Assert.That (servicot_bo.IsActive, Is.False);
//			Assert.That (servicot_bo.Fim, Is.Not.EqualTo( default(DateTime)));
//			Assert.That (servicot_bo.Inicio, Is.Not.EqualTo (default(DateTime)));
//		
//		}

		[Test]
		public void SelectServicoTransporteInserido()
		{
			var transporte_db = BLFacade.LoadEntity<ISolicitacaoTransporte> (_idTransporte.ToString());

			Assert.That (transporte_db, Is.Not.Null);
			Assert.That (transporte_db.Key, Is.Not.Null);
			Assert.That (transporte_db.Utente, Is.Not.Null);
			Assert.That ( transporte_db.Estado, Is.Not.Null);
			Assert.That (transporte_db.Latitude, Is.Not.Null);
			Assert.That (transporte_db.Longitude,Is.Not.Null);
//			Assert.That (transporte_db.Rating, Is.Not.Null);
//			Assert.That (transporte_db.Taxi, Is.Not.Null);
//			Assert.That (transporte_db.Taxista, Is.Not.Null);
			throw new NotSupportedException ();
		
		}
//
//
		public void ListarServicoExistentes()
		{
			var icountdb = BLFacade.CountEntity<ISolicitacaoTransporte> ();
		
			
			IList<ISolicitacaoTransporte> servico_bo = BLFacade.ListEnity<ISolicitacaoTransporte>();
	
			Assert.That (servico_bo, Is.Not.Null);
			Assert.That (servico_bo.Count, Is.GreaterThan(0));
			Assert.That (icountdb.Equals(servico_bo.Count ));

			foreach (var item in servico_bo) {

				Assert.That (item.Key, Is.Not.Null);
				Assert.That (item.Utente, Is.Not.Null);
				Assert.That ( item.Estado, Is.Not.Null);
				Assert.That (item.Latitude, Is.Not.Null);
				Assert.That (item.Longitude,Is.Not.Null);
//				Assert.That (item.Rating, Is.Not.Null);
//				Assert.That (item.Taxi, Is.Not.Null);
//				Assert.That (item.Taxista, Is.Not.Null);
				throw new NotSupportedException ();
			}

		}


		[Test]
		public void RemoverServicoQueNaoExistente()
		{
			int idbCount = BLFacade.CountEntity<ISolicitacaoTransporte> ();

			var servico = BLFacade.RemoveEntity<ISolicitacaoTransporte> ("1223");

			int edbCount = BLFacade.CountEntity<ISolicitacaoTransporte> ();

			Assert.That ( idbCount == edbCount );
			Assert.IsNull (servico );


		}

		[Test]
		public void ActualizarPropriedadesDoServicoTransporte()
		{

			int? rating = 5, estado = 4;
			var transporte = BLFacade.LoadEntity<ISolicitacaoTransporte> (_idTransporte.ToString());
//			transporte.Rating = rating;
			transporte.Estado = (EstadoPedido)estado;
			DBService.Session.Flush ();

			var tr_db = BLFacade.LoadEntity<ISolicitacaoTransporte> (_idTransporte.ToString());

			Assert.That (tr_db, Is.Not.Null );
			Assert.That (tr_db.Estado, Is.EqualTo(EstadoPedido.Avaliado));
//			Assert.That (tr_db.Rating, Is.EqualTo(rating));

			throw new NotSupportedException ();



		}
	
	}
}

