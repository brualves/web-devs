using System;
using TaxiService.BusinessLayer.Notification;
using NUnit.Framework;

namespace TaxiService.BusinessLayer.Test
{
	public class GcmNotificationTest : BaseFixture
	{


		private string _deviceAppID = "";




		[Test]
		public void ApenasUmaInstanciaDaEntidade(){
		
			var instace_1 = GcmNotifier.Instance() ;
			var instace_2 = GcmNotifier.Instance() ;

			Assert.IsNotNull ( instace_1 );
			Assert.AreSame ( instace_1 , instace_2);		
		
		}

		[Test]
		public void NotificarDispositivo(){

			INotifier gcm = GcmNotifier.Instance ();

			String result = gcm.Notify ( _deviceAppID, new { Action = "Teste App "});

			Assert.IsNotNullOrEmpty (result);


		}






	}
}

