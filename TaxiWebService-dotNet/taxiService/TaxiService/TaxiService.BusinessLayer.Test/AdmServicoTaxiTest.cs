using System;
using NUnit.Framework;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer.Test
{
	[TestFixture]
	public class AdmServicoTaxiTest : BaseFixture
	{

		Guid _idServico;
		string _IdTaxi, _IdTaxista;

		protected override void OnSetup ()
		{
			base.OnSetup ();
			ConfigDB.Config ();
			_idServico = ConfigDB._servico_db.Id;
			_IdTaxi = ConfigDB._servico_db.Taxi.Matricula;
			_IdTaxista = ConfigDB._servico_db.Taxista.NomeUtilizador;
		}

		[Test]
		public void InserirNovoServicoTaxi ()
		{
		
			var idbcount = BLFacade.CountEntity<IServicoTaxi> ();
			var taxi = BLFacade.LoadEntity<ITaxi> (_IdTaxi);
			var taxista = BLFacade.LoadEntity<ITaxista> (_IdTaxista);
			var servico = BLFacade.CreateEntity<IServicoTaxi> (String.Empty);

			servico.Taxi = taxi;
			servico.Taxista = taxista;

			var inserted = BLFacade.InsertEntity<IServicoTaxi> (servico);

			DBService.Session.Flush ();
		
			var edbcount = BLFacade.CountEntity<IServicoTaxi> ();

			var servico_db = BLFacade.LoadEntity<IServicoTaxi> (servico.Key);


			Assert.That (inserted, Is.True);
			Assert.That (servico_db, Is.Not.Null);
			Assert.That ((idbcount + 1), Is.EqualTo (edbcount));
			Assert.That (servico_db.Key, Is.EqualTo (servico.Key));

			Assert.That (servico_db.Fim, Is.EqualTo (default(DateTime)));
			Assert.That (servico_db.Inicio, Is.Not.EqualTo (default(DateTime)));
			Assert.That (servico_db.IsActive, Is.True);
			Assert.That (servico_db.Taxi.Key, Is.EqualTo (_IdTaxi));
			Assert.That (servico_db.Taxista.Key, Is.EqualTo (_IdTaxista));
		
		
		}

		[Test]
		public	void RemoverServicoTaxi ()
		{

			int idbCount = BLFacade.CountEntity<IServicoTaxi> ();

			var servicotaxi = BLFacade.RemoveEntity<IServicoTaxi> (_idServico.ToString ());

			int edbCount = BLFacade.CountEntity<IServicoTaxi> ();

			var servico_db = BLFacade.LoadEntity<IServicoTaxi> (_idServico.ToString ());

			Assert.That ((idbCount - 1) == edbCount);
			Assert.IsNotNull (servicotaxi);
			Assert.IsNull (servico_db);


		}

		[Test]
		public void FecharServico ()
		{

			var servicot_bo = BLFacade.LoadEntity<IServicoTaxi> (_idServico.ToString ());
		
			var closed = servicot_bo.CloseService (default(DateTime));

			var updated = BLFacade.UpdateEntity<IServicoTaxi> (servicot_bo);

			DBService.Session.Flush ();

			servicot_bo = BLFacade.LoadEntity<IServicoTaxi> (_idServico.ToString ());


			Assert.That (servicot_bo, Is.Not.Null);
			Assert.That (updated, Is.True);
			Assert.That (closed, Is.True);
			Assert.That (servicot_bo.IsActive, Is.False);
			Assert.That (servicot_bo.Fim, Is.Not.EqualTo (default(DateTime)));
			Assert.That (servicot_bo.Inicio, Is.Not.EqualTo (default(DateTime)));
		
		}

		[Test]
		public void SelectServicoTaxiInserido ()
		{
			var servico_bo = BLFacade.LoadEntity<IServicoTaxi> (_idServico.ToString ());

			Assert.That (servico_bo, Is.Not.Null);
			Assert.That (servico_bo.Fim, Is.EqualTo (default(DateTime)));
			Assert.That (servico_bo.Inicio, Is.Not.EqualTo (default(DateTime)));
			Assert.That (servico_bo.IsActive, Is.True);
			Assert.That (servico_bo.Taxi, Is.Not.Null);
			Assert.That (servico_bo.Taxista, Is.Not.Null);

		
		}
		//
		//
		public void ListarServicoExistentes ()
		{
			var icountdb = BLFacade.CountEntity<IServicoTaxi> ();
		
			
			IList<IServicoTaxi> servico_bo = BLFacade.ListEnity<IServicoTaxi> ();
	
			Assert.That (servico_bo, Is.Not.Null);
			Assert.That (servico_bo.Count, Is.GreaterThan (0));
			Assert.That (icountdb.Equals (servico_bo.Count));

			foreach (var item in servico_bo) {
				
				Assert.That (item.Taxi, Is.Not.Null);
				Assert.That (item.Taxista, Is.Not.Null);
				Assert.That (item.Fim, Is.EqualTo (default(DateTime)));
				Assert.That (item.Inicio, Is.Not.EqualTo (default(DateTime)));
				Assert.That (item.IsActive, Is.True);
			}


		}

		[Test]
		public void RemoverServicoQueNaoExistente ()
		{
			int idbCount = BLFacade.CountEntity<IServicoTaxi> ();

			var servico = BLFacade.RemoveEntity<IServicoTaxi> ("1223");

			int edbCount = BLFacade.CountEntity<IServicoTaxi> ();

			Assert.That (idbCount == edbCount);
			Assert.IsNull (servico);


		}
	}
}

