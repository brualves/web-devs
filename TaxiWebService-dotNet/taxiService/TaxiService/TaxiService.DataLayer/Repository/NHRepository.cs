using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Linq;
using TaxiService.Domain.RepoInterface;
using TaxiService.Domain.Entities;
using TaxiService.Domain.Exception;
using NHibernate.Criterion;
using TaxiService.Common;

namespace TaxiService.DataLayer.Repository
{
	public class NHRepository : NHibernateBase, IRepository
	{

		private static string LOG_HANDLER = "NHRepository";

		private static ILogHandler LogHandler { get { return LogFramework.GetLogHandler (LOG_HANDLER); } }

		public NHRepository (ISession session):base(session)
		{
		}

		public T SelectById<T> (Guid iD) where T: Entidade
		{
			try {

				return Transact<T> (() => {
					return Session.Get<T> (iD); });
			} catch (HibernateException ex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex.InnerException);
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				throw new DBException (
					(String.IsNullOrEmpty (ex.InnerException.Message) ? 
					ex.Message : 
					ex.InnerException.Message));
			}
		}

		public void Insert (Entidade entity)
		{
			try {
				Transact (() => {
					Session.Save (entity);});

			} catch (HibernateException ex) {

				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex.InnerException);
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				throw new DBException (
					(String.IsNullOrEmpty (ex.InnerException.Message) ? 
					ex.Message : 
					ex.InnerException.Message));
			}
		}

		public IList<T> SelectByCriteria<T> (string property, object value) where T : Entidade
		{
			try {
				return	Transact<IList<T>> (() => {
					return Session.CreateCriteria<T> ().Add (Restrictions.Eq (property, value)
					).List<T> ();});

			} catch (HibernateException ex) {

				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex.InnerException);
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);

				throw new DBException (
					(String.IsNullOrEmpty (ex.InnerException.Message) ? 
					ex.Message : 
					ex.InnerException.Message));
			}
		}

		public void Update (Entidade entity)
		{
			try {
				Transact (() => {
					Session.Update (entity);});
			} catch (HibernateException ex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex.InnerException);
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				throw new DBException (
					(String.IsNullOrEmpty (ex.InnerException.Message) ? 
					ex.Message : 
					ex.InnerException.Message));
			}

		}

		public	int Count <T> () where T: Entidade
		{
			try {
				return	Transact<int> (() => {
					return Session.CreateCriteria<T> ().List<T> ().Count;});
				
			} catch (HibernateException ex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				throw new DBException (
					(String.IsNullOrEmpty (ex.InnerException.Message) ? 
					ex.Message : 
					ex.InnerException.Message));
			}


		}

		public bool Delete (Entidade entity)
		{	

			try {
			
				Transact (() => Session.Delete (entity));
				return true;
			} catch (HibernateException ex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				throw new DBException (
					(String.IsNullOrEmpty (ex.InnerException.Message) ? 
					ex.Message : 
					ex.InnerException.Message));
			}
		
		}

		public bool Contains (Entidade entity)
		{
			try {
				if (entity == null || entity.Id == default(Guid))
					return false;
				return Transact (() => {
					return Session.Get (entity.GetType (), entity.Id) != null; });
			} catch (Exception ex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				throw new DBException (
					(String.IsNullOrEmpty (ex.InnerException.Message) ? 
					ex.Message : 
					ex.InnerException.Message));
			}

		}

		public IList<T> List<T> ()  where T: Entidade
		{
			try {
				return Transact<IList<T>> (() => {
					return Session.CreateCriteria<T> ().List<T> ();});
			} catch (Exception ex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				throw new DBException (
					(String.IsNullOrEmpty (ex.InnerException.Message) ? 
					ex.Message : 
					ex.InnerException.Message));
			}
			
		}
	}
}
