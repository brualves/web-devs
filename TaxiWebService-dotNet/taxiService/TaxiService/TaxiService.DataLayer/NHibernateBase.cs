using System;
using NHibernate;

namespace TaxiService.DataLayer
{
	public class NHibernateBase
	{
	
		protected readonly ISession _session;


		public NHibernateBase(ISession session){

			_session = session;

		}

		public ISession Session {
			get { return _session; }
		}
	
		protected virtual TResult Transact<TResult>( Func<TResult> func)
		{
			if (!Session.Transaction.IsActive)
			{
				// Wrap in transaction
				TResult result;
				using (var tx = Session.BeginTransaction())
				{
					result = func.Invoke();
					tx.Commit();
				}
				return result;
			}
			// Don't wrap;
			return func.Invoke();
		}
		protected virtual void Transact(Action action)
		{
			Transact<bool>(() => { action.Invoke(); return false; });
		}
	

	
	}
}

