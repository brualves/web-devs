using System;
using NHibernate.Cfg;
using NHibernate;

namespace TaxiService.DataLayer
{
	public static class NHConfigurator
	{ 


		public static Configuration Configuration { get; set; }

		public static ISessionFactory SessionFactory { get; set; }

		public static ISession Session { get { return SessionFactory.OpenSession (); } }


		static NHConfigurator ()
		{
			Configuration = new Configuration ().Configure ();
			SessionFactory = Configuration.BuildSessionFactory ();
		
		}

	}
}

