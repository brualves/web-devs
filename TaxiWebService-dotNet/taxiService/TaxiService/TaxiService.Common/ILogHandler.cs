using System;

namespace TaxiService.Common
{
	public interface ILogHandler
	{
		void Add( LogFramework.MSGTYPE mtype, string msg  );
		void Add( LogFramework.MSGTYPE mtype, System.Exception ex);

	}
}

