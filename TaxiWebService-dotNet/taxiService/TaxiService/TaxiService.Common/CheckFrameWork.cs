using System;
using System.Collections.Generic;

namespace TaxiService.Common
{
	public static class CheckFrameWork
	{

		public delegate ValidationResul ValidateFunc<T>(T obj);

		public class ValidationResul
		{
			public bool IsValid { get; set;}
			public String Message{ get; set;}
		
		}

		private static readonly Dictionary<Type,Delegate>  _checkFuncs = new Dictionary<Type,Delegate> ();

		public static void Add<T> (ValidateFunc<T> func)
		{
			if (_checkFuncs.ContainsKey (typeof(T)))
				_checkFuncs [typeof(T)] = func;
			else
				_checkFuncs.Add (typeof(T), func);
		}

		public static Delegate Get <T> ()
		{
			Delegate ret_func;
			_checkFuncs.TryGetValue (typeof(T), out ret_func);
			return ret_func;
		}

		public static ValidationResul Validate<T> (T objtocheck)
		{
			var exec_func = Get<T> ();
			if (exec_func == null)
				return null;
			return exec_func.DynamicInvoke (objtocheck) as ValidationResul ;
		}
	}
}

