using System;
using TaxiService.Common;
using System.Collections.Generic;
using log4net;

namespace TaxiService.Common
{
	public static class LogFramework
	{
		public enum MSGTYPE
		{
			ERROR,
			WARNING,
			INFO }
		;

		private static Dictionary<string, ILogHandler> _logHandlers ;

		public static ILogHandler GetLogHandler (String name)
		{ 
			ILogHandler log;

			if (_logHandlers.TryGetValue (name, out log))
				return log;

			log = new LogHandler ( LogManager.GetLogger(name));
			_logHandlers [name] = log;
			return log;
		}

		//public static ILogHandler GetLogHandler( Type logType){return null;}
		private class LogHandler :ILogHandler
		{
			private readonly ILog _log;

			public LogHandler (ILog log)
			{
				_log = log;
			}

			public void Add (LogFramework.MSGTYPE mtype, String msg)
			{
			
				switch (mtype) {
				case MSGTYPE.ERROR:
					{
						_log.Error (msg);
						break;}
				case MSGTYPE.WARNING:
					{
						_log.Warn (msg);
						break;}
				case MSGTYPE.INFO:
					{
						_log.Info (msg);
						break;}
				default:
					break;
				}
			}

			public void Add (LogFramework.MSGTYPE mtype, System.Exception ex)
			{
				switch (mtype) {
				case MSGTYPE.ERROR:
					{
						_log.Error (ex);
						break;}
				case MSGTYPE.WARNING:
					{
						_log.Warn (ex);
						break;}
				case MSGTYPE.INFO:
					{
						_log.Info (ex);
						break;}
				default:
					break;
				}
			}

		}

		static LogFramework ()
		{
			
			log4net.Config.XmlConfigurator.Configure ();
			_logHandlers = new Dictionary<string, ILogHandler> ();
			
		}
	}
}

