using System;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer.Notification
{
	public interface INotifier
	{
		String Notify(string dest, object data );
		String Notify( IList<String> dest, object data );

	}
}

