
namespace TaxiService.BusinessLayer.Notification
{

	//Todos os Métodos públicos são ThreadSafe
	//Podem ser acedidos em cenários multithreading 
	public class TaxistaNotificationMsg
	{
		public Message 		Tipo { get; set; }

		public string 		IdServico { get; set; }

		public EstadoPedido Estado { get; set; }

		public double 		Longitude { get; set; }

		public double 		Latitude { get; set; }

		public string 		Referencia { get; set; } 				 

	}

}

