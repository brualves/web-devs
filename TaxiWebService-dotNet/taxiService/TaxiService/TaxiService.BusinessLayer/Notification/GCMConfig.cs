using System;

namespace TaxiService.BusinessLayer.Notification
{
	public static class GCMConfig
	{

		public static readonly string ApiKei 		= "AIzaSyB0o2QYP5EkebBt8iq6nEJx60DYXD2saJY";
		public static readonly string GoogleServer = "https://android.googleapis.com/gcm/send";
		public static readonly string ContentType  =  "application/json";
		public static readonly string Method  = "POST";

		public static readonly int RetryAttempts  = 3;
		public static readonly int RetryInterval = 3000;


		
	}
}

