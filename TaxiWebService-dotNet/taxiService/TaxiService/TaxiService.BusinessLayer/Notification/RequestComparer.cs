using System;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer.Notification
{
	public class RequestComparer : IComparer<TaxistaResponseMsg> 
	{
	
		public int Compare (TaxistaResponseMsg x, TaxistaResponseMsg y)
		{
			if (x == null || y == null )
				throw new ArgumentNullException();

			return (int) (x.Distance - y.Distance);
		}
	
	
	}
}

