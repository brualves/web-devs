using System;

namespace TaxiService.BusinessLayer.Notification
{
	class NotificationException : Exception
	{
		public NotificationException (string str) : base(str) {}
	}

}

