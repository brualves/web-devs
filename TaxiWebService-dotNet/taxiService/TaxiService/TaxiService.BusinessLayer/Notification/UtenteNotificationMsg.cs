using System;
using System.Collections.Generic;
using TaxiService.Domain.Entities;
using System.Threading;
using TaxiService.BusinessLayer.Notification;

namespace TaxiService.BusinessLayer.Notification
{
	public class UtenteNotificationMsg
	{
		public Message Type { get; set;}
		public EstadoPedido Estado { get; set;}
		public string TaxistaID { get; set;}

		public	string IdServico{ get; set;}
	
	}
	
}

