using System;
using System.Collections.Generic;
using TaxiService.BusinessLayer.BOEntities;

namespace TaxiService.BusinessLayer.Notification
{
	public class TaxistaAppNotifier : INotification 
	{

		private Predicate <ITaxista> _predicate ;
		private object _payLoad;
		private ISolicitacaoTransporte _solicitacao;
		private long _endTicks;

		public TaxistaAppNotifier 
			(Predicate <ITaxista> predicate , object payLoad, ISolicitacaoTransporte solicitacao )
		{
			_payLoad = payLoad;
			_predicate = predicate;
			_endTicks = DateTime.Now.AddMinutes ( solicitacao.TimeOut).Ticks;
			_solicitacao = solicitacao;
		}



		public List<string> GetUser ()
		{
			var taxistas = new List<string> ();

			foreach (var item in TaxistasManager.Instance.GetServicosAtivos() ) 
				if( _predicate.Invoke(item.Taxista ) )
					taxistas.Add( item.Taxista.IdAplicacao );
			return taxistas;
		}

		public object GetPayLoad ()
		{
			return _payLoad;
		}

		public bool IsExpired ()
		{
			//Ver se o objeto não foi eliminado 
			var solicitacao = BLFacade.LoadEntity<ISolicitacaoTransporte> ( _solicitacao.Key );
			if (solicitacao == null || solicitacao.GetServicoTransporte() != null )
				return true;

			var ticksNow = DateTime.Now.Ticks;

			return ticksNow > _endTicks;


		}

	}
}

