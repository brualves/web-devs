using System;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer.Notification
{
	public class GcmResponse
	{
	
		public string multicast_id { get; set; }

		public int success { get; set; }

		public int failure { get; set; }

		public string canonical_ids { get; set; }

		public IList<Result> result { get; set; }


	public	class Result
		{
			public string message_id {
				get;
				set;
			}

			public string registration_id {
				get;
				set;
			}

			public string error {
				get;
				set;
			}
		}


	}
}

