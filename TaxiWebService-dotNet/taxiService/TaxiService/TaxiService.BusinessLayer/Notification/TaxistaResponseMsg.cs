using System;
using System.Threading;
using System.Collections.Concurrent;
using System.Collections.Generic;
using TaxiService.Domain.Entities;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.BusinessLayer.Notification;

namespace TaxiService.BusinessLayer.Notification
{

	//Todos os Métodos públicos são ThreadSafe
	//Podem ser acedidos em cenários multithreading 
	public class TaxistaResponseMsg
	{
		public double Distance { get; set; }

		public string TaxistaID { get; set; }

		public bool Confirmed { get ; set; }

		public string AppID { get; set; }

		public string ServicoID { get; set; }

		public double? Latitude { get; set; }

		public double? Longitude { get; set; }
	}

}

