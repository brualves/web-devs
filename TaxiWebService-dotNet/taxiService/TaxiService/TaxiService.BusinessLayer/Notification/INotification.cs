using System;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer.Notification
{
	public interface INotification
	{
		List<String> GetUser ();

		object 		 GetPayLoad ();

		bool 		 IsExpired ();
	}
}

