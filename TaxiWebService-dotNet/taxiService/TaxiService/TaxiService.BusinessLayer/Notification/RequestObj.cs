using System;
using System.Threading;
using TaxiService.BusinessLayer.BOEntities;

namespace TaxiService.BusinessLayer.Notification
{

	//Todos os Métodos públicos são ThreadSafe
	public class RequestObj
	{
		public string TaxistaID { get; set; }

		public String IdServico { get; set; }

		public int Timeout { get; set; }

		public string AppID { get; set; }

		public EstadoPedido Estado { get; set; }

		public AutoResetEvent WaitEvent { get; set; }


		//..Métodos públicos (thread safe )

		public void Accept (object servicoID)
		{
			lock (this) {

				var solicitacao = BLFacade.LoadEntity<ISolicitacaoTransporte > (IdServico);

				solicitacao.Estado = Estado = EstadoPedido.Aceite;
				
				WaitEvent.Set ();
			}
		}

		public void Reject ()
		{
			lock (this) {

				var solicitacao = BLFacade.LoadEntity<ISolicitacaoTransporte > (IdServico);
				
				solicitacao.Estado = Estado = EstadoPedido.Rejeitado;
				WaitEvent.Set ();
			}
		}

		public void Cancel ()
		{
			lock (this) {

				var solicitacao = BLFacade.LoadEntity<ISolicitacaoTransporte > (IdServico);

				solicitacao.Estado = Estado = EstadoPedido.Cancelado;

				WaitEvent.Set ();
			}
		}

		public void WaitForEvent (int timeOut)
		{
			WaitEvent.WaitOne (timeOut);
		}

	}


}

