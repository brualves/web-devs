using System;
using System.Threading;
using System.Collections.Concurrent;
using System.Collections.Generic;
using TaxiService.Domain.Entities;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.BusinessLayer.Notification;

namespace TaxiService.BusinessLayer.Notification
{

	//Todos os Métodos são ThreadSafe
	//Podem ser acedidos em cenários multithreading 
	public class RQManager
	{

		public static Dictionary<String, RQContext> _requestQueue = new Dictionary<String, RQContext> ();

		public static int Count {
			get { 
				lock (_requestQueue) {
					return _requestQueue.Count;	
				}
			}
		}

		public static void Clear ()
		{
			lock (_requestQueue) {
				_requestQueue.Clear ();
			}
		}

		public static RequestObj AddRequest (ISolicitacaoTransporte solicitacao , int timeOut)
		{
			lock (_requestQueue) {


				//var solicitacao = BLFacade.LoadEntity<ISolicitacaoTransporte> (requestID);

				//if (solicitacao == null)
				//	throw new ApplicationException ("Servico Tranporte invalido");

				//..inicializar objecto que representa o pedido 
				var reqObj = new RequestObj{ 
						IdServico = solicitacao.Key ,
						AppID 	  = solicitacao.Utente.IdAplicacao,
						Timeout	  = timeOut,
						WaitEvent = new AutoResetEvent (false),
						Estado 	  = EstadoPedido.EmProcessamento
				};

				//var respObj = ;

				var context = new RQContext {Request = reqObj,Response = new ResponseObj () };				
						
				_requestQueue [solicitacao.Key] = context; 

				StartResponseThread (context);
				
				return reqObj;
			}

		}

		public static RequestObj GetRequest (string servicoID)
		{
			lock (_requestQueue) {
				return _requestQueue [servicoID].Request;	
			}

		}

		static void StartResponseThread (RQContext context)
		{

			new Thread (
				( ) => {

				ProcessRequest (context, (context.Request.Timeout == default(int)) ? Config.RequestTimeOut : context.Request.Timeout, 
				               GcmNotifier.Instance ());

			}
			
			).Start ();
							
		}

		public static void ProcessRequest (RQContext context, int timeOut, INotifier notificatioinService)
		{

			var servicoTranporte = new AdmSolicitacaoTransporte ().SelectByKey (context.Request.IdServico);


			if (servicoTranporte == null)
				throw new ApplicationException ("Servico Tranporte Invalido ");

			//..obter ApplicationID dos taxistas em serviço 
			IList<String> appID = GetTaxistaAppID (

				( taxista ) => {
				return taxista.Rating >= servicoTranporte.MinRate;

			}); 

			if (appID == null || appID.Count == 0) {
				RQManager.RemoveRequest (context.Request.IdServico);//

				//..sinalizar que o pedido foi rejeitado 
				context.Request.Reject (); 

				return;
			}


			//..mensagem a ser enviado para Aplicação Taxista 
			var requestMsg = new TaxistaNotificationMsg (){
				Tipo 	 	= Message.Notificar,
				IdServico 	= context.Request.IdServico,
				Estado		= context.Request.Estado,
				Latitude   	= (double)servicoTranporte.Latitude,
				Longitude 	= (double)servicoTranporte.Longitude,
				Referencia	= servicoTranporte.PontoReferencia
			};

			long initialTime = DateTime.Now.Ticks, endTime;

			do {

				try {

					//..notificar os taxistas a cerca do pedido 
					notificatioinService.Notify (appID, requestMsg); 


					context.Response.WaitForResponse (
						(int) (timeOut / 2) , ((appID.Count < 5) ? appID.Count : (int)(appID.Count * 0.75)));

					if (ProcessResponse (context, 
					                     RQManager.Config.ConfirmationTimeOut, 
					                     notificatioinService))
						return;
										
				} catch (OperationCanceledException) {
		
					RQManager.RemoveRequest (context.Request.IdServico);
					context.Request.Cancel (); //...sinalizar que o pedido foi cancelado 
					return;
				} catch (TimeoutException) {
				}

				endTime = DateTime.Now.Ticks;

			} while ( ( initialTime - endTime ) <= timeOut );

			if (! ProcessResponse (context, 
			                       RQManager.Config.ConfirmationTimeOut, 
			                       notificatioinService)) {

				RQManager.RemoveRequest (context.Request.IdServico);//

				//..sinalizar que o pedido foi rejeitado 
				context.Request.Reject (); 
			}

		}

		static void RemoveRequest (string idServico)
		{
			lock (_requestQueue) {
				_requestQueue.Remove (idServico);
			}
		}

		static bool ProcessResponse (RQContext context, int timeOut, INotifier notifier)
		{

			if (context == null)
				throw new ArgumentNullException ("context");

			List<TaxistaResponseMsg> response = (List<TaxistaResponseMsg>)context.Response.GetResponseList (); 

			if (response == null || response.Count == 0)
				return false;

			//Organizar lista de respostas 
			response.Sort (new RequestComparer ());

			//Processar repostas 
			foreach (var item in response) {

				try {


					notifier.Notify (item.AppID, new TaxistaNotificationMsg ());

					context.Response.WaitForConfirmation (timeOut);

					if (item.Confirmed) {

						//..remover pedido da lista dos pedidos a processar 
						RQManager.RemoveRequest (context.Request.IdServico);
					
						//..aceitar o pedido indicando o serviço táxi que aceitou prestar o serviço 
						context.Request.Accept (item.TaxistaID);

						return true;
					}

				} 

				//..no caso de timeOut processar proximo item 
				catch (TimeoutException) {
				}
			}

			return false;	

		}

//		static void NotifyAllTaxista (RQContext context, IList<string> appID)
//		{
//			throw new NotImplementedException ();
//		}

		public static void CancelRequest (string requestID)
		{
			lock (_requestQueue) {

				_requestQueue[requestID].Response.Cancel();
			}

		}

		public static void AddRequestResponse (string requestId, TaxistaResponseMsg response)
		{
			lock (_requestQueue) {

				var context = _requestQueue [requestId];
				 
				context.Response.AddResponse (response);

			}
		
		}

		public static void AddRequestConfirmation (string requestId, string responseId, bool confirmed)
		{
			lock (_requestQueue) {
				var context = _requestQueue [requestId];
				context.Response.Confirm (responseId, confirmed);
				
			}
			
		}

		static IList<string> GetTaxistaAppID (Predicate<ITaxista> predicate)
		{
			if (predicate == null)
				throw new ArgumentNullException ("predicate");
						
			IList<string> appID = new List<string> ();

			var servicoAtivos = BLFacade.ListEnity<IServicoTaxi> (
				(servico) => {
				return servico.Fim == default(DateTime); });

			if (servicoAtivos == null || servicoAtivos.Count == 0)
				return null;

			foreach (var item in servicoAtivos) 
				if (predicate.Invoke (item.Taxista) && ! 
				   string.IsNullOrEmpty (item.Taxista.IdAplicacao))
					appID.Add (item.Taxista.IdAplicacao);
		
			return appID;

		}


		public static class Config
		{
			public static readonly int ConfirmationTimeOut = 30 * 1000 ; //30s 
			public static readonly int RequestTimeOut = 1 * 60 * 1000 ; // 5min 				
			public static readonly int MaxRequestTimeOut = ConfirmationTimeOut + RequestTimeOut;

		}
	}
}

