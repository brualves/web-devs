using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;

namespace TaxiService.BusinessLayer.Notification
{

	//Todos os Métodos públicos são ThreadSafe
	public class ResponseObj
	{
		private ConcurrentDictionary<string, TaxistaResponseMsg > _responseList = 
			new ConcurrentDictionary< string, TaxistaResponseMsg> ();

		private CountdownEvent _syncEvent = new CountdownEvent ( 0 );
		private CancellationTokenSource _cancelToken = new CancellationTokenSource(); 


		public TaxistaResponseMsg this [string index] {
			get {
				return _responseList [index];
			}
		}

		public CancellationToken CancellationToken { get { return _cancelToken.Token; }}

		public void AddResponse (TaxistaResponseMsg response)
		{
			lock (this) {

				_responseList [response.TaxistaID] = response;

				_syncEvent.Signal ();
			}
		
		}

		public void Cancel ()
		{
			lock (this) {
				_cancelToken.Cancel();
			}
		}

		public void WaitForResponse (int timeOut,int response)
		{
			//lock (this) {

				_syncEvent.Reset (response);	
				_syncEvent.Wait (timeOut, CancellationToken);
			//}

		}

		public void WaitForConfirmation (int timeOut)
		{
			WaitForResponse (1, timeOut);
		}

		public IList<TaxistaResponseMsg> GetResponseList ()
		{
			List<TaxistaResponseMsg> items = new List<TaxistaResponseMsg> ();

			foreach (var item in _responseList.Values) 
				items.Add (item);

			return items;
		}

		public void Confirm( string responseId, bool confirm )
		{
		//	lock (this) {
	
				_responseList[responseId].Confirmed = confirm;
				_syncEvent.Signal();
		//}

		}
	}


}

