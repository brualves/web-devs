using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.IO;
using ServiceStack.Text;
using System.Threading;

namespace TaxiService.BusinessLayer.Notification
{
	public class GcmNotifier : INotifier
	{
		private static GcmNotifier _singleInstance;
		const int NotificationInterval = 60 * 1000;
		private IList<INotification> _notificationQueue;

		private GcmNotifier ()
		{
			_notificationQueue = new List<INotification> ();

			new Timer (
				(obj) => ProcessNotification (), 
				null,
				0,
				NotificationInterval);
		}

		public	 static  GcmNotifier Instance ()
		{
			if (_singleInstance == null)
				_singleInstance = new GcmNotifier ();
			return _singleInstance;
		}

		public string Notify (string dest, object data)
		{

			return Notify (new List<string> { dest }, data);					
					
		}

		public string Notify (IList<String> dest, object data)
		{
			if (dest == null || dest.Count == 0)
				return null;

			int attempts = 0;
			
			do {
				
				try {


					return SendNotification (dest, data);
					
				} catch (NotificationException) {
					
					Thread.Sleep (GCMConfig.RetryInterval);
				}
				
			} while ( ++attempts < GCMConfig.RetryAttempts );
			
			throw new NotificationException ("RetryAttempts error");
		}

		public void AddNotification (INotification notification)
		{

			_notificationQueue.Add (notification);
					
		}

		public void ProcessNotification ()
		{
		
			foreach (var item in _notificationQueue) {
				if (! item.IsExpired ()) {

					try {

						Notify (item.GetUser (), item.GetPayLoad ());	
				
					} catch (Exception) {
					}
				} else				
					_notificationQueue.Remove (item);
			}
		}

		private  String SendNotification (IList<String> dest, Object payload)
		{
			HttpWebRequest tRequest;

			tRequest = (HttpWebRequest)WebRequest.Create (GCMConfig.GoogleServer);
			
			tRequest.Method = GCMConfig.Method; //POST
			tRequest.ContentType = GCMConfig.ContentType; //JSON
			
			tRequest.Headers.Add (string.Format ("Authorization: key={0}", GCMConfig.ApiKei));
			
			String collaspeKey = Guid.NewGuid ().ToString ("n");

			var obj = new {
				registration_ids = dest,
				data 		 	 = payload,
				time_to_live 	 = 0,
				collapse_key 	 = collaspeKey
			};
						
			var postData = JsonSerializer.SerializeToString (obj);

			Byte[] byteArray = Encoding.UTF8.GetBytes (postData);
			tRequest.ContentLength = byteArray.Length;

			String responseBody = null;
			
			using (Stream dataStream = tRequest.GetRequestStream ()) {
			
				dataStream.Write (byteArray, 0, byteArray.Length);
				//dataStream.Close ()
				using (HttpWebResponse tResponse = (HttpWebResponse)tRequest.GetResponse ())
				using (Stream responseStream = tResponse.GetResponseStream ()) {

					if (tResponse.StatusCode != HttpStatusCode.OK)
						throw new NotificationException ("Response Status Code : " + tResponse.StatusCode);
					 
					StreamReader tReader = new StreamReader (responseStream);
					responseBody = tReader.ReadToEnd ();
				
				}

			}
			return responseBody;

		}
	}
}

