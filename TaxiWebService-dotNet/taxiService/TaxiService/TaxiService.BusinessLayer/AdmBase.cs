using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.Domain.Entities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer
{
	public abstract  class  AdmBase<T> : IAdmPerfil<T> where T: IBOEntity
	{


		public virtual bool Add (T entity)
		{
			if (entity == null)
				throw new ArgumentNullException ("entity");

			BaseEntity baseE = entity as BaseEntity;

			if (baseE == null) 
				throw new ArgumentException ("entity");	


			DBService.Insert (baseE.Entity);
			return true;
		}

		public virtual bool Update (T entity)
		{
			if (entity == null)
				throw new ArgumentNullException ("entity");

			BaseEntity baseE = entity as BaseEntity;
			
			if (baseE == null) 
				throw new ArgumentException ("entity");	
			DBService.Update (baseE.Entity);
			return true;
		}

		public virtual T Remove (T entity)
		{

			if (entity == null)
				throw new ArgumentNullException ("entity");

			BaseEntity baseE = entity as BaseEntity;

			if (baseE == null) 
				throw new ArgumentException ("entity");

			DBService.Delete (baseE.Entity);

			DBService.Session.Flush ();

			return entity;
		
		}
		/* Retorna a enitdade removida ou null caso a entidade não exista */
		public virtual T Remove (string key)
		{
			if (string.IsNullOrEmpty (key))
				throw new ArgumentNullException ("entity");
	
			var t_entity = SelectByKey (key);
			if (t_entity == null)
				return default(T);
			return Remove (t_entity);
		}

		public abstract T SelectByKey (string key);

		public abstract IList<T> ListEntities ();

		public abstract int CountEntities ();

		public abstract T NewEntity (string key);

	}
}

