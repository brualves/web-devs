using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.Domain.Entities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer
{
	internal class AdmServicoTaxi : AdmBase<IServicoTaxi>
	{



		private IServicoTaxi SelectByKey(Guid key)
		{

			
			var servicoTaxi = DBService.SelectById <ServicoTaxi>(key);
			if (servicoTaxi == null) 
				return null;
			return new BOServicoTaxi (servicoTaxi);

		}
		#region implemented abstract members of AdmBase

		public override IServicoTaxi SelectByKey (string key)
		{
			Guid newGuid = Guid.Empty;
			try {
				newGuid  =  new Guid(key);
			} catch (Exception) { return null;}

			return SelectByKey ( newGuid ) ;
		}

		public override IList<IServicoTaxi> ListEntities ()
		{
			IList<IServicoTaxi> listServico = null;


			var itemsDB = DBService.List<ServicoTaxi> ();

			if (itemsDB == null || itemsDB.Count == 0)
				return null;

			listServico = new List<IServicoTaxi>();

			foreach (var item in itemsDB) 
				listServico.Add (
					new BOServicoTaxi(item)
					);
			return listServico;
		}

		public override int CountEntities ()
		{
			return DBService.Count<ServicoTaxi> ();
		}

		public override IServicoTaxi NewEntity (string key)
		{
			return new BOServicoTaxi( new ServicoTaxi());
		}

		#endregion
	}
}

