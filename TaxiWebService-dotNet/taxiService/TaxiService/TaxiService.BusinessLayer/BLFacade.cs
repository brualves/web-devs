using System;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer
{
	public class BLFacade
	{

		private static Dictionary<Type, object> 
			_boEntitiesAdm = new Dictionary<Type,object> ();

		static BLFacade ()
		{
			_boEntitiesAdm.Add (typeof(IEmpresa), new AdmEmpresa ());
			_boEntitiesAdm.Add (typeof(IZona), new AdmZona ());
			_boEntitiesAdm.Add (typeof(ITaxi), new AdmTaxi ());
			_boEntitiesAdm.Add (typeof(ITaxista), new AdmTaxista ());
			_boEntitiesAdm.Add (typeof(IServicoTaxi), new AdmServicoTaxi ());
			_boEntitiesAdm.Add (typeof(ISolicitacaoTransporte), new AdmSolicitacaoTransporte ());
			_boEntitiesAdm.Add (typeof(IUtente), new AdmUtente ());
			_boEntitiesAdm.Add (typeof(IServicoTransporte), new AdmServicoTransporte ());
			_boEntitiesAdm.Add (typeof(IRespostaServico), new AdmRespostasServico ());

		}

		public static T CreateEntity<T> (string id) where T : IBOEntity
		{		
			IAdmPerfil<T> adm = GetAdmImpl<T> ();
			T entity = adm.NewEntity (id);
			if (entity == null)
				throw new ArgumentException (typeof(T).ToString ());
			return entity;
		}

		public static T LoadEntity<T> (string id) where T : IBOEntity
		{
			IAdmPerfil<T> adm = GetAdmImpl<T> ();

			T entety = adm.SelectByKey (id);
			
			if (entety == null)
				return default(T);
			return  entety;
		}

		private	static IAdmPerfil<T> GetAdmImpl<T> () where T : IBOEntity
		{
			object adm = null;
			_boEntitiesAdm.TryGetValue (typeof(T), out adm);
			if (adm == null)
				throw new ArgumentException (typeof(T).ToString ());
			return  (IAdmPerfil<T>)adm;
		}

	
		public static bool InsertEntity<T>(T entity) where T : IBOEntity 
		{
			var adm = GetAdmImpl<T> ();
			return adm.Add (entity);
		}

		public static bool UpdateEntity<T> (T entity) where T : IBOEntity 
		{
			var admPerfil = GetAdmImpl<T> ();
			return admPerfil.Update (entity);
		}
		public static IList<T> ListEnity<T>() where T : IBOEntity 
		{
			var admPerfil = GetAdmImpl<T> (); 
			return  admPerfil.ListEntities ();
		}

		public static IList<T> ListEnity<T>( Predicate<T> filter) where T : IBOEntity {

			var retItems = new List<T> ();
			var items = ListEnity<T>();

			foreach (var item in items) 
				if( filter.Invoke(item) )
					retItems.Add(item);
		
			return retItems;
		}

		public static T RemoveEntity<T>(T entity) where T : IBOEntity  {
			var admPerfil = GetAdmImpl<T> (); 
			return admPerfil.Remove (entity);
		}
		public static T RemoveEntity<T>(string id) where T : IBOEntity  {

			var admPerfil = GetAdmImpl<T> (); 
			return admPerfil.Remove (id);
		}

		public static int CountEntity<T>() where T : IBOEntity  {
			var admPerfil = GetAdmImpl<T>(); 
			return admPerfil.CountEntities ();
		}

		public static int CountEntity<T>(object fiter) where T : IBOEntity  {
			throw new NotImplementedException ();
		}

	

	}
}

