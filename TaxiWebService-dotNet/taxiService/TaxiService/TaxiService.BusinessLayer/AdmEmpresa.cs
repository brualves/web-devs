using System;
using System.Collections.Generic;
using TaxiService.Domain.Entities;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections;

namespace TaxiService.BusinessLayer
{
	internal class AdmEmpresa : AdmBase<IEmpresa>
	{
		public static readonly string KeyProperty = "Designacao";


		public override IEmpresa NewEntity (string key)
		{
			if (string.IsNullOrEmpty( key) )
				throw new ArgumentNullException ("key");
			return new BOEmpresa (new Empresa() {Designacao = key});
		}



		public override IEmpresa SelectByKey (string key)
		{
			IList<Empresa> lista_empresa = DBService.SelectByCriteria<Empresa> (KeyProperty, key);

			if (lista_empresa == null || lista_empresa.Count == 0 )
				return null;

			if (lista_empresa.Count > 1)
				//Log Msg 
				throw new InvalidProgramException ();
		
			IEnumerator<Empresa> itempresa = lista_empresa.GetEnumerator ();
			itempresa.MoveNext ();
			return new BOEmpresa (itempresa.Current);
		}

		public override int CountEntities ()
		{
			return DBService.Count<Empresa> ();
		}

		public override IList<IEmpresa> ListEntities ()
		{
			IList<IEmpresa> empresa_bo = null;
			IList<Empresa> empresas = DBService.List<Empresa> ();
			if (empresas == null)
				return null;
			empresa_bo = new List<IEmpresa> ();
			foreach (var empresa in empresas) {
				empresa_bo.Add (new BOEmpresa (empresa));
			}

			return empresa_bo;
		}

	}
}

