using System;
using System.Collections.Generic;
using TaxiService.Domain.Entities;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections;

namespace TaxiService.BusinessLayer
{
	internal class AdmRespostasServico : AdmBase<IRespostaServico>
	{
	
		public override IRespostaServico NewEntity (string key)
		{
			return new BORespostaServico (new RespostaServico());
		}

		private IRespostaServico SelectByKey (Guid key)
		{						
			var resposta = DBService.SelectById <RespostaServico> (key);
			if (resposta == null) 
				return null;
			return new BORespostaServico (resposta);		
		}
		
		public override IRespostaServico SelectByKey (string key)
		{
			Guid newGuid = Guid.Empty;
			try {
				newGuid = new Guid (key);
			} catch (Exception) {
				return null;
			}
			
			return SelectByKey (newGuid);
		}


		public override int CountEntities ()
		{
			return DBService.Count<RespostaServico> ();
		}

		public override IList<IRespostaServico> ListEntities ()
		{
			IList<IRespostaServico> listRespostas = null;
			
			
			var itemsDB = DBService.List<RespostaServico> ();
			
			if (itemsDB == null || itemsDB.Count == 0)
				return null;
			
			listRespostas = new List<IRespostaServico> ();
			
			foreach (var item in itemsDB) 
				listRespostas.Add (new BORespostaServico (item));
			
			return listRespostas;
		}

	}
}

