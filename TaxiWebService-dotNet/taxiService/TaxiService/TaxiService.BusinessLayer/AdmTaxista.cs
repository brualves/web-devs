using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.Domain.Entities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer
{
	internal class AdmTaxista : AdmBase<ITaxista>
	{

		private readonly string KEY_COLUMN = "NomeUtilizador";

		public override bool Add (ITaxista entity)
		{
			throw new NotImplementedException ();
		}

		public override ITaxista SelectByKey (string key)
		{

			if (String.IsNullOrEmpty (key))
				throw new ArgumentNullException ("key");
			var taxistas = DBService.SelectByCriteria <Taxista> (KEY_COLUMN, key);
			if (taxistas == null || taxistas.Count == 0 )
				return null;
			
			if (taxistas.Count != 1)
				//Log Msg 
				throw new InvalidProgramException ();
			
			IEnumerator<Taxista> ittaxista = taxistas.GetEnumerator ();
			ittaxista.MoveNext ();
			return new BOTaxista (ittaxista.Current);
		}

		public override IList<ITaxista> ListEntities ()
		{
			IList<ITaxista> taxista_bo = null;
			IList<Taxista> taxistas = DBService.List<Taxista> ();
			if (taxistas == null)
				return null;

			taxista_bo = new List<ITaxista> ();

			foreach (var taxista in taxistas) 
				taxista_bo.Add (new BOTaxista (taxista));
			
			return taxista_bo;
		}

		public override int CountEntities ()
		{
			return DBService.Count<Taxista> ();
		}

		public override ITaxista NewEntity (string key)
		{
			if (string.IsNullOrEmpty( key) )
				throw new ArgumentNullException ("key");
			return new BOTaxista (new Taxista() { NomeUtilizador = key});
		}

	}
}

