using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.Domain.Entities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer
{
	internal class AdmZona : AdmBase<IZona> 
	{
		private readonly string KEY_COLUMN = "Codigo";


		public override IZona SelectByKey (string key)
		{
			if (String.IsNullOrEmpty (key))
				throw new ArgumentNullException ("key");
			var zonas = DBService.SelectByCriteria <Zona>( KEY_COLUMN, key );
			if (zonas == null || zonas.Count == 0 )
				return null;

			if (zonas.Count > 1)
				//Log Msg 
				throw new InvalidProgramException ();

			IEnumerator<Zona> itzona = zonas.GetEnumerator (); itzona.MoveNext ();
			return new BOZona (itzona.Current);

		}

		public override IList<IZona> ListEntities ()
		{
			IList<IZona> zona_bo = null;
			IList<Zona> zonas = DBService.List<Zona> ();
			if (zonas == null)
				return null;
			zona_bo = new List<IZona> ();

			foreach (var zona in zonas) {
				zona_bo.Add (new BOZona (zona));
			}
			
			return zona_bo;

		}

		public override int CountEntities ()
		{
			return DBService.Count<Zona> ();
		}

		public override IZona NewEntity (string key)
		{
			if (string.IsNullOrEmpty( key) )
				throw new ArgumentNullException ("key");
			return new BOZona (new Zona() {Codigo = key});
		}


	}
}

