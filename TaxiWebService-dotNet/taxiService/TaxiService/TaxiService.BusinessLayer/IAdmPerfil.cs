using System;
using System.Collections.Generic;
using TaxiService.BusinessLayer.BOEntities;

namespace TaxiService.BusinessLayer
{
	
	public interface IAdmPerfil < T > where T : IBOEntity
	{

		T NewEntity (string key);

		bool Add (T entity);

		T SelectByKey (string key);

		bool Update (T entity);

		T Remove (string key);

		T Remove (T entity);

		IList<T> ListEntities ();

		int CountEntities ();


	}
}

