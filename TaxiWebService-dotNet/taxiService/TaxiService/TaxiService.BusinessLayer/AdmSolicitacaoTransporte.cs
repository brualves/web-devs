using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.Domain.Entities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer
{
	internal class AdmSolicitacaoTransporte : AdmBase<ISolicitacaoTransporte>
	{



		private ISolicitacaoTransporte SelectByKey(Guid key)
		{

			
			var transporte = DBService.SelectById <SolicitacaoTranporte>(key);
			if (transporte == null) 
				return null;
			return new BOSolicitacaoTransporte (transporte);

		}

		#region implemented abstract members of AdmBase

		public override ISolicitacaoTransporte SelectByKey (string key)
		{
			Guid newGuid = Guid.Empty;
			try {
				newGuid  =  new Guid(key);
			} catch (Exception) { return null;}

			return SelectByKey ( newGuid ) ;
		}

		public override IList<ISolicitacaoTransporte> ListEntities ()
		{
			IList<ISolicitacaoTransporte> listTransporte = null;


			var itemsDB = DBService.List<SolicitacaoTranporte> ();

			if (itemsDB == null || itemsDB.Count == 0)
				return null;

			listTransporte = new List<ISolicitacaoTransporte>();

			foreach (var item in itemsDB) 
				listTransporte.Add (
					new BOSolicitacaoTransporte(item)
					);
			return listTransporte;
		}

		public override int CountEntities ()
		{
			return DBService.Count<SolicitacaoTranporte> ();
		}

		public override ISolicitacaoTransporte NewEntity (string key)
		{
			return new BOSolicitacaoTransporte( new SolicitacaoTranporte());
		}

//
//		public override bool Add (IServicoTransporte entity)
//		{
//			bool add = base.Add (entity);
//
//			if (! add)
//				return false;
//			//notificar os taxistas onLine;
//		}

		#endregion
	}
}

