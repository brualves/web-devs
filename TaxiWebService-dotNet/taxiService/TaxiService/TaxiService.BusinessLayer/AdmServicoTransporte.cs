using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.Domain.Entities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer
{
	public class AdmServicoTransporte : AdmBase<IServicoTransporte>
	{				
		private IServicoTransporte SelectByKey (Guid key)
		{						
			var transporte = DBService.SelectById <ServicoTransporte> (key);
			if (transporte == null) 
				return null;
			return new BOServicoTransporte (transporte);		
		}

		public override IServicoTransporte SelectByKey (string key)
		{
			Guid newGuid = Guid.Empty;
			try {
				newGuid = new Guid (key);
			} catch (Exception) {
				return null;
			}
			
			return SelectByKey (newGuid);
		}

		public override IList<IServicoTransporte> ListEntities ()
		{

			IList<IServicoTransporte> listTransporte = null;


			var itemsDB = DBService.List<ServicoTransporte> ();

			if (itemsDB == null || itemsDB.Count == 0)
				return null;

			listTransporte = new List<IServicoTransporte> ();

			foreach (var item in itemsDB) 
				listTransporte.Add (new BOServicoTransporte (item));

			return listTransporte;
		}

		public override int CountEntities ()
		{
			return DBService.Count<ServicoTransporte> ();
		}

		public override IServicoTransporte NewEntity (string key)
		{ 
			return new BOServicoTransporte (new ServicoTransporte ());
		}
	}
}

