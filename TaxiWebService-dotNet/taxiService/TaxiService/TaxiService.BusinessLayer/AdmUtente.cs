using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.Domain.Entities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer
{
	internal class AdmUtente : AdmBase<IUtente>
	{

		private readonly string KEY_COLUMN = "NomeUtilizador";

//		public override bool Add (IUtente entity)
//		{
//			throw new NotImplementedException ();
//		}

		public override IUtente SelectByKey (string key)
		{

			if (String.IsNullOrEmpty (key))
				throw new ArgumentNullException ("key");
			var utente = DBService.SelectByCriteria <Utente> (KEY_COLUMN, key);
			if (utente == null || utente.Count == 0 )
				return null;
			
			if (utente.Count != 1)
				//Log Msg 
				throw new InvalidProgramException ();
			
			IEnumerator<Utente> itutente = utente.GetEnumerator ();
			itutente.MoveNext ();
			return new BOUtente (itutente.Current);
		}

		public override IList<IUtente> ListEntities ()
		{
			IList<IUtente> utente_bo = null;
			IList<Utente> utentes = DBService.List<Utente> ();
			if (utentes == null)
				return null;

			utente_bo = new List<IUtente> ();

			foreach (var item in utentes) 
				utente_bo.Add (new BOUtente (item));
			
			return utente_bo;
		}

		public override int CountEntities ()
		{
			return DBService.Count<Utente> ();
		}

		public override IUtente NewEntity (string key)
		{
			if (string.IsNullOrEmpty( key) )
				throw new ArgumentNullException ("key");
			return new BOUtente (new Utente() { NomeUtilizador = key});
		}

	}
}

