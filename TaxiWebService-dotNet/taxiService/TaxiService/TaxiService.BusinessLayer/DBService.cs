using System;
using System.Reflection;
using TaxiService.Domain.Entities;
using TaxiService.Domain.RepoInterface;
using TaxiService.DataLayer.Repository;
using TaxiService.DataLayer;
using TaxiService.Domain.Exception;
using TaxiService.Common;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;

namespace TaxiService.BusinessLayer
{
	public static class DBService
	{
	
		private static string LOG_HANDLER = "DAL_SERVICE";
		private static ISession _session;

		private  static IRepository Repository { get { return  new NHRepository (Session);}}

		public static ISession Session {
			get { 
				if (_session == null || ! _session.IsOpen)
					return (_session = NHConfigurator.Session);
				return _session;
			}
		}
	 
		private static ILogHandler LogHandler { get { return LogFramework.GetLogHandler (LOG_HANDLER); } }

		public static void Insert (Entidade entety)
		{
		
			var repo = Repository;

			try {
				repo.Insert (entety);
			} catch (DBException dbex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
				Session.Close();
				throw;
			} catch (System.Exception ex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				Session.Close();
				throw;
			}

		
		}

		public static T SelectById<T> (Guid id) where T : Entidade
		{
			var repo = Repository;

			try {
				return repo.SelectById<T> (id);

			} catch (DBException dbex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
				Session.Close();
				throw;

			} catch (System.Exception ex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				Session.Close();
				//Send mail 

				throw;
			}
		}

		public static IList<Entidade> List (Type type)
		{
			var repo = Repository;
			if (typeof(Empresa).Equals (type)) 
				return (IList<Entidade>) repo.List<Empresa> ();
			return null;
			
		}

		public static IList<T> List<T> () where T: Entidade
		{

			try {
				var repo = Repository;
				return repo.List<T> ();
			
			} catch (DBException dbex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
				Session.Close();
				throw;
			} catch (Exception ex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				Session.Close();
				//Send mail 
				throw;
			}
		}

		public static	int Count <T> () where T : Entidade
		{
			try {
		 	
				var repo = Repository;
				return repo.Count<T> ();
			
			} catch (DBException dbex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
				Session.Close();
				throw;
			} catch (Exception ex) {
				
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				Session.Close();
				//Send mail 
				throw ;
			}
		}

		public static void Delete (Entidade entity)
		{
			try {
				
				var repo = Repository;

				repo.Delete (entity);

				Session.Evict(entity);

				
			} catch (DBException dbex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
				Session.Close();
			} catch (Exception ex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				//Send mail 
				Session.Close();
				throw;
			}
		}
	
		public static IList<T> SelectByCriteria<T> (string property, object value) where T:Entidade
		{

			return Repository.SelectByCriteria<T> (property, value);

		
		}

		public static void Update (Entidade entity)
		{
			try {
				
				var repo = Repository;
				repo.Update (entity);
				
			} catch (DBException dbex) {
				LogHandler.Add (LogFramework.MSGTYPE.ERROR, dbex);
				Session.Close();
				throw;
			} catch (Exception ex) {

				LogHandler.Add (LogFramework.MSGTYPE.ERROR, ex);
				Session.Close();		
				throw;
			}
		}
	}
}

