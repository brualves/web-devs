using System;
using TaxiService.BusinessLayer.BOEntities;
using TaxiService.Domain.Entities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer
{
	internal class AdmTaxi : AdmBase<ITaxi>
	{
		private readonly string KEY_PROPERTY = "Matricula";
	
		public override ITaxi SelectByKey (string key)
		{
			IList<Taxi> lista_taxi = DBService.SelectByCriteria<Taxi> (KEY_PROPERTY, key);
			
			if (lista_taxi == null || lista_taxi.Count == 0 )
				return null;
			if (lista_taxi.Count > 1)
				throw new InvalidProgramException ();
			
			IEnumerator<Taxi> ittaxi = lista_taxi.GetEnumerator ();
			ittaxi.MoveNext ();

			return new BOTaxi (ittaxi.Current);
		}

		public override IList<ITaxi> ListEntities ()
		{
			IList<ITaxi> taxis_bo = null;
			IList<Taxi> taxis = DBService.List<Taxi> ();
			if (taxis == null)
				return null;
			taxis_bo = new List<ITaxi> ();
			foreach (var item in taxis) {
				taxis_bo.Add (new BOTaxi (item));
			}
			
			return taxis_bo;
		}

		public override int CountEntities ()
		{
			return DBService.Count<Taxi> ();
		}

		public override ITaxi NewEntity (string key)
		{
			if (string.IsNullOrEmpty (key))
				throw new ArgumentNullException ("key");
			return new BOTaxi (new Taxi { Matricula = key });
		}
	}
}

