using System;
using System.Collections.Generic;
using TaxiService.Domain.Entities;
using System.Threading;
using TaxiService.BusinessLayer.Notification;

namespace TaxiService.BusinessLayer.BOEntities
{
	public interface IUtente : IBOEntity
	{
		String PrimeiroNome { get; set; }

		String UltimoNome { get; set; }

		String NomeUtilizador { get; }

		String Email { get; set; }

		string PassWord { get; set;}

		String Telefone { get; set; }

		string IdAplicacao { get; set; }

		ISolicitacaoTransporte GetServico (String idServico);

		bool ServicosPendentes ();

		IList<IServicoTransporte> Transportes{ get; }

		ISolicitacaoTransporte SolicitarServico (double latitude, double longitude, int minRate, string pontoRef, int maxTimeOut);
	}

	internal class BOUtente : BaseEntity, IUtente
	{


		Utente _utente;

		internal BOUtente (Utente utente)
		{
			_utente = utente;
		}

		public string IdAplicacao {
			get{ return _utente.IdAplicacao; }
			set { _utente.IdAplicacao = value;}
		}

		void NotifyUser (RequestObj request, INotifier notifier)
		{
			if (request == null)
				throw new ArgumentNullException ("request");

			//..enviar notificação ao utente 
			try {

				String gcmResponse = notifier.Notify (request.AppID, new UtenteNotificationMsg () {
						Type   	   = Message.Notificar,
						Estado 	   = request.Estado,
						TaxistaID  = request.TaxistaID,
						IdServico  = request.IdServico
							
					});
					
			} catch (Exception) {

				//..erro ao enviar notificação 
			}
					
		}

		void StartNewTask (string servicoID, int maxTimeOut)
		{

//			new Thread (
//					() => {
//
//				RequestObj request = null;
//
//				try {
//
//					//..adicionar servico a fila de pedidos 
//					request = RQManager.AddRequest (servicoID);
//
//					request.WaitForEvent (maxTimeOut);
//							
//				} catch (Exception) {
//							
//					request.Reject ();
//							
//				} finally {
//							
//					NotifyUser (request, GcmNotifier.Instance ());
//				}
//						
//			}).Start ();						

		}
		public string PassWord { get {return _utente.PassWord; } 
			set { _utente.PassWord = value ;}}


		public ISolicitacaoTransporte SolicitarServico (double latitude, double longitude, int minRate, string pontoRef, int maxTimeOut)
		{

			var servico = BLFacade.CreateEntity<ISolicitacaoTransporte> (String.Empty);

			servico.Utente = this;
			servico.Estado = EstadoPedido.EmProcessamento;
			servico.Latitude = latitude;
			servico.Longitude = longitude;
			servico.PontoReferencia = pontoRef;
			servico.TimeOut = maxTimeOut;
			servico.MinRate = minRate;

			if (! BLFacade.InsertEntity<ISolicitacaoTransporte> (servico))
				throw new ApplicationException ("Insert Service exception");

			//..iniciar thread cliente 
			//StartNewTask (servico.Key, (maxTimeOut == default(int) ) ? RQManager.Config.MaxRequestTimeOut : (int)maxTimeOut);

			return servico;
		}

		public ISolicitacaoTransporte GetServico (string idServico)
		{
			throw new NotImplementedException ();
//			var transportes = Transportes;
//
//			if (transportes == null)
//				return null;
//
//			foreach (var item in transportes) 
//				if (item.Key.Equals (idServico))
//					return item;
//
//			return null;

		}

		public bool ServicosPendentes ()
		{
			var transportes = _utente.Servicos;

			if (transportes == null)
				return false;

			foreach (var item in transportes) 
				if ((EstadoPedido)item.Estado != EstadoPedido.Concluido)
					return true;
			return false;

		}

		public String Telefone { get { return _utente.Telefone; } set { _utente.Telefone = value; } }

		public string PrimeiroNome {
			get { return _utente.PrimeiroNome; }
			set { _utente.PrimeiroNome = value;}
		}

		public string UltimoNome {
			get{ return _utente.UltimoNome;}
			set{ _utente.UltimoNome = value;}
		}

		public string NomeUtilizador { get { return _utente.NomeUtilizador; } }

		public string Email { 
			get { return _utente.Email;}
			set { _utente.Email = value;}
		}

		public IList<IServicoTransporte> Transportes {
			get {
								
//				var transportes = _utente.Servicos;  // DBService.SelectByCriteria<ServicoTransporte> ("Utente", _utente);
//	
//				if (transportes == null || transportes.Count == 0)
//					return null;
//
//				var items = new List<IServicoTransporte> ();
//
//				foreach (var item in transportes) 
//					if (item.ServicoTransporte != null)
//						items.Add (null);//new BOServicoTransporte(item));
//				return items;
				throw new NotImplementedException();
			}
		}
						
		public override string Key {
			get { return _utente.NomeUtilizador; }
		}

		public override Entidade Entity {
			get { return _utente;}
			set { _utente = (Utente)value;}
		}


	}
}

