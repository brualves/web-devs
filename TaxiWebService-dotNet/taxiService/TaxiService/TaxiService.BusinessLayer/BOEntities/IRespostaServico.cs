using System;
using TaxiService.Domain.Entities;

namespace TaxiService.BusinessLayer.BOEntities
{

	public interface IRespostaServico : IBOEntity
	{
		
		ISolicitacaoTransporte SolicitacaoTranporte { get; set; }

		IServicoTaxi ServicoTaxi { get; set; }
		
		double Latitude  { get; set; }
		
		double Longitude { get; set; }
		
		int? Tipo		{ get; set;}


	}

	public enum TipoResposta
	{
		Aceitar, Confirmar
	}

	public class BORespostaServico : BaseEntity, IRespostaServico
	{

		RespostaServico _resposta;

		ISolicitacaoTransporte _solicitacao;
		IServicoTaxi _servicoTaxi;

		public BORespostaServico ( RespostaServico resposta) {
			_resposta = resposta;
		}	

		public override string Key { get { return _resposta.Id.ToString();}}

		public override Entidade Entity {
			get { return _resposta;}
			set { _resposta = (RespostaServico) value;}
		}


		public ISolicitacaoTransporte SolicitacaoTranporte {
			get {
					if(_solicitacao == null )
					_solicitacao = new BOSolicitacaoTransporte(_resposta.SolicitacaoTranporte);
				return _solicitacao;
			}
			set {
				_solicitacao = value;
				if(_solicitacao == null ) {_solicitacao =null; _resposta.SolicitacaoTranporte = null;}
				else 
					_resposta.SolicitacaoTranporte =  ((BaseEntity)_solicitacao).Entity as SolicitacaoTranporte;
			}
		}
		public IServicoTaxi ServicoTaxi {
			get {
				 if(_servicoTaxi == null )
					_servicoTaxi = new BOServicoTaxi(_resposta.ServicoTaxi);
				return _servicoTaxi;
			}
			set {
				_servicoTaxi = value;
				if(_servicoTaxi == null ){ _servicoTaxi = null; _resposta.ServicoTaxi = null ;}
				else 
					_resposta.ServicoTaxi = ((BaseEntity) _servicoTaxi).Entity as ServicoTaxi;
 			}
		}
		public double Latitude {
			get {
				return _resposta.Latitude;
			}
			set {
				_resposta.Latitude = value;
			}
		}
		public double Longitude {

			get { return _resposta.Longitude;}
			set { _resposta.Longitude = value;}
		}
		public int? Tipo {

			get { return _resposta.Tipo ;}
			set { _resposta.Tipo = value;}
		}
	}
}

