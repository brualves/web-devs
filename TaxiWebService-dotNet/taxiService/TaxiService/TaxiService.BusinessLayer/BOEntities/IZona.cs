using System;
using System.Collections.Generic;
using TaxiService.Domain.Entities;

namespace TaxiService.BusinessLayer.BOEntities
{
	public interface IZona : IBOEntity
	{

		int?  	Contigente{ get; set; }

		string  Designacao { get; set; }

		string Codigo{ get; }

		IList<ITaxi> Taxis{ get; }

		ITaxi GetTaxi (string key);

	}
	
	internal class BOZona : BaseEntity,IZona
	{
		private Zona _zona;

		internal BOZona (Zona zona)
		{
			_zona = zona;
		}

		public override string Key { get { return Codigo; } }
	
		public override Entidade Entity { 
			get { return _zona;}  
			set { _zona = value as Zona; }
		}

		public IList<ITaxi> Taxis {
			get {

				var taxis_db = _zona.Taxis;
				if (taxis_db == null)
					return null;
				var taxis_bo = new List<ITaxi> ();
				foreach (var item in taxis_db)
					taxis_bo.Add (new BOTaxi (item));
				return taxis_bo;
			} 
		}

		public ITaxi GetTaxi (string key)
		{
			var taxi = (BOTaxi)new AdmTaxi ().SelectByKey (key);
			if (taxi == null || _zona.Equals (taxi.Entity))
				return null;
			return taxi;
		}

		public int? Contigente {
			get { return _zona.Contigente;}
			set { _zona.Contigente = value;}
		}

		public string Designacao {
			get { return _zona.Designacao;}
			set { _zona.Designacao = value;}
		}

		public string Codigo {
			get { return _zona.Codigo;}
		}
	}
}

