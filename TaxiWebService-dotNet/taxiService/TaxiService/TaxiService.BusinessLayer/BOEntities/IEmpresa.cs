using System;
using TaxiService.Domain.Entities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer.BOEntities
{
	public interface IEmpresa : IBOEntity
	{

		string Alvara 	  { get; set; }

		string Designacao { get; }

		string Telefone   { get; set; }

		IList<ITaxi> 	 Taxis 	 { get; }

		IList<ITaxista> Taxistas { get; }

		ITaxista GetTaxista (string key);

		ITaxi GetTaxi (string key);

		IList<ITaxi> GetTaxis (bool? isActive);

		bool AddTaxista (ITaxista taxista);

		bool AddTaxis (ITaxi taxi);


	}
	
	internal class BOEmpresa : BaseEntity, IEmpresa
	{

		private Empresa _empresaDB;

		internal BOEmpresa (Empresa empresa)
		{
			_empresaDB = empresa;
		}

		public override Entidade Entity { get { return _empresaDB; } set { _empresaDB = value as Empresa; } }

		public override string Key { get { return Designacao; } }

		public bool AddTaxista (ITaxista taxista)
		{
			if (taxista == null)
				throw new ArgumentNullException ("taxista");

			BOTaxista inTaxista = (BOTaxista)taxista;
			inTaxista.Empresa = this;
			DBService.Insert (inTaxista.Entity);
			return true;
		}

		public bool AddTaxis (ITaxi taxi)
		{
			if (taxi == null)
				throw new ArgumentNullException ("taxi");
			BOTaxi inTaxi = (BOTaxi)taxi;
			inTaxi.Empresa = this;
			DBService.Insert (inTaxi.Entity);
			return true;
		}

		public string Alvara {
			get { return _empresaDB.Alvara; }
			set { _empresaDB.Alvara = value; }
		}

		public string Designacao {
			get { return _empresaDB.Designacao;}
			internal set { _empresaDB.Designacao = value;}
		}

		public string Telefone {
			get { return _empresaDB.Telefone;}
			set { _empresaDB.Telefone = value;}
		}

		public IList<ITaxi> Taxis {	
			get { 

				var taxis_db = _empresaDB.Taxis;
				if(taxis_db == null ) return null;
				var taxis_bo = new List<ITaxi>();
				foreach (var item in taxis_db) taxis_bo.Add(new BOTaxi(item));
				return taxis_bo;
			}
		}


		public IList<ITaxi> GetTaxis (bool? isActive)
		{
			if (isActive == null)
				return null;
			var taxis = Taxis;
			if (taxis == null || taxis.Count == 0) 
				return null;
			var retTaxis = new List<ITaxi> ();
			foreach (var item in taxis) 
				if (item.IsAvailable == isActive)
					retTaxis.Add (item);
			return retTaxis;
		}

		public IList<ITaxista> Taxistas {
			get {
				var taxista_db = _empresaDB.Taxista;
				if(taxista_db == null ) return null;
				var taxista_bo = new List<ITaxista>();
				foreach (var item in taxista_db) taxista_bo.Add(new BOTaxista(item));
				return taxista_bo;
			}
		}

		public ITaxista GetTaxista (string key)
		{

			BOTaxista taxista = (BOTaxista)( new AdmTaxista ().SelectByKey (key));

			if (taxista == null || _empresaDB.Equals(taxista.Entity) )
				return null;
			return taxista;
		}

		public ITaxi GetTaxi (string key)
		{
			var taxi = (BOTaxi)new AdmTaxi ().SelectByKey (key);
			if (taxi == null || _empresaDB.Equals(taxi.Entity) )
				return null;
			return taxi;
		}
	}
}

