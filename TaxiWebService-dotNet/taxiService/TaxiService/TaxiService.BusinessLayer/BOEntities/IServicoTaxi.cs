using System;
using TaxiService.Domain.Entities;

namespace TaxiService.BusinessLayer.BOEntities
{
	public interface IServicoTaxi : IBOEntity
	{
	
		ITaxi 	 Taxi { get; set; }

		ITaxista Taxista { get; set; }

		DateTime Inicio { get; }

		DateTime Fim 	{ get; }

		bool   	 IsActive { get; }

		bool CloseService (DateTime dataFim);

		bool AcceptTransport (ISolicitacaoTransporte transport);
	}

	internal class BOServicoTaxi : BaseEntity, IServicoTaxi
	{

		private ServicoTaxi _servicoTaxi;
		private BOTaxi _taxi;
		private BOTaxista _taxista;

		internal BOServicoTaxi (ServicoTaxi servico)
		{
			_servicoTaxi = servico;
			_servicoTaxi.InicioServico = DateTime.Now;
		}
		#region IServicoTaxi implementation
		public bool CloseService (DateTime dataFim)
		{
			if (IsActive)
				_servicoTaxi.FimServico = dataFim;
			return true;
		}

		public ITaxi Taxi {
			get {
				if (_taxi == null)
					_taxi = new BOTaxi (_servicoTaxi.Taxi);
				return	 _taxi;
			}

			set {
				_taxi = (BOTaxi)value;
				_servicoTaxi.Taxi = (Taxi)_taxi.Entity;
			}
		}

		public ITaxista Taxista {
			get {
				if (_taxista == null)
					_taxista = new BOTaxista (_servicoTaxi.Taxista);
				return	 _taxista;
			}
			set {
				_taxista = (BOTaxista)value;
				_servicoTaxi.Taxista = (Taxista)_taxista.Entity;
			}
		}

		public DateTime Inicio {
			get {
				return _servicoTaxi.InicioServico;
			}
			internal set {
				_servicoTaxi.InicioServico = value;
			}
		}

		public DateTime Fim {
			get {
				return _servicoTaxi.FimServico;
			}
			set {
				_servicoTaxi.FimServico = value;
			}
		}

		public bool IsActive {
			get { return  Fim == default(DateTime);}

		}

		public bool AcceptTransport (ISolicitacaoTransporte transport)
		{
//			BOServicoTransporte tr = (BOServicoTransporte) transport;
//
//			tr.SetServicoTaxi (_servicoTaxi);
//			  if (! new AdmServicoTransporte ().Update (tr))
//					return;

			//notifica utente que o pedido foi aceite

//			return true;

			throw new NotImplementedException ();
		}
		#endregion
		#region implemented abstract members of BaseEntity
		public override string Key {
			get { return _servicoTaxi.Id.ToString ();}
		}

		public override Entidade Entity {
			get {
				return _servicoTaxi;
			}
			set { _servicoTaxi = (ServicoTaxi)value; }
		}
		#endregion
	}
}

