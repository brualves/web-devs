using System;
using System.Collections.Generic;
using TaxiService.Domain.Entities;

namespace TaxiService.BusinessLayer.BOEntities
{
	public interface ITaxista : IBOEntity
	{

		string  NomeUtilizador { get;}

		string  PrimeiroNome { get; set; }

		int?    Idade { get; set; }
		
		string  CAP { get; set; }
		
		string  Telefone { get; set; }

		string PassWord { get; set; }

		IEmpresa Empresa { get; }

		IList<IServicoTaxi> Servicos { get;}

		int? Rating { get;} 

		string IdAplicacao { get; set;}

		string Email { get; set;}
	}
	
	internal class BOTaxista : BaseEntity ,ITaxista
	{
				
		private  Taxista _taxista;
		private  BOEmpresa _empresa;
		
		internal BOTaxista (Taxista taxista)
		{
			if (taxista == null)
				throw new ArgumentNullException ("taxista");
		
			_taxista = taxista;
		}

		public override string Key { get { return NomeUtilizador; } }

		public string NomeUtilizador { get { return _taxista.NomeUtilizador;;}}

		public string IdAplicacao{
			get{  return _taxista.ApplicationID ; }
			set { _taxista.ApplicationID = value ;}
		}

		public override Entidade Entity { get { return _taxista; } set { _taxista = value as Taxista ;}}

		public IList<IServicoTaxi> Servicos 
		{
			get {
				var servicos_db = _taxista.Servicos;
				if(servicos_db == null ) return null;
				
				var servicos = new List<IServicoTaxi>();
				foreach (var item in servicos_db) 
					servicos.Add(new BOServicoTaxi(item));
				return servicos;
			}
		}

		public string PrimeiroNome {
			get { return _taxista.PrimeiroNome;}
			set { _taxista.PrimeiroNome = value;}
		}
		public string Email {
			get{ return _taxista.Email;} 
			set{ _taxista.Email = value;}
		}

		public int? Idade {
			get { return _taxista.Idade;}
			set { _taxista.Idade = value;}
		}
	
		public string CAP {
			get { return _taxista.CAP;}
			set { _taxista.CAP = value;}
		}


		public string PassWord {
			get {return  _taxista.PassWord; }
			set{ _taxista.PassWord = value; }
		}
		public string Telefone {
			get { return _taxista.Telefone;}
			set { _taxista.Telefone = value;}
		}

		public IEmpresa Empresa {
			get {
				if (_empresa == null) {
					return  _empresa = new BOEmpresa (_taxista.Empresa);
				}
				return _empresa;
	
			}

			internal set{ _empresa = value as BOEmpresa;}
		}

		public int? Rating {
			get {
				var servicos = Servicos;
				int nrAval = 1, rating = 3;

				foreach (var item in servicos) {

					var transportes = DBService.
						SelectByCriteria<ServicoTransporte>( "ServicoTaxi", 
					                                         ((BOServicoTaxi)item).Entity);

					if(transportes !=  null ){
						foreach (var tr in transportes) {
							rating +=  ( tr.Avaliacao == null ) ? 0 : (int)tr.Avaliacao;	nrAval++;
						}
					}
				}

				return rating/nrAval;

			}
		}
	}
}

