using System;
using TaxiService.Domain.Entities;
using TaxiService.BusinessLayer.BOEntities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer
{

	public interface IBOEntity 
	{
		string Key { get;}
		
	} 

	public abstract class BaseEntity : IBOEntity 
	{


		public abstract string Key { get;} 
		public abstract Entidade Entity{ get; set;}

	}
}

