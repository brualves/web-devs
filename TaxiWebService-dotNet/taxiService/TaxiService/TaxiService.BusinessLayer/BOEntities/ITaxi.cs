using System;
using System.Collections.Generic;
using TaxiService.Domain.Entities;

namespace TaxiService.BusinessLayer.BOEntities
{
	public interface ITaxi : IBOEntity
	{

		IEmpresa Empresa { get;}

		string Matricula { get;}
		
		string Licenca { get; set; }
		
		int? Lugares { get; set; }

		int? Numero { get; set; }
		
		DateTime Ano { get; set; }
		
		ITaxista Taxista { get; set; }

		IZona Zona { get; set; }
		
		List<IServicoTaxi> Servicos { get; set; }

		bool IsAvailable { get;}
	
	}

	internal class BOTaxi : BaseEntity, ITaxi 
	{

		Taxi _taxi;
		BOZona _zona;
		BOEmpresa _empresa;

		internal BOTaxi( Taxi taxi){ _taxi = taxi;}

		public override Entidade Entity {
			get { return _taxi;}
			set { _taxi = value as Taxi;}
		}

		public override string Key { get { return Matricula;}}

		public IEmpresa Empresa 
		{
			get { 
				if(_empresa == null )
					_empresa = new BOEmpresa(_taxi.Empresa);
				return _empresa;
			}
		internal set { _empresa = value as BOEmpresa;
				if (_empresa == null ) return;
				_taxi.Empresa = _empresa.Entity as Empresa;
			}
		}

		public bool IsAvailable { get{
				var servicos = Servicos;

				if (servicos == null || servicos.Count == 0 )
					return true;

				foreach (var item in servicos)
					if (! item.IsActive )
						return false;
	
				return true;

			}}

		public int? Numero { get{ return _taxi.Numero;} set{ _taxi.Numero = value;} }

		public string Matricula { get { return  _taxi.Matricula;} }

		public string Licenca {
			get { return _taxi.Licenca ;}
			set { _taxi.Licenca = value;}
		}

		public int? Lugares {
			get { return _taxi.Lugares ;}
			set { _taxi.Lugares = value;}
		}

		public DateTime Ano {
			get { return _taxi.Ano ;}
			set { _taxi.Ano = value ;}
		}

		public ITaxista Taxista {
			get {
				throw new NotImplementedException ();
			}
			set {
				throw new NotImplementedException ();
			}
		}

		public IZona Zona {
			get { 
				if(_zona == null ) _zona = new BOZona( _taxi.Zona);
				return _zona;
			}
			set {  _zona = value as BOZona;
				if(_zona == null ) return;  
				_taxi.Zona = _zona.Entity as Zona; 
			}
		}

		public List<IServicoTaxi> Servicos {
			get {
				var servicos_db = _taxi.Servicos;
				if(servicos_db == null ) return null;

				var servicos = new List<IServicoTaxi>();
				foreach (var item in servicos_db) 
					servicos.Add(new BOServicoTaxi(item));
				return servicos;
			}
			set {
				throw new NotImplementedException ();
			}
		}



	}
}

