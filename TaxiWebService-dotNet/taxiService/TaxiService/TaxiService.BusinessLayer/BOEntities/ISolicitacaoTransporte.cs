using System;
using TaxiService.Domain.Entities;
using System.Collections.Generic;

namespace TaxiService.BusinessLayer.BOEntities
{
	public interface ISolicitacaoTransporte : IBOEntity
	{
		IUtente  	 Utente { get; set; }

		EstadoPedido Estado{ get; set; }

		double  	 Latitude{ get; set; }

		double  	 Longitude{ get; set; }

		String 		 PontoReferencia { get; set; }

		int 		 MinRate { get; set; }

		IList<RespostaServico> Respostas 	{ get; }

		IServicoTransporte GetServicoTransporte ();

		//IServicoTransporte ServicoTransporte { get; set; }

		//	int 	 		Rating{ get; set;}	
		//ITaxi 	 		Taxi{ get;}
		//ITaxista 		Taxista {get;}
		int TimeOut { get; set; }

		bool Cancel ();

	}

	internal class BOSolicitacaoTransporte : BaseEntity, ISolicitacaoTransporte
	{


		private SolicitacaoTranporte _solicitacaoTransporte;
		IUtente _utente;

		const string SERVICE_COLUMN = "SolicitacaoTranporte";

		//IServicoTransporte _servico;

		internal BOSolicitacaoTransporte (SolicitacaoTranporte transporte)
		{
		 
			_solicitacaoTransporte = transporte;

		}
	
		public int TimeOut { get; set; }

		public string PontoReferencia {
			get { return _solicitacaoTransporte.PontoReferencia; }
			set { _solicitacaoTransporte.PontoReferencia = value;}
		}

		#region IServicoTransporte implementation
		public bool Cancel ()
		{
			throw new NotImplementedException ();
		}

		public IUtente Utente {
			get {
				if (_utente == null) 
					_utente = new BOUtente (_solicitacaoTransporte.Utente);
				return _utente;
			}
			set {
				_utente = value;
				if (value == null) {
					_solicitacaoTransporte.Utente = null;
					return;
				}
				_solicitacaoTransporte.Utente = (Utente)((BOUtente)_utente).Entity;
			}
		}
//		public ITaxi Taxi {
//			get {
//				if(_solicitacaoTransporte.Resposta == null ) return null;
//				if(_taxi ==  null)  
//					_taxi = new BOTaxi(_solicitacaoTransporte.Resposta.Taxi);
//				return _taxi;
//			}
//		internal set {
//
//				if(_solicitacaoTransporte.Resposta == null  ) return ;
//			
//				if (value == null){ _solicitacaoTransporte.Resposta.Taxi = null;_taxi = null; return;}
//
//				_taxi = value;
//				_solicitacaoTransporte.Resposta.Taxi = (Taxi)((BOTaxi)_taxi).Entity;
//			}
//		}
//		public ITaxista Taxista {
//			get {
//				if(_solicitacaoTransporte.Resposta == null  ) return null;
//				if(_taxista == null )
//					_taxista = new BOTaxista(_solicitacaoTransporte.Resposta.Taxista);
//				return _taxista;
//			}
//			internal set {
//
//				if(_solicitacaoTransporte.Resposta == null )return;
//			
//				if(value == null ) { _solicitacaoTransporte.Resposta.Taxista = null;_taxista = null;return;}
//				_taxista = value;
//				_solicitacaoTransporte.Resposta.Taxista = (Taxista)((BOTaxista)_taxista).Entity ;
//
//			}
//		}
		public double Latitude {
			get { return _solicitacaoTransporte.Latitude;}
			set { _solicitacaoTransporte.Latitude = value;}
		}

		public double Longitude {
			get { return _solicitacaoTransporte.Longitude;}
			set { _solicitacaoTransporte.Longitude = value;}
		}
//		public int? Rating {
//			get {
//				return _solicitacaoTransporte.Avaliacao;
//			}
//			set {
//				_solicitacaoTransporte.Avaliacao = value;
//			}
//		}

		public EstadoPedido Estado {
			get { return (EstadoPedido)_solicitacaoTransporte.Estado;} 
			set { _solicitacaoTransporte.Estado = (int)value; }
		}
		#endregion

		public int MinRate { 
			get { return _solicitacaoTransporte.MinRate;}
			set{ _solicitacaoTransporte.MinRate = value;} 
		}

//		internal void SetServicoTaxi(ServicoTaxi servico ){
//
//			_solicitacaoTransporte.Resposta = servico;
//
//		}

		public override string Key {
			get { return _solicitacaoTransporte.Id.ToString ();}
		}

		public override Entidade Entity {
			get {
				return _solicitacaoTransporte;
			}
			set {
				_solicitacaoTransporte = (SolicitacaoTranporte)value;
			}
		}


		public IList<RespostaServico> Respostas 	{ get { throw new NotImplementedException (); } }


		public IServicoTransporte GetServicoTransporte ()
		{
			var servico = DBService.SelectByCriteria <ServicoTransporte>(
				SERVICE_COLUMN,  this.Entity );

			var enumerator = servico.GetEnumerator ();
			enumerator.MoveNext ();

			return (servico == null || servico.Count == 0 ) 
				? null : new BOServicoTransporte(enumerator.Current);

		
		}

//		public IServicoTransporte ServicoTransporte { 
//			get {
//				if (_servico == null)
//					_servico = new BOServicoTransporte (_solicitacaoTransporte.ServicoTransporte);
//				return _servico;
//
//			} 
//			set {
//				_servico = value;
//				if (_servico == null) 
//					_solicitacaoTransporte.ServicoTransporte = null;
//				else 
//					_solicitacaoTransporte.ServicoTransporte = ((BaseEntity)_servico).Entity as ServicoTransporte; 
//				;
//			}
//		}
	}
}

