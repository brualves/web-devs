using System;
using TaxiService.Domain.Entities;

namespace TaxiService.BusinessLayer.BOEntities
{
	public interface IServicoTransporte  : IBOEntity
	{
		ISolicitacaoTransporte SolicitacaoTransporte { get; set; }

		IServicoTaxi ServicoTaxi { get; set; }

		int? Avalicao			 { get; set; }

		int DistanciaTempo 		{ get; set; }

	}

	public class BOServicoTransporte : BaseEntity, IServicoTransporte
	{

		private ServicoTransporte _servicoTransporte;
		private IServicoTaxi 		_servicoTaxi;
		private ISolicitacaoTransporte _solicitacao;

		public BOServicoTransporte (ServicoTransporte servicoTransporte)
		{
			_servicoTransporte = servicoTransporte;
		}


		public int DistanciaTempo 		{ 
			get { return _servicoTransporte.DistanciaTempo;}
			set{_servicoTransporte.DistanciaTempo = value ;}}

		public IServicoTaxi ServicoTaxi { 
			get { 
				if (_servicoTaxi == null)
					_servicoTaxi = new BOServicoTaxi (_servicoTransporte.ServicoTaxi);
				return _servicoTaxi;
			}

			set {
				_servicoTaxi = value;
				if (_servicoTaxi == null) 
					_servicoTransporte.ServicoTaxi = null;
				else
				_servicoTransporte.ServicoTaxi = (ServicoTaxi)((BaseEntity)_servicoTaxi).Entity;						
			}
		}

		public override string Key {
			get { return _servicoTransporte.Id.ToString ();}
		}

		public override Entidade Entity {
			get {
				return _servicoTransporte;
			}
			set {
				_servicoTransporte = (ServicoTransporte)value;
			}
		}

		public ISolicitacaoTransporte SolicitacaoTransporte {
			get {
				if (_solicitacao == null)
					_solicitacao = new BOSolicitacaoTransporte (_servicoTransporte.SolicitacaoTranporte);
				return _solicitacao;
			}
			set {
				_solicitacao = value;
				if (_servicoTransporte == null) {
					_servicoTransporte.SolicitacaoTranporte = null;
					return;
				}

				_servicoTransporte.SolicitacaoTranporte = (SolicitacaoTranporte)((BaseEntity)value).Entity;

			}
		}

		public int? Avalicao {
			get { return _servicoTransporte.Avaliacao;}
			set { _servicoTransporte.Avaliacao = value;}
		}

	}
}

