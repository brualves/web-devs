using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using TaxiService.BusinessLayer.BOEntities;

namespace TaxiService.BusinessLayer
{
	public class TaxistasManager
	{
		static TaxistasManager _instance;

		//ConcurrentBag <IServicoTaxi> _servicosAtivos;


		public static TaxistasManager Instance {
			get{
				if( _instance == null)
					_instance = new TaxistasManager();
				return _instance ;
			}
		}


		private TaxistasManager ()
		{
			//_servicosAtivos = new ConcurrentBag<IServicoTaxi> ();
		}


		public  IList <IServicoTaxi> GetServicosAtivos()
		{ 

			var servicoAtivos = BLFacade.ListEnity<IServicoTaxi> (
				(servico) => {
				return servico.Fim == default(DateTime); });

			return servicoAtivos;

		}

	}
}

