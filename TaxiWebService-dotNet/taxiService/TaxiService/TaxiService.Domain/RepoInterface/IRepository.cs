using System;
using System.Collections.Generic;
using TaxiService.Domain.Entities;

namespace TaxiService.Domain.RepoInterface
{
	public interface IRepository
	{
	
		void Insert (Entidade entety);

		void Update (Entidade entety);

		bool Delete (Entidade entety);

		bool Contains (Entidade entety);

		T SelectById<T> (Guid iD) where T : Entidade;

		IList<T> List<T> ()  	  where T : Entidade;

		int Count <T> () 		  where T : Entidade;

		IList<T> SelectByCriteria<T> (String property, object value) where T : Entidade;
	}
}

