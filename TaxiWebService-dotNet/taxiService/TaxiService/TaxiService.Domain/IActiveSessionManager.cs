using System;

namespace TaxiService.Domain
{
	public interface IActiveSessionManager 
	{
		ISession GetActiveSession();
		
	}

}

