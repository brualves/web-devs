using System;

namespace TaxiService.Domain
{
	public interface ISession
	{

		void Commit();
		void Rollback();
	}
}

