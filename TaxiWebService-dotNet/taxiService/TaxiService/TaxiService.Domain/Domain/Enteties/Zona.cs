using System;
using Iesi.Collections.Generic;

namespace TaxiService.BussinessLayer.Domain.Entities
{
	public class Zona : Entity
	{
		public virtual String Designacao { get; set; }

		public virtual int?	Contigente{ get; set; }

		public virtual ISet<Taxi> Taxis { get; set; }
	}
}

