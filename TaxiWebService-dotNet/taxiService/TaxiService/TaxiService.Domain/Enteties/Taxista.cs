using System;
using Iesi.Collections.Generic;

namespace TaxiService.Domain.Entities
{
	public class Taxista : Utilizador
	{

		public virtual int?    Idade 				{ get; set; }

		public virtual string  CAP 					{ get; set; }

		public virtual string  Telefone 			{ get; set; }

		public virtual string ApplicationID 		{ get; set; }

		public virtual Empresa Empresa 				{ get; set; }

		public virtual ISet<ServicoTaxi> Servicos 	{ get; set; }



	}
}

