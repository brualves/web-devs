using System;
using Iesi.Collections.Generic;

namespace TaxiService.Domain.Entities
{
	public class SolicitacaoTranporte : Entidade
	{
		public virtual Utente Utente { get; set; }
			
		public virtual int Estado{ get; set;}

		public virtual double Latitude  { get; set; }

		public virtual double Longitude { get; set; }

		public virtual string PontoReferencia { get; set; }

		public virtual ISet<RespostaServico> Respostas 	{ get; set; }

		//public virtual ServicoTransporte ServicoTransporte { get; set; }

		public virtual int MinRate { get; set;}
	}
}

