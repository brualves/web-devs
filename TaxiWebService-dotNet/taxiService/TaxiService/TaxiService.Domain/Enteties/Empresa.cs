using System;
using Iesi.Collections.Generic;

namespace TaxiService.Domain.Entities
{
	public class Empresa : Entidade
	{
		public virtual string Alvara { get; set; }

		public virtual string Designacao { get; set; }

		public virtual string Telefone { get; set; }

		public virtual ISet<Taxi> Taxis { get; set; }

		public virtual ISet<Taxista> Taxista { get; set; }

		public virtual bool Equals (Empresa other)
		{
			if (other == null)
				return false;

			if (Entidade.IsTransient (other) && (Alvara.Equals (other.Alvara) || Designacao.Equals (other.Designacao)))
				return true;
			return base.Equals (other);
		
		}

		public override bool Equals (object other)
		{
			if (other == null)
				return false; 
			return Equals (other as Empresa);
		}

		public override int GetHashCode ()
		{
			return base.GetHashCode ();
		}
	}
}

