using System;

namespace TaxiService.Domain.Entities
{
	public class ServicoTransporte : Entidade 
	{

		public virtual SolicitacaoTranporte SolicitacaoTranporte { get; set; }

		public virtual ServicoTaxi ServicoTaxi { get; set;}

		public virtual int? Avaliacao{ get; set;}
	
		public virtual int DistanciaTempo { get; set;}
	}
}

