using System;
using Iesi.Collections.Generic;

namespace TaxiService.Domain.Entities
{
	public class ServicoTaxi : Entidade
	{
		public virtual Taxi Taxi { get; set;}
		public virtual Taxista Taxista { get; set;}
		public virtual DateTime InicioServico { get; set;}
		public virtual DateTime FimServico { get; set;}

	}
}

