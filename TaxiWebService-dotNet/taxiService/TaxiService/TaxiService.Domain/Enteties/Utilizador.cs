using System;

namespace TaxiService.Domain.Entities
{
	public abstract class Utilizador : Entidade
	{

		public virtual string NomeUtilizador { get; set; }

		public virtual string PrimeiroNome { get; set; }

		public virtual string UltimoNome { get; set; }

		public virtual string Email { get; set; }

		public virtual string PassWord { get; set; }

	}
}

