using System;
using Iesi.Collections.Generic;

namespace TaxiService.Domain.Entities
{
	public class RespostaServico : Entidade
	{

		public virtual SolicitacaoTranporte SolicitacaoTranporte { get; set; }

		public virtual ServicoTaxi ServicoTaxi { get; set; }

		public virtual double Latitude  { get; set; }
		
		public virtual double Longitude { get; set; }

		public virtual int? Tipo{ get; set;}

	}

	

}

