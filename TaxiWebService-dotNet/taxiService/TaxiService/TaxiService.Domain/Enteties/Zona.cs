using System;
using TaxiService.Domain.Entities;
using Iesi.Collections.Generic;

namespace TaxiService.Domain.Entities
{
	public class Zona : Entidade
	{
		public virtual string Designacao { get; set; }

		public virtual int?	Contigente{ get; set; }

		public virtual string Codigo { get; set; }

		public virtual ISet<Taxi> Taxis { get; set; }
	}
}

