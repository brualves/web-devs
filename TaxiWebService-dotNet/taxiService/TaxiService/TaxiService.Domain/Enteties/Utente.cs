using System;
using Iesi.Collections.Generic;

namespace TaxiService.Domain.Entities
{
	public class Utente : Utilizador
	{
		public virtual string IdAplicacao 			   	{ get; set; }

		public virtual string Telefone 			  		  	{ get; set; }

		public virtual ISet<SolicitacaoTranporte> Servicos 	{ get; set; }

		public virtual int Penalizacao						{ get; set; }

	}
}

