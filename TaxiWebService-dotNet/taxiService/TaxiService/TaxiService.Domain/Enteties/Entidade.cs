using System;

namespace TaxiService.Domain.Entities
{
	public abstract class Entidade <TId>
	{
	
		public virtual TId Id 				{ get; set; }

		public virtual TimeSpan Version 	{ get; set; }

		public virtual DateTime DataCriacao { get; set; }

		public virtual Utilizador CriadoPor	{ get; set; }

		public virtual DateTime DataModficacao 	{ get; set; }
	
		public virtual Utilizador ModificadoPor 	 	{ get; set; }

		public override bool Equals (object obj)
		{
			return Equals (obj as Entidade <TId>);
		}

		protected static bool IsTransient (Entidade <TId> obj)
		{
			return obj != null &&
				Equals (obj.Id, default(TId));
		}

		protected Type GetUnproxiedType ()
		{
			return GetType ();
		}

		public virtual bool Equals (Entidade<TId> other)
		{
			if (other == null)
				return false;
			if (ReferenceEquals (this, other))
				return true;
			if (!IsTransient (this) &&
				!IsTransient (other) &&
				Equals (Id, other.Id)) {
				var otherType = other.GetUnproxiedType ();
				var thisType = GetUnproxiedType ();
				return thisType.IsAssignableFrom (otherType) ||
					otherType.IsAssignableFrom (thisType);
			}
			return false;
		}

		public override int GetHashCode ()
		{
			if (Equals (Id, default(TId)))
				return base.GetHashCode ();
			return Id.GetHashCode ();
		} 

	}
	
	public abstract class Entidade : Entidade<Guid>{}
}

