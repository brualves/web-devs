using System;
using Iesi.Collections.Generic;

namespace TaxiService.Domain.Entities
{
	public class Taxi : Entidade
	{

		public virtual string Matricula { get; set; }

		public virtual string Licenca { get; set; }

		public virtual int? Lugares { get; set; }

		public virtual DateTime Ano { get; set; }

		public virtual Empresa Empresa { get; set; }

		public virtual Zona Zona { get; set; }

		public virtual int? Numero{ get; set; }

		public virtual ISet<ServicoTaxi> Servicos { get; set; }
	}
}

