using System;

namespace TaxiService.Domain.Exception
{
	public class DBException : SystemException
	{

		public DBException(String message ) : base (message) { }
	}
}

