//TODO ao elminar o ticket eliminar o respetivo input

var qeventos = {};


qeventos.renderColumns = function (helpers, callback) {

    var column = helpers.columnAttr;

    // get all the data for the entire row
    var rowData = helpers.rowData;
    var customMarkup = '';

    // Only override the output for specific columns.
    // This will default to output the text value of the row item
    switch (column) {
        case 'name':
            customMarkup = '<a href="evento/' + rowData.id + '" >' + rowData.name + '</a>';
            break;

        case 'action':
            customMarkup = '<a class="tab-action" href="#"><i class="fa fa-trash"></i></a>';
            customMarkup += '<a class="tab-action" href="#"><i class="fa fa-share"></i></a>'

            break;
        default:
            // otherwise, just use the existing text value
            customMarkup = helpers.item.text();
            break;
    }

    helpers.item.html(customMarkup);
    callback();


};

qeventos.init = function () {

    $('#myWizard').wizard();
    $('#myPillbox').pillbox();


    var repeater = $('#eventListRepeater');
    repeater.repeater({
        dataSource: loadEvents,
        list_columnRendered: qeventos.renderColumns
    });

    //register for events Wizard events
    $('#myWizard').on('finished.fu.wizard', function (evt, data) {

        $('form#createEvent').submit();

        //collect user input
    });

    $('#publishEvent').on('click', function (evt) {

        evt.preventDefault();
        $('form#createEvent').attr('action', "/publicarevento")
        $('form#createEvent').submit();

    })


    $($('#myWizard')).on('actionclicked.fu.wizard', function (evt, data) {

        if (data.step === 3 && data.direction === 'next') {

            var dateVal = $('#initilaDate').val().replace('/', '-');
            if (dateVal && dateVal.trim())
                dateVal = new Date(dateVal).toDateString();

            $('#step4-event').text($('#eventName').val());
            $('#step4-when').text(dateVal);
            $('#step4-where').text($('#eventPlace').val());
            $('#step4-ticket').text($('#ticket').val());


        }

    });

    $('#addTicket').click(
        function () {
            if (!$('#ticketType').val() || !$('#ticketType').val().trim())
                return false;

            var ticket = $('#ticketType').val() + '-' + $('#ticketValue').val() + '(Kz)';
            $('#myPillbox').pillbox('addItems', [{text: ticket, value: '', attr: {}, data: {}}]);

            ticket += ';';

            $('#ticket').val($('#ticket').val() + ticket);


            $('#ticketType').val("");
            $('#ticketValue').val("");

            return false;
        }
    );


    /*
     setTimeout(function () {
     $('.alert').alert('close');
     }, 5000)
     */


}

function loadEvents(options, callback) {

    // define the columns for the grid
    var columns = [
        {
            'label': 'Evento',      // column header label
            'property': 'name',   // the JSON property you are binding to
            'sortable': true      // is the column sortable?
        },
        {
            'label': 'Modificado',
            'property': 'createdOn',
            'sortable': true
        },
        {
            'label': 'Estado',
            'property': 'eventStatus',
            'sortable': true
        },
        {
            //'label': 'Category',
            'property': 'action'

        }
    ];

    // set options
    var pageIndex = options.pageIndex;
    var pageSize = options.pageSize;
    var options = {
        'pageIndex': pageIndex,
        'pageSize': pageSize,
        'sortDirection': options.sortDirection,
        'sortBy': options.sortProperty,
        'filterBy': options.filter.value || '',
        'searchBy': options.search || ''
    };

    // call API, posting options
    $.ajax({
        'type': 'get',
        'url': 'eventos',
        'data': options
    }).done(function (data) {

        var items = data.items;
        var totalItems = data.total;
        var totalPages = Math.ceil(totalItems / pageSize);
        var startIndex = (pageIndex * pageSize) + 1;
        var endIndex = (startIndex + pageSize) - 1;
        if (endIndex > items.length) {
            endIndex = items.length;
        }

        // configure datasource
        var dataSource = {
            'page': pageIndex,
            'pages': totalPages,
            'count': totalItems,
            'start': startIndex,
            'end': endIndex,
            'columns': columns,
            'items': items
        };
        // pass the datasource back to the repeater
        callback(dataSource);
    });

    /*
     // generate the rows in your dataset
     // note: the property names of your items should
     // match the column properties defined above
     //
     function generateRandomData() {
     var items = [];
     items.push({name: 'eventoteste'});

     return items;
     }

     items = generateRandomData();

     // transform array
     var pageIndex = options.pageIndex;
     var pageSize = options.pageSize;
     var totalItems = items.length;
     var totalPages = Math.ceil(totalItems / pageSize);
     var startIndex = (pageIndex * pageSize) + 1;
     var endIndex = (startIndex + pageSize) - 1;
     if (endIndex > items.length) {
     endIndex = items.length;
     }

     var rows = items.slice(startIndex - 1, endIndex);

     // define the datasource
     var dataSource = {
     'page': pageIndex,
     'pages': totalPages,
     'count': totalItems,
     'start': startIndex,
     'end': endIndex,
     'columns': columns,
     'items': rows
     };
     */
    // pass the datasource back to the repeater
    // callback(dataSource);
}
;

/*$(function () {
 // initialize the repeater
 var repeater = $('#eventListRepeater');
 repeater.repeater({
 // setup your custom datasource to handle data retrieval;
 // responsible for any paging, sorting, filtering, searching logic
 dataSource: loadEvents
 });
 });*/

