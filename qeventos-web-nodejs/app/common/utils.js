/**
 * Created by brunoalves on 17/03/15.
 */



var utils = {}

// route middleware to make sure a user is logged in
utils.isLoggedIn = function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();
    // if they aren't redirect them to the home page
    res.redirect('/login');

};


utils.eventCategories = function () {

//TODO deve ser lido da BD
    return [
        {catKey: 'Reves', catVal: 'Reves'},
        {catKey: 'Discoteca', catVal: 'Discoteca'},
        {catKey: 'Concertos', catVal: 'Concertos'},
        {catKey: 'Lançamento CD', catVal: 'Lançamento CD'},
        {catKey: 'Futebol', catVal: 'Futebol'}
    ];
};

module.exports = utils;