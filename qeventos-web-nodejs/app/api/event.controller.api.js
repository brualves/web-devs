var Event = require('../models/event');

module.exports = function (rest) {
    rest.get('/events', function (req, content, cb) {

        Event.find(function (err, events) {
            if (err)
                throw  err;

            cb(null, events);

        });
    });

    rest.get('/events/:id', function (req, content, cb) {
        Event.findById(req.params.id, function (err, event) {
            if (err)
                throw  err;
            cb(null, event);
        });

    });
};