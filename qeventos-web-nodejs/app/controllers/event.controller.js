/**
 * Created by brunoalves on 20/12/14.
 */

var config = require('../../config/config');
var formidable = require('formidable');

var utils = require('../common/utils');

var Address = require('mongoose').model('Address');
var Event = require('mongoose').model('Event');

var eventController = {};

module.exports = function (app, passport) {

    app.post('/novoevento', utils.isLoggedIn, eventController.createEvent);
    app.get('/novoevento', utils.isLoggedIn, eventController.newEvent);
    app.get('/eventos', utils.isLoggedIn, eventController.getUserEvents);

};

eventController.getUserEvents = function (req, res) {
    Event.find(function (err, events) {
        if (err)
            throw  err;

        res.status(200).json(
            {
                total: events.length,
                items: events

            }
        );

    });
};

eventController.createEvent = function (req, res) {


    //TODO validar os dados do evento

    var eventLocation = {
        name: req.body.location,
        streetAddress: req.body.addressLine1 + req.body.addressLine2,
        addressRegion: req.body.city,
        addressLocality: req.body.locality,
        //postOfficeBoxNumber: req.body.,
        postalCode: req.body.postalCode,
        addressCountry: req.body.country,
    };

    var offers = eventController.parseTicket(req.body.ticket);

    var postEvent = new Event({
        name: req.body.name,
        category: req.body.category,
        description: req.body.description,

        location: eventLocation,

        offers: offers,

        //TODO imagem do evento
        startDate: req.body.startDate,
        doorTime: req.body.doorTime

    });


    postEvent.save(
        function (err, newEvent) {
            if (err)
                throw  err;

            req.flash('homeMessage', 'Evento: ' + newEvent.name + 'criado com sucesso!!');

            res.redirect(301, '/home');
        }
    );

    /*    var form = new formidable.IncomingForm();
     form.keepExtensions = true;
     form.uploadDir = config.root + '/uploads';

     form.parse(req, function (err, fields, files) {

     if (err)
     throw  err;

     console.log(fields);
     console.log(files);

     var entity = new Event({
     name: fields.name,
     description: fields.description,
     category: fields.category,
     startDate: fields.startDate
     });

     entity.save(function (err) {
     if (err)
     throw  err;

     res.redirect(303, '/');
     });
     });*/
    //console.log(req.body.name);
    //console.log(req.body.description);

};


eventController.newEvent = function (req, res) {
    res.render('event/newEvent',
        {
            title: 'Qeventos | Criar evento',
            categories: utils.eventCategories()
        });
};


eventController.parseTicket = function (httpTicket) {

    if (!httpTicket) return null;
    var resultItem = [];
    var tickets = httpTicket.split(';');


    for (var index = 0; index < tickets.length; index++) {
        var ticket = tickets[index].split('-');
        if (ticket.length == 2 && ticket[0] && ticket[1])
            resultItem.push({category: 'Ticket', type: ticket[0], price: ticket[1]});

    }
    return resultItem;

}






