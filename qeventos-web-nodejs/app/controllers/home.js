//TODO rever controller
var express = require('express'),
    router = express.Router();

module.exports = function (app, passport) {
    app.use('/', router);
};

router.get('/', function (req, res, next) {
    res.render('index',
        {
            title: 'Q-eventos ',
            layout: 'main'
        });
});
