/**
 * Created by brunoalves on 14/03/15.
 */

var utils = require('../common/utils');

var accountController = {};
var passport = null;

module.exports = function (app, passport) {


    //TODO refazer os links, passar a usar links lógicos
    app.get('/home', utils.isLoggedIn, accountController.home);

    //TODO redirect para o Home page se o user já tiver autenticado
    app.get('/login', accountController.login);

    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/home', // redirect to the secure profile section
        failureRedirect: '/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/signup', function (req, res) {

        res.render('account/register', {title: 'Q-eventos | Registar'});
    });
    app.post('/signup', passport.authenticate(
        'local-signup', {
            successRedirect: '/home', // redirect to the secure profile section
            failureRedirect: '/signup', // redirect back to the signup page if there is an error
            failureFlash: true // allow flash messages
        }));
}


accountController.home = function (req, res) {
    res.render('account/home', {
        title: 'Q-eventos | Home',
        categories: utils.eventCategories(),
        message: req.flash('homeMessage')
    });
}

accountController.login = function (req, res) {

    res.render('account/login', {
        title: 'Qeventos | Login',
        message: req.flash('loginMessage')
    });
};


