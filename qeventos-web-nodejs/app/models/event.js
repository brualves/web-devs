/**
 * Created by brunoalves on 19/12/14.
 */
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var thingSchema = new Schema({
    additionalType: String,
    alternateName: String,
    description: String,
    image: String,
    name: String,
    sameAs: String,
    url: String,
    createdOn: {type: Date, default: Date.now}
});


var offers = new Schema({type: String, price: String, category: String});

thingSchema.add({

    doorTime: Date,
    duration: Number,
    endDate: Date,
    eventStatus: {type: String, default: 'draft'},
    category: String,

    location: {
        name: String,
        addressCountry: String,
        addressLocality: String,
        addressRegion: String,
        postOfficeBoxNumber: String,
        postalCode: String,
        streetAddress: String
    },
    offers: [offers],

    //attendee
    //organizer
    //performer
    //previousStartDate
    //recordedIn
    startDate: Date
    //subEvent :
    //superEvent
    //typicalAgeRange
    //workPerformed

});


module.exports = mongoose.model('Event', thingSchema);