/**
 * Created by brunoalves on 21/03/15.
 */

var mongoose = require('mongoose');

var thingSchema = require('./thing')

thingSchema.add({

    addressCountry: String,
    addressLocality: String,
    addressRegion: String,
    postOfficeBoxNumber: String,
    postalCode: String,
    streetAddress: String

});


/*


 addressCountry	Country 	The country. For example, USA. You can also provide the two-letter ISO 3166-1 alpha-2 country code.
 addressLocality	Text 	The locality. For example, Mountain View.
 addressRegion	Text 	The region. For example, CA.
 postOfficeBoxNumber	Text 	The post office box number for PO box addresses.
 postalCode	Text 	The postal code. For example, 94043.
 streetAddress	Text


 */


module.exports = {
    model: mongoose.model('Address', thingSchema),
    schema: thingSchema
};
