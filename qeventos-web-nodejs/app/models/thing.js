/**
 * Created by brunoalves on 21/03/15.
 */


var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var thingSchema = new Schema(
    {
        additionalType: String,
        alternateName: String,
        description: String,
        image: String,
        name: String,
        sameAs: String,
        url: String,
        createdOn: {type: Date, default: Date.now}
    }
);

module.exports = thingSchema;