/**
 * Created by brunoalves on 15/03/15.
 */


var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
    local: {
        email: String,
        password: String,
    },
    authId: String,
    name: String,
    role: String,
    created: Date
});

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    //TODO guardar e comparar o hash da password
    return bcrypt.compareSync(password, this.local.password);
};


// methods ======================
// generating a hash
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

var User = mongoose.model('User', userSchema);

module.exports = User;
