'use strict';

module.exports = function (grunt) {
    // show elapsed time at the end
    require('time-grunt')(grunt);
    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        app: 'app',
        staticFiles: 'public',

        concurrent: {
            target: {
                tasks: ['nodemon', 'watch'],
                options: {logConcurrentOutput: true}
            }
        },
        sass: {
            options: {
                //includePaths: ['<%= staticFiles %>/components/foundation/scss']
            },
            dist: {
                options: {
                    outputStyle: 'extended'
                },
                files: {
                    '<%= staticFiles %>/css/app.css': '<%= staticFiles %>/scss/app.scss',

                    '<%= staticFiles %>/css/site.css': '<%= staticFiles %>/scss/site.scss'
                }
            }
        },

        nodemon: {
            dev: {
                script: 'app.js',
                options: {
                    args: ['dev'],
                    ignore: ['node_modules/**', 'public/**'],
                    watch: ['app/controllers/**/*.js', 'app/models/**/*.js', 'config/*.js', 'app/api/**/*.js']
                }
            }
        },
        watch: {
            grunt: {
                files: ['Gruntfile.js'],
                tasks: ['sass', 'develop']
            },
            sass: {
                files: '<%= staticFiles %>/scss/**/*.scss',
                tasks: ['sass']
            }
        }
    });

    //grunt.registerTask('default', ['sass', 'nodemon', 'watch']);
    grunt.registerTask('default', ['sass', 'concurrent']);


};
