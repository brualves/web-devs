var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),


/*
config.port = process.env.OPENSHIFT_NODEJS_PORT || 3000
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'*/

env = process.env.NODE_ENV || 'development';

var config = {
    development: {
        root: rootPath,
        app: {
            name: 'q-eventos'
        },
        port: 3000,
        host: '127.0.0.1',
        db: 'mongodb://localhost/q-eventos-development'

    },

    test: {
        root: rootPath,
        app: {
            name: 'q-eventos'
        },
        port: 3000,
        db: 'mongodb://localhost/q-eventos-test'

    },

    production: {
        root: rootPath,
        app: {
            name: 'q-eventos'
        },
        port: process.env.OPENSHIFT_NODEJS_PORT,
        host: process.env.OPENSHIFT_NODEJS_IP,
        db: process.env.OPENSHIFT_MONGODB_DB_URL + 'qeventos'

    }
};

module.exports = config[env];
