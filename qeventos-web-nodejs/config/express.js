var express = require('express');
var glob = require('glob');

var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');

var exphbs = require('express-handlebars');

var rest = require('connect-rest');
var session = require('express-session');
var flash = require('connect-flash');


//Passport
var passport = require('passport');


module.exports = function (app, config) {
    app.set('views', config.root + '/app/views');

    app.engine('hbs', exphbs({defaultLayout: 'site', layoutsDir: app.get('views') + '/layouts', extname: 'hbs'}));
    app.set('view engine', 'hbs');

    // app.use(favicon(config.root + '/public/img/favicon.ico'));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(cookieParser());
    app.use(compress());
    app.use(express.static(config.root + '/public'));
    app.use(methodOverride());

    app.use(session({secret: 'secret'}));

    require('./passport')(passport);

    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());

    //set current user available for template engine
    app.use(function (req, res, next) {
        res.locals.user = req.user;
        next();
    });


    //Routes config
    var controllers = glob.sync(config.root + '/app/controllers/*.js');
    controllers.forEach(function (controller) {
        require(controller)(app, passport);
    });

    //API configuration
    var apiOptions = {
        context: '/api',
        domain: require('domain').create()
    };

    // link API into pipeline
    app.use(rest.rester(apiOptions));

    var controllerApi = glob.sync(config.root + '/app/api/*.js');

    controllerApi.forEach(function (controllerApi) {
        require(controllerApi)(rest);
    });

    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    if (app.get('env') === 'development') {
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: err,
                title: 'error'
            });
        });
    }

    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {},
            title: 'error'
        });
    });

};
